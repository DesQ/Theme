/**
 * This file is a part of DesQ Appearance.
 * DesQ Appearance is the Settings plugin to manage DesQ's widget theme,
 * fonts, colors and other related configurations.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This plugin is derived from cutefish
 * Copyright (C) 2020 Reven Martin
 * Copyright (C) 2020 KeePassXC Team <team@keepassxc.org>
 * Copyright (C) 2019 Andrew Richards
 *
 * Derived from Phantomstyle and relicensed under the GPLv2 or v3.
 * https://github.com/randrew/phantomstyle
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "OnyxStyle.hpp"
#include "OnyxColor.hpp"

#include <QDebug>
#include <QAbstractItemView>
#include <QApplication>
#include <QCheckBox>
#include <QComboBox>
#include <QDialogButtonBox>
#include <QGroupBox>
#include <QHeaderView>
#include <QListView>
#include <QMainWindow>
#include <QMdiSubWindow>
#include <QMenu>
#include <QPainter>
#include <QPainterPath>
#include <QPoint>
#include <QPolygon>
#include <QPushButton>
#include <QProgressBar>
#include <QRadioButton>
#include <QScrollBar>
#include <QSharedData>
#include <QSlider>
#include <QSpinBox>
#include <QSplitter>
#include <QString>
#include <QStyleOption>
#include <QTableView>
#include <QToolBar>
#include <QToolButton>
#include <QTreeView>
#include <QWindow>
#include <QWizard>
#include <QtMath>
#include <qdrawutil.h>

#include <QVariantAnimation>
#include <QEasingCurve>

#include <QSettings>

#include <cmath>

QT_BEGIN_NAMESPACE
Q_GUI_EXPORT int qt_defaultDpiX();

QT_END_NAMESPACE


namespace Phantom {
    namespace {
        constexpr qint16 DefaultFrameWidth   = 0;
        constexpr qint16 SplitterMaxLength   = 100; // Length of splitter handle (not thickness)
        constexpr qint16 MenuMinimumWidth    = 20;  // Smallest width that menu items can have
        constexpr qint16 MenuBar_FrameWidth  = 0;
        constexpr qint16 MenuBar_ItemSpacing = 10;
        constexpr qint16 SpinBox_ButtonWidth = 20;

        // These two are currently not based on font, but could be
        constexpr qint16 LineEdit_ContentsHPad             = 5;
        constexpr qint16 ComboBox_NonEditable_ContentsHPad = 7;
        constexpr qint16 HeaderSortIndicator_HOffset       = 1;
        constexpr qint16 HeaderSortIndicator_VOffset       = 2;
        constexpr qint16 TabBar_InctiveVShift = 0;

        constexpr qreal DefaultFrame_Radius     = 5.0;
        constexpr qreal TabBarTab_Rounding      = 5.0;
        constexpr qreal SpinBox_Rounding        = 5.0;
        constexpr qreal LineEdit_Rounding       = 5.0;
        constexpr qreal FrameFocusRect_Rounding = 5.0;
        constexpr qreal PushButton_Rounding     = 5.0;
        constexpr qreal ToolButton_Rounding     = 5.0;
        constexpr qreal ProgressBar_Rounding    = 5.0;
        constexpr qreal GroupBox_Rounding       = 5.0;
        constexpr qreal SliderGroove_Rounding   = 5.0;
        constexpr qreal SliderHandle_Rounding   = 5.0;

        constexpr qreal CheckMark_WidthOfHeightScale = 0.8;
        constexpr qreal PushButton_HorizontalPaddingFontHeightRatio = 1.0;
        constexpr qreal TabBar_HPaddingFontRatio            = 1.25;
        constexpr qreal TabBar_VPaddingFontRatio            = 1.0 / 1.25;
        constexpr qreal GroupBox_LabelBottomMarginFontRatio = 1.0 / 4.0;
        constexpr qreal ComboBox_ArrowMarginRatio           = 1.0 / 3.25;

        constexpr qreal MenuBar_HorizontalPaddingFontRatio = 1.0 / 2.0;
        constexpr qreal MenuBar_VerticalPaddingFontRatio   = 1.0 / 3.0;

        constexpr qreal MenuItem_LeftMarginFontRatio          = 1.0 / 2.0;
        constexpr qreal MenuItem_RightMarginForTextFontRatio  = 1.0 / 1.5;
        constexpr qreal MenuItem_RightMarginForArrowFontRatio = 1.0 / 4.0;
        constexpr qreal MenuItem_VerticalMarginsFontRatio     = 1.0 / 5.0;

        // Number that's multiplied with a font's height to get the space between a
        // menu item's checkbox (or other sign) and its text (or icon).
        constexpr qreal MenuItem_CheckRightSpaceFontRatio        = 1.0 / 4.0;
        constexpr qreal MenuItem_TextMnemonicSpaceFontRatio      = 1.5;
        constexpr qreal MenuItem_SubMenuArrowSpaceFontRatio      = 1.0 / 1.5;
        constexpr qreal MenuItem_SubMenuArrowWidthFontRatio      = 1.0 / 2.75;
        constexpr qreal MenuItem_SeparatorHeightFontRatio        = 1.0 / 1.5;
        constexpr qreal MenuItem_CheckMarkVerticalInsetFontRatio = 1.0 / 5.0;
        constexpr qreal MenuItem_IconRightSpaceFontRatio         = 1.0 / 3.0;

        constexpr bool BranchesOnEdge        = false;
        constexpr bool OverhangShadows       = false;
        constexpr bool IndicatorShadows      = false;
        constexpr bool MenuExtraBottomMargin = true;
        constexpr bool MenuBarLeftMargin     = false;
        constexpr bool MenuBarDrawBorder     = false;
        constexpr bool AllowToolBarAutoRaise = true;

        // Note that this only applies to the disclosure etc. decorators in tree views.
        constexpr bool ShowItemViewDecorationSelected          = false;
        constexpr bool UseQMenuForComboBoxPopup                = true;
        constexpr bool ItemView_UseFontHeightForDecorationSize = true;

        struct Grad {
            Grad( const QColor& from, const QColor& to ) {
                rgbA = Rgb::ofQColor( from );
                rgbB = Rgb::ofQColor( to );
                lA   = rgbA.toHsl().l;
                lB   = rgbB.toHsl().l;
            }

            QColor sample( qreal alpha ) const {
                Hsl hsl = Rgb::lerp( rgbA, rgbB, alpha ).toHsl();
                hsl.l = Phantom::lerp( lA, lB, alpha );
                return hsl.toQColor();
            }

            Rgb   rgbA, rgbB;
            qreal lA, lB;
        };

        namespace DeriveColors {
            Q_NEVER_INLINE QColor adjustLightness( const QColor& qcolor, qreal ld ) {
                Hsl         hsl   = Hsl::ofQColor( qcolor );
                const qreal gamma = 3.0;

                hsl.l = std::pow( Phantom::saturate( std::pow( hsl.l, 1.0 / gamma ) + ld * 0.8 ), gamma );
                return hsl.toQColor();
            }


            bool hack_isLightPalette( const QPalette& pal ) {
                Hsl hsl0 = Hsl::ofQColor( pal.color( QPalette::WindowText ) );
                Hsl hsl1 = Hsl::ofQColor( pal.color( QPalette::Window ) );

                return hsl0.l < hsl1.l;
            }


            QColor buttonColor( const QPalette& pal ) {
                // temp hack
                if ( pal.color( QPalette::Button ) == pal.color( QPalette::Window ) ) {
                    return adjustLightness( pal.color( QPalette::Button ), 0.01 );
                }

                return pal.color( QPalette::Button );
            }


            QColor highlightedOutlineOf( const QPalette& pal ) {
                return adjustLightness( pal.color( QPalette::Highlight ), -0.08 );
            }


            QColor dividerColor( const QColor& underlying ) {
                return adjustLightness( underlying, -0.05 );
            }


            QColor lightDividerColor( const QColor& underlying ) {
                return adjustLightness( underlying, 0.02 );
            }


            QColor outlineOf( const QPalette& pal ) {
                return adjustLightness( pal.color( QPalette::Window ), -0.1 );
            }


            QColor gutterColorOf( const QPalette& pal ) {
                return adjustLightness( pal.color( QPalette::Window ), -0.05 );
            }


            QColor darkGutterColorOf( const QPalette& pal ) {
                return adjustLightness( pal.color( QPalette::Window ), -0.08 );
            }


            QColor lightShadeOf( const QColor& underlying ) {
                return adjustLightness( underlying, 0.08 );
            }


            QColor darkShadeOf( const QColor& underlying ) {
                return adjustLightness( underlying, -0.08 );
            }


            QColor overhangShadowOf( const QColor& underlying ) {
                return adjustLightness( underlying, -0.05 );
            }


            QColor sliderGutterShadowOf( const QColor& underlying ) {
                return adjustLightness( underlying, -0.01 );
            }


            QColor specularOf( const QColor& underlying ) {
                return adjustLightness( underlying, 0.01 );
            }


            QColor lightSpecularOf( const QColor& underlying ) {
                return adjustLightness( underlying, 0.05 );
            }


            QColor pressedOf( const QColor& color ) {
                return adjustLightness( color, -0.05 );
            }


            QColor darkPressedOf( const QColor& color ) {
                return adjustLightness( color, -0.08 );
            }


            QColor lightOnOf( const QColor& color ) {
                return adjustLightness( color, -0.04 );
            }


            QColor onOf( const QColor& color ) {
                return adjustLightness( color, -0.08 );
            }


            QColor indicatorColorOf( const QPalette& palette, QPalette::ColorGroup group = QPalette::Current ) {
                if ( hack_isLightPalette( palette ) ) {
                    qreal adjust = (palette.currentColorGroup() == QPalette::Disabled) ? 0.09 : 0.32;
                    return adjustLightness( palette.color( group, QPalette::WindowText ), adjust );
                }

                return adjustLightness( palette.color( group, QPalette::WindowText ), -0.05 );
            }


            QColor inactiveTabFillColorOf( const QColor& underlying ) {
                // used to be -0.01
                return adjustLightness( underlying, -0.025 );
            }


            QColor progressBarOutlineColorOf( const QPalette& pal ) {
                // Pretty wasteful
                Hsl hsl0 = Hsl::ofQColor( pal.color( QPalette::Window ) );
                Hsl hsl1 = Hsl::ofQColor( pal.color( QPalette::Highlight ) );

                hsl1.l = Phantom::saturate( std::min( hsl0.l - 0.1, hsl1.l - 0.2 ) );
                return hsl1.toQColor();
            }


            QColor itemViewMultiSelectionCurrentBorderOf( const QPalette& pal ) {
                return adjustLightness( pal.color( QPalette::Highlight ), -0.15 );
            }


            QColor itemViewHeaderOnLineColorOf( const QPalette& pal ) {
                return hack_isLightPalette( pal )
                           ? highlightedOutlineOf( pal )
                           : Grad( pal.color( QPalette::WindowText ), pal.color( QPalette::Window ) ).sample( 0.5 );
            }
        } // namespace DeriveColors

        namespace SwatchColors {
            enum SwatchColor {
                S_none = 0,
                S_window,
                S_button,
                S_base,
                S_text,
                S_windowText,
                S_highlight,
                S_highlightedText,
                S_scrollbarGutter,
                S_scrollbarSlider,
                S_scrollbarSlider_hover,
                S_scrollbarSlider_pressed,
                S_window_outline,
                S_window_specular,
                S_window_divider,
                S_window_lighter,
                S_window_darker,
                S_frame_outline,
                S_button_specular,
                S_button_pressed,
                S_button_on,
                S_button_pressed_specular,
                S_sliderHandle,
                S_sliderHandle_pressed,
                S_sliderHandle_specular,
                S_sliderHandle_pressed_specular,
                S_base_shadow,
                S_base_divider,
                S_windowText_disabled,
                S_highlight_outline,
                S_highlight_specular,
                S_progressBar_outline,
                S_inactiveTabYesFrame,
                S_inactiveTabNoFrame,
                S_inactiveTabYesFrame_specular,
                S_inactiveTabNoFrame_specular,
                S_indicator_current,
                S_indicator_disabled,
                S_itemView_multiSelection_currentBorder,
                S_itemView_headerOnLine,
                S_scrollbarGutter_disabled,

                // Aliases
                S_progressBar          = S_highlight,
                S_progressBar_specular = S_highlight_specular,
                S_tabFrame             = S_window,
                S_tabFrame_specular    = S_window_specular,
            };
        }

        using Swatchy = SwatchColors::SwatchColor;

        enum {
            Num_SwatchColors = SwatchColors::S_scrollbarGutter_disabled + 1,
            Num_ShadowSteps  = 3,
        };

        struct PhSwatch : public QSharedData {
            // The pens store the brushes within them, so storing the brushes here as
            // well is redundant. However, QPen::brush() does not return its brush by
            // reference, so we'd end up doing a bunch of inc/dec work every time we use
            // one. Also, it saves us the indirection of chasing two pointers (Swatch ->
            // QPen -> QBrush) every time we want to get a QColor.
            QBrush brushes[ Num_SwatchColors ];
            QPen   pens[ Num_SwatchColors ];
            QColor scrollbarShadowColors[ Num_ShadowSteps ];

            // Note: the casts to int in the assert macros are to suppress a false
            // positive warning for tautological comparison in the clang linter.
            inline const QColor& color( Swatchy swatchValue ) const {
                Q_ASSERT( swatchValue >= 0 && static_cast<int>(swatchValue) < Num_SwatchColors );
                return brushes[ swatchValue ].color();
            }

            inline const QBrush& brush( Swatchy swatchValue ) const {
                Q_ASSERT( swatchValue >= 0 && static_cast<int>(swatchValue) < Num_SwatchColors );
                return brushes[ swatchValue ];
            }

            inline const QPen& pen( Swatchy swatchValue ) const {
                Q_ASSERT( swatchValue >= 0 && static_cast<int>(swatchValue) < Num_SwatchColors );
                return pens[ swatchValue ];
            }

            void loadFromQPalette( const QPalette& pal );
        };

        using PhSwatchPtr  = QExplicitlySharedDataPointer<PhSwatch>;
        using PhCacheEntry = QPair<uint, PhSwatchPtr>;
        enum : int {
            Num_ColorCacheEntries = 10,
        };
        using PhSwatchCache = QVarLengthArray<PhCacheEntry, Num_ColorCacheEntries>;
        Q_NEVER_INLINE void PhSwatch::loadFromQPalette( const QPalette& pal ) {
            using namespace SwatchColors;
            namespace Dc = DeriveColors;
            bool   isLight = Dc::hack_isLightPalette( pal );
            QColor colors[ Num_SwatchColors ];
            colors[ S_none ] = QColor();

            colors[ S_window ] = pal.color( QPalette::Window );
            colors[ S_button ] = pal.color( QPalette::Button );

            if ( colors[ S_button ] == colors[ S_window ] ) {
                colors[ S_button ] = Dc::adjustLightness( colors[ S_button ], 0.01 );
            }

            colors[ S_base ]                    = pal.color( QPalette::Base );
            colors[ S_text ]                    = pal.color( QPalette::Text );
            colors[ S_windowText ]              = pal.color( QPalette::WindowText );
            colors[ S_highlight ]               = pal.color( QPalette::Highlight );
            colors[ S_highlightedText ]         = pal.color( QPalette::HighlightedText );
            colors[ S_scrollbarGutter ]         = isLight ? Dc::gutterColorOf( pal ) : Dc::darkGutterColorOf( pal );
            colors[ S_scrollbarSlider ]         = isLight ? colors[ S_button ] : Dc::adjustLightness( colors[ S_window ], 0.1 );
            colors[ S_scrollbarSlider_hover ]   = isLight ? Dc::adjustLightness( colors[ S_button ], -0.1 ) : Dc::adjustLightness( colors[ S_window ], 0.2 );
            colors[ S_scrollbarSlider_pressed ] = isLight ? Dc::adjustLightness( colors[ S_button ], -0.2 ) : Dc::adjustLightness( colors[ S_window ], 0.25 );

            colors[ S_window_outline ] =
                isLight ? Dc::adjustLightness( colors[ S_window ], -0.1 ) : Dc::adjustLightness( colors[ S_window ], 0.03 );
            colors[ S_window_specular ] = Dc::specularOf( colors[ S_window ] );
            colors[ S_window_divider ]  =
                isLight ? Dc::dividerColor( colors[ S_window ] ) : Dc::lightDividerColor( colors[ S_window ] );
            colors[ S_window_lighter ]  = Dc::lightShadeOf( colors[ S_window ] );
            colors[ S_window_darker ]   = Dc::darkShadeOf( colors[ S_window ] );
            colors[ S_frame_outline ]   = isLight ? colors[ S_window_outline ] : Dc::adjustLightness( colors[ S_window ], 0.08 );
            colors[ S_button_specular ] =
                isLight ? Dc::specularOf( colors[ S_button ] ) : Dc::lightSpecularOf( colors[ S_button ] );
            colors[ S_button_pressed ]          = isLight ? Dc::pressedOf( colors[ S_button ] ) : Dc::darkPressedOf( colors[ S_button ] );
            colors[ S_button_on ]               = isLight ? Dc::lightOnOf( colors[ S_button ] ) : Dc::onOf( colors[ S_button ] );
            colors[ S_button_pressed_specular ] =
                isLight ? Dc::specularOf( colors[ S_button_pressed ] ) : Dc::lightSpecularOf( colors[ S_button_pressed ] );

            colors[ S_sliderHandle ]          = isLight ? colors[ S_button ] : Dc::adjustLightness( colors[ S_button ], -0.03 );
            colors[ S_sliderHandle_specular ] =
                isLight ? Dc::specularOf( colors[ S_sliderHandle ] ) : Dc::lightSpecularOf( colors[ S_sliderHandle ] );
            colors[ S_sliderHandle_pressed ] =
                isLight ? colors[ S_button_pressed ] : Dc::adjustLightness( colors[ S_button_pressed ], 0.03 );
            colors[ S_sliderHandle_pressed_specular ] = isLight ? Dc::specularOf( colors[ S_sliderHandle_pressed ] )
                                                              : Dc::lightSpecularOf( colors[ S_sliderHandle_pressed ] );

            colors[ S_base_shadow ]         = Dc::overhangShadowOf( colors[ S_base ] );
            colors[ S_base_divider ]        = colors[ S_window_divider ];
            colors[ S_windowText_disabled ] = pal.color( QPalette::Disabled, QPalette::WindowText );
            colors[ S_highlight_outline ]   = isLight ? Dc::adjustLightness( colors[ S_highlight ], -0.02 )
                                                  : Dc::adjustLightness( colors[ S_highlight ], 0.05 );
            colors[ S_highlight_specular ]                    = Dc::specularOf( colors[ S_highlight ] );
            colors[ S_progressBar_outline ]                   = Dc::progressBarOutlineColorOf( pal );
            colors[ S_inactiveTabYesFrame ]                   = Dc::inactiveTabFillColorOf( colors[ S_tabFrame ] );
            colors[ S_inactiveTabNoFrame ]                    = Dc::inactiveTabFillColorOf( colors[ S_window ] );
            colors[ S_inactiveTabYesFrame_specular ]          = Dc::specularOf( colors[ S_inactiveTabYesFrame ] );
            colors[ S_inactiveTabNoFrame_specular ]           = Dc::specularOf( colors[ S_inactiveTabNoFrame ] );
            colors[ S_indicator_current ]                     = Dc::indicatorColorOf( pal, QPalette::Current );
            colors[ S_indicator_disabled ]                    = Dc::indicatorColorOf( pal, QPalette::Disabled );
            colors[ S_itemView_multiSelection_currentBorder ] = Dc::itemViewMultiSelectionCurrentBorderOf( pal );
            colors[ S_itemView_headerOnLine ]                 = Dc::itemViewHeaderOnLineColorOf( pal );
            colors[ S_scrollbarGutter_disabled ]              = colors[ S_window ];

            brushes[ S_none ] = Qt::NoBrush;
            for (int i = S_none + 1; i < Num_SwatchColors; ++i) {
                // todo try to reuse
                brushes[ i ] = colors[ i ];
            }
            pens[ S_none ] = Qt::NoPen;
            // QPen::setColor constructs a QBrush behind the scenes, so better to just
            // re-use the ones we already made.
            for (int i = S_none + 1; i < Num_SwatchColors; ++i) {
                pens[ i ].setBrush( brushes[ i ] );
                // Width is already 1, don't need to set it. Caps and joins already fine at
                // their defaults, too.
            }

            Grad gutterGrad( Dc::sliderGutterShadowOf( colors[ S_scrollbarGutter ] ), colors[ S_scrollbarGutter ] );
            for (int i = 0; i < Num_ShadowSteps; ++i) {
                scrollbarShadowColors[ i ] = gutterGrad.sample( i / static_cast<qreal>(Num_ShadowSteps) );
            }
        }


        // This is the "hash" (not really a hash) function we'll use on the happy fast
        // path when looking up a PhSwatch for a given QPalette. It's fragile, because
        // it uses QPalette::cacheKey(), so it may not match even when the contents
        // (currentColorGroup + the RGB colors) of the QPalette are actually a match.
        // But it's cheaper to calculate, so we'll store a single one of these "hashes"
        // for the head (most recently used) cached PhSwatch, and check to see if it
        // matches. This is the most common case, so we can usually save some work by
        // doing this. (The second most common case is probably having a different
        // ColorGroup but the rest of the contents are the same, but we don't have a
        // special path for that.)
        inline quint64 fastfragile_hash_qpalette( const QPalette& p ) {
            union {
                qint64  i;
                quint64 u;
            }
            x;
            x.i = p.cacheKey();
            // QPalette::ColorGroup has range 0..5 (inclusive), so it only uses 3 bits.
            // The high 32 bits in QPalette::cacheKey() are a global incrementing serial
            // number for the QPalette creation. We don't store (2^29-1) things in our
            // cache, and I doubt that many will ever be created in a real application
            // while also retaining some of them across such a broad time range, so it's
            // really unlikely that repurposing these top 3 bits to also include the
            // QPalette::currentColorGroup() (which the cacheKey doesn't include for some
            // reason...) will generate a collision.
            //
            // This may not be true in the future if the way the QPalette::cacheKey() is
            // generated changes. If that happens, change to use the definition of
            // `fastfragile_hash_qpalette` below, which is less likely to collide with an
            // arbitrarily numbered key but also does more work.
            #if QT_VERSION < QT_VERSION_CHECK( 6, 0, 0 )
                x.u = x.u ^ (static_cast<quint64>(p.currentColorGroup() ) << (64 - 3) );
                return x.u;

            #else
                // Use this definition here if the contents/layout of QPalette::cacheKey()
                // (as in, the C++ code in qpalette.cpp) are changed. We'll also put a Qt6
                // guard for it, so that it will default to a more safe definition on the
                // next guaranteed big breaking change for Qt. A warning will hopefully get
                // someone to double-check it at some point in the future.
                #warning "Verify contents and layout of QPalette::cacheKey() have not changed"
                QtPrivate::QHashCombine c;
                uint h = qHash( p.currentColorGroup() );
                h = c( h, (uint)(x.u & 0xFFFFFFFFu) );
                h = c( h, (uint)( (x.u >> 32) & 0xFFFFFFFFu) );
                return h;

            #endif
        }


        // This hash function is for when we want an actual accurate hash of a
        // QPalette. QPalette's cacheKey() isn't very reliable -- it seems to change to
        // a new random number whenever it's modified, with the exception of the
        // currentColorGroup being changed. This kind of sucks for us, because it means
        // two QPalette's can have the same contents but hash to different values. And
        // this actually happens a lot! We'll do the hashing ourselves. Also, we're not
        // interested in all of the colors, only some of them, and we ignore
        // pens/brushes.
        uint accurate_hash_qpalette( const QPalette& p ) {
            // Probably shouldn't use this, could replace with our own guy. It's not a
            // great hasher anyway.
            QtPrivate::QHashCombine c;
            uint h = qHash( p.currentColorGroup() );
            QPalette::ColorRole const roles[] = {
                QPalette::Window,
                QPalette::Button,
                QPalette::Base,
                QPalette::Text,
                QPalette::WindowText,
                QPalette::Highlight,
                QPalette::HighlightedText
            };

            for (auto role : roles) {
                h = c( h, p.color( role ).rgb() );
            }
            return h;
        }


        Q_NEVER_INLINE PhSwatchPtr deep_getCachedSwatchOfQPalette( PhSwatchCache *cache,
                                                                   int           cacheCount,   // Just
                                                                                               // saving a
                                                                                               // call to
                                                                                               // cache->count()
                                                                   const QPalette& qpalette ) {
            // Calculate our hash key from the QPalette's current ColorGroup and the
            // actual RGBA values that we use. We have to mix the ColorGroup in
            // ourselves, because QPalette does not account for it in the cache key.
            uint key = accurate_hash_qpalette( qpalette );
            int  n   = cacheCount;
            int  idx = -1;

            for (int i = 0; i < n; ++i) {
                const auto& x = cache->at( i );

                if ( x.first == key ) {
                    idx = i;
                    break;
                }
            }

            if ( idx == -1 ) {
                PhSwatchPtr ptr;

                if ( n < Num_ColorCacheEntries ) {
                    ptr = new PhSwatch;
                }
                else {
                    // Remove the oldest guy from the cache. Remember that because we may
                    // re-enter QStyle functions multiple times when drawing or calculating
                    // something, we may have to load several swaitches derived from
                    // different QPalettes on different stack frames at the same time. But as
                    // an extra cost-savings measure, we'll check and see if something else
                    // has a reference to the removed guy. If there aren't any references to
                    // it, then we'll re-use it directly instead of allocating a new one. (We
                    // will only ever run into the case where we can't re-use it directly if
                    // some other stack frame has a reference to it.) This is nice because
                    // then the QPens and QBrushes don't all also have to reallocate their d
                    // ptr stuff.
                    ptr = cache->last().second;
                    cache->removeLast();
                    ptr.detach();
                }

                ptr->loadFromQPalette( qpalette );
                #if QT_VERSION < QT_VERSION_CHECK( 6, 0, 0 )
                    cache->prepend( PhCacheEntry( key, ptr ) );
                #else
                    cache->insert( cache->cbegin(), std::make_pair( key, ptr ) );
                #endif
                return ptr;
            }
            else {
                if ( idx == 0 ) {
                    return cache->at( idx ).second;
                }

                PhCacheEntry e = cache->at( idx );
                // Using std::move from algorithm could be more efficient here, but I don't
                // want to depend on algorithm or write this myself. Small N with a movable
                // type means it doesn't really matter in this case.
                cache->remove( idx );
                // cache->prepend( e );
                cache->insert( cache->cbegin(), e );
                return e.second;
            }
        }


        Q_NEVER_INLINE PhSwatchPtr getCachedSwatchOfQPalette( PhSwatchCache *cache,
                                                              quint64       *headSwatchFastKey,   // Optimistic
                                                                                                  // fast-path
                                                                                                  // quick
                                                                                                  // hash
                                                                                                  // key
                                                              const QPalette& qpalette ) {
            quint64 ck         = fastfragile_hash_qpalette( qpalette );
            int     cacheCount = cache->count();

            // This hint is counter-productive if we're being called in a way that
            // interleaves different QPalettes. But misses to this optimistic path were
            // rare in my tests. (Probably not going to amount to any significant
            // difference, anyway.)
            if ( Q_LIKELY( (cacheCount > 0) && (*headSwatchFastKey == ck) ) ) {
                return cache->at( 0 ).second;
            }

            *headSwatchFastKey = ck;
            return deep_getCachedSwatchOfQPalette( cache, cacheCount, qpalette );
        }
    } // namespace
}     // namespace Phantom

class OnyxStylePrivate {
    public:
        OnyxStylePrivate();

        // A fast'n'easy hash of QPalette::cacheKey()+QPalette::currentColorGroup()
        // of only the head element of swatchCache list. The most common thing that
        // happens when deriving a PhSwatch from a QPalette is that we just end up
        // re-using the last one that we used. For that case, we can potentially save
        // calling `accurate_hash_qpalette()` and instead use the value returned by
        // QPalette::cacheKey() (and QPalette::currentColorGroup()) and compare it to
        // the last one that we used. If it matches, then we know we can just use the
        // head of the cache list without having to do any further checks, which
        // saves a few hundred (!) nanoseconds.
        //
        // However, the `QPalette::cacheKey()` value is fragile and may change even
        // if none of the colors in the QPalette have changed. In other words, all of
        // the colors in a QPalette may match another QPalette (or a derived
        // PhSwatch) even if the `QPalette::cacheKey()` value is different.
        //
        // So if `QPalette::cacheKey()+currentColorGroup()` doesn't match, then we'll
        // use our more accurate `accurate_hash_qpalette()` to get a more accurate
        // comparison key, and then search through the cache list to find a matching
        // cached PhSwatch. (The more accurate cache key is what we store alongside
        // each PhSwatch element, as the `.first` in each QPair. The
        // QPalette::cacheKey() that we associate with the PhSwatch in the head
        // position, `headSwatchFastKey`, is only stored for our single head element,
        // as a special fast case.) If we find it, we'll move it to the head of the
        // cache list. If not, we'll make a new one, and put it at the head. Either
        // way, the `headSwatchFastKey` will be updated to the
        // `fastfragile_qpalette_hash()` of the QPalette that we needed to derive a
        // PhSwatch from, so that if we get called with the same QPalette again next
        // time (which is probably going to be the case), it'll match and we can take
        // the fast path.
        quint64 headSwatchFastKey;

        Phantom::PhSwatchCache swatchCache;
        QPen checkBox_pen_scratch;
};

namespace Phantom {
    namespace {
        // Minimal QPainter save/restore just for pen, brush, and AA render hint. If
        // you touch more than that, this won't help you. But if you're only touching
        // those things, this will save you some typing from manually storing/saving
        // those properties each time.
        struct PSave final {
            Q_DISABLE_COPY( PSave )

            explicit PSave( QPainter *painter_ ) {
                Q_ASSERT( painter_ );
                painter = painter_;
                pen     = painter_->pen();
                brush   = painter_->brush();
                hintAA  = painter_->testRenderHint( QPainter::Antialiasing );
            }

            Q_NEVER_INLINE void restore() {
                QPainter *p = painter;

                if ( !p ) {
                    return;
                }

                bool hintAA_ = hintAA;
                // QPainter will check both pen and brush for equality when setting, so we
                // should set it unconditionally here.
                p->setPen( pen );
                p->setBrush( brush );

                // But it won't check the render hint to guard against doing extra work.
                // We'll do that ourselves. (Though at least for the raster engine, this
                // doesn't cause very much work to occur. But it still chases a few
                // pointers.)
                if ( p->testRenderHint( QPainter::Antialiasing ) != hintAA_ ) {
                    p->setRenderHint( QPainter::Antialiasing, hintAA_ );
                }

                painter = nullptr;
                pen     = QPen();
                brush   = QBrush();
                hintAA  = false;
            }

            ~PSave() {
                restore();
            }

            private:
                QPainter *painter;
                QPen     pen;
                QBrush   brush;
                bool     hintAA;
        };

        const qreal Pi = M_PI;

        qreal dpiScaled( qreal value ) {
            #ifdef Q_OS_MAC
                // On mac the DPI is always 72 so we should not scale it
                return value;

            #else
                const qreal scale = qt_defaultDpiX() / 96.0;
                return value * scale;

            #endif
        }


        struct MenuItemMetrics {
            int fontHeight;
            int frameThickness;
            int leftMargin;
            int rightMarginForText;
            int rightMarginForArrow;
            int topMargin;
            int bottomMargin;
            int checkWidth;
            int checkRightSpace;
            int iconRightSpace;
            int mnemonicSpace;
            int arrowSpace;
            int arrowWidth;
            int separatorHeight;
            int totalHeight;

            static MenuItemMetrics ofFontHeight( int fontHeight );

            private:
                MenuItemMetrics() {
                }
        };

        MenuItemMetrics MenuItemMetrics::ofFontHeight( int fontHeight ) {
            MenuItemMetrics m;

            m.fontHeight          = fontHeight;
            m.frameThickness      = dpiScaled( 1.0 );
            m.leftMargin          = static_cast<int>(fontHeight * MenuItem_LeftMarginFontRatio);
            m.rightMarginForText  = static_cast<int>(fontHeight * MenuItem_RightMarginForTextFontRatio);
            m.rightMarginForArrow = static_cast<int>(fontHeight * MenuItem_RightMarginForArrowFontRatio);
            m.topMargin           = static_cast<int>(fontHeight * MenuItem_VerticalMarginsFontRatio);
            m.bottomMargin        = static_cast<int>(fontHeight * MenuItem_VerticalMarginsFontRatio);
            int checkVMargin = static_cast<int>(fontHeight * MenuItem_CheckMarkVerticalInsetFontRatio);
            int checkHeight  = fontHeight - checkVMargin * 2;

            if ( checkHeight < 0 ) {
                checkHeight = 0;
            }

            m.checkWidth      = static_cast<int>(checkHeight * CheckMark_WidthOfHeightScale);
            m.checkRightSpace = static_cast<int>(fontHeight * MenuItem_CheckRightSpaceFontRatio);
            m.iconRightSpace  = static_cast<int>(fontHeight * MenuItem_IconRightSpaceFontRatio);
            m.mnemonicSpace   = static_cast<int>(fontHeight * MenuItem_TextMnemonicSpaceFontRatio);
            m.arrowSpace      = static_cast<int>(fontHeight * MenuItem_SubMenuArrowSpaceFontRatio);
            m.arrowWidth      = static_cast<int>(fontHeight * MenuItem_SubMenuArrowWidthFontRatio);
            m.separatorHeight = static_cast<int>(fontHeight * MenuItem_SeparatorHeightFontRatio);
            // Odd numbers only
            m.separatorHeight = (m.separatorHeight / 2) * 2 + 1;
            m.totalHeight     = fontHeight + m.frameThickness * 2 + m.topMargin + m.bottomMargin;
            return m;
        }


        QRect menuItemContentRect( const MenuItemMetrics& metrics, QRect itemRect, bool hasArrow ) {
            QRect r  = itemRect;
            int   ft = metrics.frameThickness;
            int   rm = hasArrow ? metrics.rightMarginForArrow : metrics.rightMarginForText;

            r.adjust( ft + metrics.leftMargin, ft + metrics.topMargin, -(ft + rm), -(ft + metrics.bottomMargin) );
            return r.isValid() ? r : QRect();
        }


        QRect menuItemCheckRect( const MenuItemMetrics& metrics, Qt::LayoutDirection direction, QRect itemRect, bool hasArrow ) {
            QRect r            = menuItemContentRect( metrics, itemRect, hasArrow );
            int   checkVMargin = static_cast<int>(metrics.fontHeight * MenuItem_CheckMarkVerticalInsetFontRatio);

            if ( checkVMargin < 0 ) {
                checkVMargin = 0;
            }

            r.setSize( QSize( metrics.checkWidth, metrics.fontHeight ) );
            r.adjust( 0, checkVMargin, 0, -checkVMargin );
            return QStyle::visualRect( direction, itemRect, r ) & itemRect;
        }


        QPixmap getIconPixmap( QIcon icon, QWindow *window, QSize size, QIcon::Mode mode, QIcon::State state = QIcon::State::Off ) {
            #if QT_VERSION < QT_VERSION_CHECK( 6, 0, 0 )
                return icon.pixmap( window, size, mode, state );

            #else
                return icon.pixmap( size, (window ? window->devicePixelRatio() : 1.0), mode, state );

            #endif
        }


        QRect menuItemIconRect( const MenuItemMetrics& metrics, Qt::LayoutDirection direction, QRect itemRect, bool hasArrow ) {
            QRect r = menuItemContentRect( metrics, itemRect, hasArrow );

            r.setX( r.x() + metrics.checkWidth + metrics.checkRightSpace );
            r.setSize( QSize( metrics.fontHeight, metrics.fontHeight ) );
            return QStyle::visualRect( direction, itemRect, r ) & itemRect;
        }


        QRect menuItemTextRect( const MenuItemMetrics& metrics, Qt::LayoutDirection direction, QRect itemRect, bool hasArrow, bool hasIcon, int tabWidth ) {
            QRect r = menuItemContentRect( metrics, itemRect, hasArrow );

            r.setX( r.x() + metrics.checkWidth + metrics.checkRightSpace );

            if ( hasIcon ) {
                r.setX( r.x() + metrics.fontHeight + metrics.iconRightSpace );
            }

            r.setWidth( r.width() - tabWidth );
            r.setHeight( metrics.fontHeight );
            r &= itemRect;
            return QStyle::visualRect( direction, itemRect, r );
        }


        QRect menuItemMnemonicRect( const MenuItemMetrics& metrics, Qt::LayoutDirection direction, QRect itemRect, bool hasArrow, int tabWidth ) {
            QRect r = menuItemContentRect( metrics, itemRect, hasArrow );
            int   x = r.x() + r.width() - tabWidth;

            if ( hasArrow ) {
                x -= metrics.arrowSpace + metrics.arrowWidth;
            }

            r.setX( x );
            r.setHeight( metrics.fontHeight );
            r &= itemRect;
            return QStyle::visualRect( direction, itemRect, r );
        }


        QRect menuItemArrowRect( const MenuItemMetrics& metrics, Qt::LayoutDirection direction, QRect itemRect ) {
            QRect r = menuItemContentRect( metrics, itemRect, true );
            int   x = r.x() + r.width() - metrics.arrowWidth;

            r.setX( x );
            r &= itemRect;
            return QStyle::visualRect( direction, itemRect, r );
        }


        Q_NEVER_INLINE
        void progressBarFillRects( const QStyleOptionProgressBar *bar,
                                   // The rect that represents the filled/completed region
                                   QRect&                        outFilled,
                                   // The rect that represents the incomplete region
                                   QRect&                        outNonFilled,
                                   // Whether or not the progress bar is indeterminate
                                   bool&                         outIsIndeterminate,
                                   // Whether or not the progress bar is vertical
                                   bool&                         isVertical ) {
            QRect ra              = bar->rect;
            QRect rb              = ra;
            bool  isHorizontal    = !isVertical;
            bool  isInverted      = bar->invertedAppearance;
            bool  isIndeterminate = bar->minimum == 0 && bar->maximum == 0;
            bool  isForward       = !isHorizontal || bar->direction != Qt::RightToLeft;

            if ( isInverted ) {
                isForward = !isForward;
            }

            int        maxLen     = isHorizontal ? ra.width() : ra.height();
            const auto availSteps = std::max( Q_INT64_C( 1 ), qint64( bar->maximum ) - bar->minimum );
            const auto progress   = std::max( bar->progress, bar->minimum );       // workaround for bug in
            // QProgressBar
            const auto progressSteps    = qint64( progress ) - bar->minimum;
            const auto progressBarWidth = progressSteps * maxLen / availSteps;
            int        barLen           = isIndeterminate ? maxLen : progressBarWidth;

            if ( isHorizontal ) {
                if ( isForward ) {
                    ra.setWidth( barLen );
                    rb.setX( barLen );
                }
                else {
                    ra.setX( ra.x() + ra.width() - barLen );
                    rb.setWidth( rb.width() - barLen );
                }
            }
            else {
                if ( isForward ) {
                    ra.setY( ra.y() + ra.height() - barLen );
                    rb.setHeight( rb.height() - barLen );
                }
                else {
                    ra.setHeight( barLen );
                    rb.setY( barLen );
                }
            }

            outFilled          = ra;
            outNonFilled       = rb;
            outIsIndeterminate = isIndeterminate;
        }


        int calcBigLineSize( int radius ) {
            int bigLineSize = radius / 6;

            if ( bigLineSize < 4 ) {
                bigLineSize = 4;
            }

            if ( bigLineSize > radius / 2 ) {
                bigLineSize = radius / 2;
            }

            return bigLineSize;
        }


        Q_NEVER_INLINE QPointF calcRadialPos( const QStyleOptionSlider *dial, qreal offset ) {
            const int width  = dial->rect.width();
            const int height = dial->rect.height();
            const int r      = std::min( width, height ) / 2;
            const int currentSliderPosition =
                dial->upsideDown ? dial->sliderPosition : (dial->maximum - dial->sliderPosition);
            qreal a = 0;

            if ( dial->maximum == dial->minimum ) {
                a = Pi / 2;
            }
            else if ( dial->dialWrapping ) {
                a = Pi * 3 / 2 - (currentSliderPosition - dial->minimum) * 2 * Pi / (dial->maximum - dial->minimum);
            }
            else {
                a = (Pi * 8 - (currentSliderPosition - dial->minimum) * 10 * Pi / (dial->maximum - dial->minimum) ) / 6;
            }

            qreal   xc   = width / 2.0;
            qreal   yc   = height / 2.0;
            qreal   len  = r - calcBigLineSize( r ) - 3;
            qreal   back = offset * len;
            QPointF pos( QPointF( xc + back * qCos( a ), yc - back * qSin( a ) ) );
            return pos;
        }


        Q_NEVER_INLINE QPolygonF calcLines( const QStyleOptionSlider *dial ) {
            QPolygonF poly;
            qreal     width       = dial->rect.width();
            qreal     height      = dial->rect.height();
            qreal     r           = std::min( width, height ) / 2.0;
            int       bigLineSize = calcBigLineSize( r );

            qreal     xc = width / 2.0 + 0.5;
            qreal     yc = height / 2.0 + 0.5;
            const int ns = dial->tickInterval;

            if ( !ns ) { // Invalid values may be set by Qt Designer.
                return poly;
            }

            int notches = (dial->maximum + ns - 1 - dial->minimum) / ns;

            if ( notches <= 0 ) {
                return poly;
            }

            if ( (dial->maximum < dial->minimum) || (dial->maximum - dial->minimum > 1000) ) {
                int maximum = dial->minimum + 1000;
                notches = (maximum + ns - 1 - dial->minimum) / ns;
            }

            poly.resize( 2 + 2 * notches );
            int smallLineSize = bigLineSize / 2;
            for (int i = 0; i <= notches; ++i) {
                qreal angle =
                    dial->dialWrapping ? Pi * 3 / 2 - i * 2 * Pi / notches : (Pi * 8 - i * 10 * Pi / notches) / 6;
                qreal s = qSin( angle );
                qreal c = qCos( angle );

                if ( (i == 0) || ( ( (ns * i) % (dial->pageStep ? dial->pageStep : 1) ) == 0) ) {
                    poly[ 2 * i ]     = QPointF( xc + (r - bigLineSize) * c, yc - (r - bigLineSize) * s );
                    poly[ 2 * i + 1 ] = QPointF( xc + r * c, yc - r * s );
                }
                else {
                    poly[ 2 * i ]     = QPointF( xc + (r - 1 - smallLineSize) * c, yc - (r - 1 - smallLineSize) * s );
                    poly[ 2 * i + 1 ] = QPointF( xc + (r - 1) * c, yc - (r - 1) * s );
                }
            }
            return poly;
        }


        // This will draw a nice and shiny QDial for us. We don't want
        // all the shinyness in QWindowsStyle, hence we place it here
        Q_NEVER_INLINE void drawDial( const QStyleOptionSlider *option, QPainter *painter ) {
            namespace Dc = Phantom::DeriveColors;
            const QPalette& pal         = option->palette;
            QColor          buttonColor = Dc::buttonColor( option->palette );
            const int       width       = option->rect.width();
            const int       height      = option->rect.height();
            const bool      enabled     = option->state & QStyle::State_Enabled;
            qreal           r           = std::min( width, height ) / 2.0;
            r -= r / 50.0;
            painter->save();
            painter->setRenderHint( QPainter::Antialiasing );

            // Draw notches
            if ( option->subControls & QStyle::SC_DialTickmarks ) {
                painter->setPen( pal.color( QPalette::Disabled, QPalette::Text ) );
                painter->drawLines( calcLines( option ) );
            }

            const qreal d_ = r / 6;
            const qreal dx = option->rect.x() + d_ + (width - 2 * r) / 2 + 1;
            const qreal dy = option->rect.y() + d_ + (height - 2 * r) / 2 + 1;
            QRectF      br = QRectF( dx + 0.5, dy + 0.5, int(r * 2 - 2 * d_ - 2), int(r * 2 - 2 * d_ - 2) );

            if ( enabled ) {
                painter->setBrush( buttonColor );
            }
            else {
                painter->setBrush( Qt::NoBrush );
            }

            painter->setPen( Dc::outlineOf( option->palette ) );
            painter->drawEllipse( br );
            painter->setBrush( Qt::NoBrush );
            painter->setPen( Dc::specularOf( buttonColor ) );
            painter->drawEllipse( br.adjusted( 1, 1, -1, -1 ) );

            if ( option->state & QStyle::State_HasFocus ) {
                QColor highlight = pal.highlight().color();
                highlight.setHsv( highlight.hue(), std::min( 160, highlight.saturation() ), std::max( 230, highlight.value() ) );
                highlight.setAlpha( 127 );
                painter->setPen( QPen( highlight, 2.0 ) );
                painter->setBrush( Qt::NoBrush );
                painter->drawEllipse( br.adjusted( -1, -1, 1, 1 ) );
            }

            QPointF     dp = calcRadialPos( option, 0.70 );
            const qreal ds = r / 7.0;
            QRectF      dialRect( dp.x() - ds, dp.y() - ds, 2 * ds, 2 * ds );
            painter->setBrush( option->palette.color( QPalette::Window ) );
            painter->setPen( Dc::outlineOf( option->palette ) );
            painter->drawEllipse( dialRect.adjusted( -1, -1, 1, 1 ) );
            painter->restore();
        }


        int fontMetricsWidth( const QFontMetrics& fontMetrics, const QString& text ) {
            return fontMetrics.horizontalAdvance( text );
        }


        // All arrows will be fixed size: 8 x 4, drawn with 2px lines
        Q_NEVER_INLINE void drawArrow( QPainter *p, QRect rect, Qt::ArrowType arrowDirection, const QBrush& brush ) {
            QImage img( 10, 10, QImage::Format_ARGB32 );

            img.fill( Qt::transparent );

            QPainter pt( &img );

            pt.setPen( Qt::NoPen );
            pt.setBrush( Qt::transparent );

            pt.setRenderHints( QPainter::Antialiasing );
            pt.setPen( QPen( brush.color(), 2.0, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );

            switch ( arrowDirection ) {
                case Qt::UpArrow: {
                    pt.drawLine( QPoint( 1, 7 ), QPoint( 5, 3 ) );
                    pt.drawLine( QPoint( 5, 3 ), QPoint( 9, 7 ) );

                    break;
                }

                case Qt::DownArrow: {
                    pt.drawLine( QPoint( 1, 3 ), QPoint( 5, 7 ) );
                    pt.drawLine( QPoint( 5, 7 ), QPoint( 9, 3 ) );

                    break;
                }

                case Qt::LeftArrow: {
                    pt.drawLine( QPoint( 7, 1 ), QPoint( 3, 5 ) );
                    pt.drawLine( QPoint( 3, 5 ), QPoint( 7, 9 ) );

                    break;
                }

                case Qt::RightArrow: {
                    pt.drawLine( QPoint( 3, 1 ), QPoint( 7, 5 ) );
                    pt.drawLine( QPoint( 7, 5 ), QPoint( 3, 9 ) );

                    break;
                }

                case Qt::NoArrow: {
                    break;
                }
            }

            pt.end();

            int x = (rect.width() - 10) / 2;
            int y = (rect.height() - 10) / 2;

            p->drawImage( QRect( rect.x() + x, rect.y() + y, 10, 10 ), img );
        }


        // Pass allowEnabled as false to always draw the arrow with the disabled color,
        // even if the underlying palette's current color group is not disabled. Useful
        // for parts of widgets which may want to be drawn as disabled even if the
        // actual widget is not set as disabled, such as scrollbar step buttons when
        // the scrollbar has no movable range.
        Q_NEVER_INLINE void drawArrow( QPainter *painter, QRect rect, Qt::ArrowType type, const PhSwatch& swatch, bool allowEnabled = true, qreal lightnessAdjustment = 0.0 ) {
            if ( rect.isEmpty() ) {
                return;
            }

            using namespace SwatchColors;
            auto brush = swatch.brush( allowEnabled ? S_indicator_current : S_indicator_disabled );
            brush.setColor( DeriveColors::adjustLightness( brush.color(), lightnessAdjustment ) );
            Phantom::drawArrow( painter, rect, type, brush );
        }


        // This draws exactly within the rect provided. If you provide a square rect,
        // it will appear too wide -- you probably want to shrink the width of your
        // square first by multiplying it with CheckMark_WidthOfHeightScale.
        Q_NEVER_INLINE void drawCheck( QPainter *painter, QPen& scratchPen, const QRectF& r, const PhSwatch& swatch, Swatchy color ) {
            using namespace Phantom::SwatchColors;
            qreal rx, ry, rw, rh;
            QRectF( r ).getRect( &rx, &ry, &rw, &rh );
            qreal penWidth = 0.25 * std::min( rw, rh );
            qreal dimx     = rw - penWidth;
            qreal dimy     = rh - penWidth;

            if ( (dimx < 0.5) || (dimy < 0.5) ) {
                return;
            }

            qreal   x = (rw - dimx) / 2 + rx;
            qreal   y = (rh - dimy) / 2 + ry;
            QPointF points[ 3 ];
            points[ 0 ] = QPointF( 0.0, 0.55 );
            points[ 1 ] = QPointF( 0.4, 1.0 );
            points[ 2 ] = QPointF( 1.0, 0 );
            for (int i = 0; i < 3; ++i) {
                QPointF pnt = points[ i ];
                pnt.setX( pnt.x() * dimx + x );
                pnt.setY( pnt.y() * dimy + y );
                points[ i ] = pnt;
            }
            scratchPen.setBrush( swatch.brush( color ) );
            scratchPen.setCapStyle( Qt::RoundCap );
            scratchPen.setJoinStyle( Qt::RoundJoin );
            scratchPen.setWidthF( penWidth );
            Phantom::PSave save( painter );

            if ( !painter->testRenderHint( QPainter::Antialiasing ) ) {
                painter->setRenderHint( QPainter::Antialiasing );
            }

            painter->setPen( scratchPen );
            painter->setBrush( Qt::NoBrush );
            painter->drawPolyline( points, 3 );
        }


        Q_NEVER_INLINE void drawMdiButton( QPainter *painter, QRect tmp, bool hover, bool sunken, QColor baseColor ) {
            painter->save();
            painter->setRenderHints( QPainter::Antialiasing );

            /** Mouse inside button */
            if ( sunken ) {
                painter->setPen( QPen( baseColor.darker( 150 ), 1.0 ) );
                painter->setBrush( baseColor.darker( 120 ) );
            }

            /** Button pressed */
            else if ( hover ) {
                painter->setPen( QPen( baseColor.darker( 130 ), 1.0 ) );
                painter->setBrush( baseColor );
            }

            /** Otherwise */
            else {
                painter->setPen( QPen( Qt::darkGray, 1.0 ) );
                painter->setBrush( Qt::gray );
            }

            painter->drawEllipse( QRectF( tmp ).adjusted( 3.5, 3.5, -3.5, -3.5 ) );
            painter->restore();
        }


        Q_NEVER_INLINE void drawTabShape( QPainter *painter, QRectF rect, Qt::Edge edge, bool first, bool last, bool onlyOne, bool hasFrame, const QBrush& brush ) {
            bool tl = false;
            bool tr = false;
            bool br = false;
            bool bl = false;

            switch ( edge ) {
                case Qt::LeftEdge: {
                    tl = (first || onlyOne ? true : false);
                    tr = (hasFrame && (onlyOne || first) ? false : true);
                    br = (hasFrame && (onlyOne || last) ? false : true);
                    bl = (last || onlyOne ? true : false);

                    rect.adjust( 0, 0, -2, 0 );

                    break;
                }

                case Qt::TopEdge: {
                    tl = (first || onlyOne ? true : false);
                    tr = (last || onlyOne ? true : false);
                    br = (!hasFrame && (onlyOne || last) ? true : false);
                    bl = (!hasFrame && (onlyOne || first) ? true : false);

                    rect.adjust( 0, 0, 0, -2 );

                    break;
                }

                case Qt::RightEdge: {
                    tl = (!hasFrame && (onlyOne || first) ? true : false);
                    tr = (first || onlyOne ? true : false);
                    br = (last || onlyOne ? true : false);
                    bl = (!hasFrame && (onlyOne || last) ? true : false);

                    rect.adjust( 2, 0, 0, 0 );

                    break;
                }

                case Qt::BottomEdge: {
                    tl = (!hasFrame && (first || onlyOne) ? true : false);
                    tr = (!hasFrame && (last || onlyOne) ? true : false);
                    br = (last || onlyOne ? true : false);
                    bl = (first || onlyOne ? true : false);

                    rect.adjust( 0, 2, 0, 0 );

                    break;
                }
            }

            painter->save();
            painter->setRenderHints( QPainter::Antialiasing );
            painter->setPen( Qt::NoPen );

            qreal radius = 10.0;

            /** Simplification in case no corners are rounded */
            if ( !tl && !tr && !br && !bl ) {
                painter->setBrush( brush );
                painter->drawRect( QRectF( rect ) );
            }

            else {
                QPainterPath pp;
                pp.setFillRule( Qt::WindingFill );

                /** Add a normal rounded rect */
                pp.addRoundedRect( QRectF( rect ), radius, radius );

                /** Top-left corner is not to be rounded, so square it */
                if ( tl == false ) {
                    pp.addRect( QRectF( rect.topLeft(), QSize( radius, radius ) ) );
                }

                /** Top-right corner is not to be rounded, so square it */
                if ( tr == false ) {
                    pp.addRect( QRectF( rect.topRight() - QPoint( radius, 0 ), QSize( radius, radius ) ) );
                }

                /** Bottom-right corner is not to be rounded, so square it */
                if ( br == false ) {
                    pp.addRect( QRectF( rect.bottomRight() - QPoint( radius, radius ), QSize( radius, radius ) ) );
                }

                /** Bottom-left corner is not to be rounded, so square it */
                if ( bl == false ) {
                    pp.addRect( QRectF( rect.bottomLeft() - QPoint( 0, radius ), QSize( radius, radius ) ) );
                }

                painter->setRenderHints( QPainter::Antialiasing );
                painter->setBrush( brush );
                painter->drawPath( pp.simplified() );
            }

            painter->restore();
        }


        Q_NEVER_INLINE void drawSelectiveRoundedRect( QPainter *painter, QRectF rect, bool tl, bool tr, bool br, bool bl, qreal radius, const QPen& pen, const QBrush& brush ) {
            painter->save();
            painter->setRenderHints( QPainter::Antialiasing );

            qreal adjust = 0.0;

            if ( pen.style() != Qt::NoPen ) {
                adjust = pen.widthF() / 2.0;
            }

            /** Simplification in case no corners are rounded */
            if ( !tl && !tr && !br && !bl ) {
                painter->setPen( pen );
                painter->setBrush( brush );
                painter->drawRect( QRectF( rect ).adjusted( adjust, adjust, -adjust, -adjust ) );
            }

            else {
                QPainterPath pp;
                pp.setFillRule( Qt::WindingFill );

                /** Add a normal rounded rect */
                pp.addRoundedRect( QRectF( rect ).adjusted( adjust, adjust, -adjust, -adjust ), radius, radius );

                /** Top-left corner is not to be rounded, so square it */
                if ( tl == false ) {
                    pp.addRect( QRectF( rect.topLeft(), QSize( radius, radius ) ) );
                }

                /** Top-right corner is not to be rounded, so square it */
                if ( tr == false ) {
                    pp.addRect( QRectF( rect.topRight() - QPoint( radius - 1, 0 ), QSize( radius, radius ) ) );
                }

                /** Bottom-right corner is not to be rounded, so square it */
                if ( br == false ) {
                    pp.addRect( QRectF( rect.bottomRight() - QPoint( radius - 1, radius - 1 ), QSize( radius, radius ) ) );
                }

                /** Bottom-left corner is not to be rounded, so square it */
                if ( bl == false ) {
                    pp.addRect( QRectF( rect.bottomLeft() - QPoint( 0, radius - 1 ), QSize( radius, radius ) ) );
                }

                painter->setRenderHints( QPainter::Antialiasing );
                painter->setPen( pen );
                painter->setBrush( brush );
                painter->drawPath( pp.simplified() );
            }

            painter->restore();
        }


        typedef struct rounding_t {
            bool  tl     = true;
            bool  tr     = true;
            bool  br     = true;
            bool  bl     = true;
            qreal radius = 5.0;

            rounding_t() {
            }

            rounding_t( bool _tl, bool _tr, bool _br, bool _bl, qreal _r ) {
                tl     = _tl;
                tr     = _tr;
                br     = _br;
                bl     = _bl;
                radius = _r;
            }
        } Rounding;

        Q_NEVER_INLINE void drawRoundedRect( QPainter *painter, QRectF rect, Rounding rInfo, const QPen& pen, const QBrush& brush, bool shadow = false ) {
            Q_UNUSED( shadow );
            painter->save();
            painter->setRenderHints( QPainter::Antialiasing );

            qreal adjust = 0.0;

            if ( pen.style() != Qt::NoPen ) {
                adjust = pen.widthF() / 2.0;
            }

            /**
             * Simplification in case no corners are rounded
             * We will draw shadows here!
             */
            if ( !rInfo.tl && !rInfo.tr && !rInfo.br && !rInfo.bl ) {
                painter->setPen( pen );
                painter->setBrush( brush );
                painter->drawRect( QRectF( rect ).adjusted( adjust, adjust, -adjust, -adjust ) );
            }

            /**
             * Simplification in case all corners are rounded.
             * We will draw shadows here!
             */
            else if ( rInfo.tl && rInfo.tr && rInfo.br && rInfo.bl ) {
                painter->setPen( pen );
                painter->setBrush( brush );
                painter->drawRoundedRect( QRectF( rect ).adjusted( adjust, adjust, -adjust, -adjust ), rInfo.radius, rInfo.radius );
            }

            else {
                QPainterPath pp;
                pp.setFillRule( Qt::WindingFill );

                /** Add a normal rounded rect */
                pp.addRoundedRect( QRectF( rect ).adjusted( adjust, adjust, -adjust, -adjust ), rInfo.radius, rInfo.radius );

                /** Top-left corner is not to be rounded, so square it */
                if ( rInfo.tl == false ) {
                    pp.addRect( QRectF( rect.topLeft(), QSize( rInfo.radius, rInfo.radius ) ) );
                }

                /** Top-right corner is not to be rounded, so square it */
                if ( rInfo.tr == false ) {
                    pp.addRect( QRectF( rect.topRight() - QPoint( rInfo.radius, 0 ), QSize( rInfo.radius, rInfo.radius ) ) );
                }

                /** Bottom-right corner is not to be rounded, so square it */
                if ( rInfo.br == false ) {
                    pp.addRect( QRectF( rect.bottomRight() - QPoint( rInfo.radius, rInfo.radius ), QSize( rInfo.radius, rInfo.radius ) ) );
                }

                /** Bottom-left corner is not to be rounded, so square it */
                if ( rInfo.bl == false ) {
                    pp.addRect( QRectF( rect.bottomLeft() - QPoint( 0, rInfo.radius ), QSize( rInfo.radius, rInfo.radius ) ) );
                }

                painter->setRenderHints( QPainter::Antialiasing );
                painter->setPen( pen );
                painter->setBrush( brush );
                painter->drawPath( pp.simplified() );
            }

            painter->restore();
        }


        Q_NEVER_INLINE void fillRectOutline( QPainter *p, QRect rect, QMargins margins, const QColor& brush ) {
            int x, y, w, h;

            rect.getRect( &x, &y, &w, &h );
            int   ml = margins.left();
            int   mt = margins.top();
            int   mr = margins.right();
            int   mb = margins.bottom();
            QRect r0( x, y, w, mt );
            QRect r1( x, y + mt, ml, h - (mt + mb) );
            QRect r2( (x + w) - mr, y + mt, mr, h - (mt + mb) );
            QRect r3( x, (y + h) - mb, w, mb );
            p->fillRect( r0 & rect, brush );
            p->fillRect( r1 & rect, brush );
            p->fillRect( r2 & rect, brush );
            p->fillRect( r3 & rect, brush );
        }


        void fillRectOutline( QPainter *p, QRect rect, int thickness, const QColor& color ) {
            fillRectOutline( p, rect, QMargins( thickness, thickness, thickness, thickness ), color );
        }


        Q_NEVER_INLINE void fillRectEdges( QPainter *p, QRect rect, Qt::Edges edges, QMargins margins, const QColor& color ) {
            int x, y, w, h;

            rect.getRect( &x, &y, &w, &h );

            if ( edges & Qt::LeftEdge ) {
                int   ml = margins.left();
                QRect r0( x, y, ml, h );
                p->fillRect( r0 & rect, color );
            }

            if ( edges & Qt::TopEdge ) {
                int   mt = margins.top();
                QRect r1( x, y, w, mt );
                p->fillRect( r1 & rect, color );
            }

            if ( edges & Qt::RightEdge ) {
                int   mr = margins.right();
                QRect r2( (x + w) - mr, y, mr, h );
                p->fillRect( r2 & rect, color );
            }

            if ( edges & Qt::BottomEdge ) {
                int   mb = margins.bottom();
                QRect r3( x, (y + h) - mb, w, mb );
                p->fillRect( r3 & rect, color );
            }
        }


        void fillRectEdges( QPainter *p, QRect rect, Qt::Edges edges, int thickness, const QColor& color ) {
            fillRectEdges( p, rect, edges, QMargins( thickness, thickness, thickness, thickness ), color );
        }


        inline QRect expandRect( QRect rect, Qt::Edges edges, int delta ) {
            int l = edges & Qt::LeftEdge ? -delta : 0;
            int t = edges & Qt::TopEdge ? -delta : 0;
            int r = edges & Qt::RightEdge ? delta : 0;
            int b = edges & Qt::BottomEdge ? delta : 0;

            return rect.adjusted( l, t, r, b );
        }


        inline Qt::Edge oppositeEdge( Qt::Edge edge ) {
            switch ( edge ) {
                case Qt::LeftEdge: {
                    return Qt::RightEdge;
                }

                case Qt::TopEdge: {
                    return Qt::BottomEdge;
                }

                case Qt::RightEdge: {
                    return Qt::LeftEdge;
                }

                case Qt::BottomEdge: {
                    return Qt::TopEdge;
                }
            }
            return Qt::TopEdge;
        }


        inline QRect rectTranslatedTowardEdge( QRect rect, Qt::Edge edge, int delta ) {
            switch ( edge ) {
                case Qt::LeftEdge: {
                    return rect.translated( -delta, 0 );
                }

                case Qt::TopEdge: {
                    return rect.translated( 0, -delta );
                }

                case Qt::RightEdge: {
                    return rect.translated( delta, 0 );
                }

                case Qt::BottomEdge: {
                    return rect.translated( 0, delta );
                }
            }
            return rect;
        }


        Q_NEVER_INLINE void paintSolidRoundRect( QPainter *p, QRect rect, qreal radius, const PhSwatch& swatch, Swatchy fill ) {
            if ( !fill ) {
                return;
            }

            bool aa = p->testRenderHint( QPainter::Antialiasing );

            if ( radius > 0.5 ) {
                if ( !aa ) {
                    p->setRenderHint( QPainter::Antialiasing );
                }

                p->setPen( swatch.pen( SwatchColors::S_none ) );
                p->setBrush( swatch.brush( fill ) );
                p->drawRoundedRect( rect, radius, radius );
            }
            else {
                if ( aa ) {
                    p->setRenderHint( QPainter::Antialiasing, false );
                }

                p->fillRect( rect, swatch.color( fill ) );
            }
        }


        Q_NEVER_INLINE void paintBorderedRoundRect( QPainter        *p,
                                                    QRect           rect,
                                                    qreal           radius,
                                                    const PhSwatch& swatch,
                                                    Swatchy         stroke,
                                                    Swatchy         fill ) {
            if ( (rect.width() < 1) || (rect.height() < 1) ) {
                return;
            }

            if ( !stroke && !fill ) {
                return;
            }

            bool aa = p->testRenderHint( QPainter::Antialiasing );

            if ( radius > 0.5 ) {
                if ( !aa ) {
                    p->setRenderHint( QPainter::Antialiasing );
                }

                p->setPen( swatch.pen( stroke ) );
                p->setBrush( swatch.brush( fill ) );
                QRectF rf( rect.x() + 0.5, rect.y() + 0.5, rect.width() - 1.0, rect.height() - 1.0 );
                p->drawRoundedRect( rf, radius, radius );
            }
            else {
                if ( aa ) {
                    p->setRenderHint( QPainter::Antialiasing, false );
                }

                if ( stroke ) {
                    fillRectOutline( p, rect, 1, swatch.color( stroke ) );
                }

                if ( fill ) {
                    p->fillRect( rect.adjusted( 1, 1, -1, -1 ), swatch.color( fill ) );
                }
            }
        }
    } // namespace
}     // namespace Phantom

OnyxStylePrivate::OnyxStylePrivate()
    : headSwatchFastKey( 0 ) {
}


OnyxStyle::OnyxStyle() : d( new OnyxStylePrivate ) {
    setObjectName( QLatin1String( "Onyx" ) );

    /* Show icons on dialog buttons ( default :yes ) */
    mDialogButtonsHaveIcons = ( int )styleSett->value( "Interface::Dialogs::ButtonsHaveIcons" );

    /* Activate list view itesm on single-click ( default :no ) */
    mActivateItemOnSingleClick = ( int )styleSett->value( "Interface::GuiEffects::ActivateOnSingleClick" );

    /* Highlight the shortcut accelerator ( default :yes ) */
    mMnemnonics = ( int )styleSett->value( "Interface::UiElements::ShowMnemnonics" );
}


OnyxStyle::~OnyxStyle() {
    delete d;
}


// Draw text in a rectangle. The current pen set on the painter is used, unless
// an explicit textRole is set, in which case the palette will be used. The
// enabled bool indicates whether the text is enabled or not, and can influence
// how the text is drawn outside of just color. Wrapping and alignment flags
// can be passed in `alignment`.
void OnyxStyle::drawItemText( QPainter *painter, const QRect& rect, int alignment, const QPalette& pal, bool enabled, const QString& text, QPalette::ColorRole textRole ) const {
    Q_UNUSED( enabled );

    if ( text.isEmpty() ) {
        return;
    }

    if ( textRole == QPalette::NoRole ) {
        painter->drawText( rect, alignment, text );
        return;
    }

    QPen          savedPen = painter->pen();
    const QBrush& newBrush = pal.brush( textRole );
    bool          changed  = false;

    if ( savedPen.brush() != newBrush ) {
        changed = true;
        painter->setPen( QPen( newBrush, savedPen.widthF() ) );
    }

    painter->drawText( rect, alignment, text );

    if ( changed ) {
        painter->setPen( savedPen );
    }
}


void OnyxStyle::drawPrimitive( PrimitiveElement elem, const QStyleOption *option, QPainter *painter, const QWidget *widget ) const {
    Q_ASSERT( option );

    if ( !option ) {
        return;
    }

    using Swatchy = Phantom::Swatchy;
    using namespace Phantom::SwatchColors;
    namespace Ph = Phantom;
    auto                ph_swatchPtr = getCachedSwatchOfQPalette( &d->swatchCache, &d->headSwatchFastKey, option->palette );
    const Ph::PhSwatch& swatch       = *ph_swatchPtr.data();

    // Cast to int here to suppress warnings about cases listed which are not in
    // the original enum. This is for custom primitive elements.
    switch ( static_cast<int>(elem) ) {
        case PE_Frame: {
            if ( widget && widget->inherits( "QComboBoxPrivateContainer" ) ) {
                QStyleOption copy = *option;
                copy.state |= State_Raised;
                proxy()->drawPrimitive( PE_PanelMenu, &copy, painter, widget );
                break;
            }

            Ph::fillRectOutline( painter, option->rect, 1, swatch.color( S_frame_outline ) );
            break;
        }

        case PE_FrameMenu: {
            break;
        }

        case PE_FrameDockWidget: {
            painter->save();
            QColor softshadow = option->palette.window().color().darker( 120 );
            QRect  r          = option->rect;
            painter->setPen( softshadow );
            painter->drawRect( r.adjusted( 0, 0, -1, -1 ) );
            painter->setPen( QPen( option->palette.light(), 1 ) );
            painter->drawLine( QPoint( r.left() + 1, r.top() + 1 ), QPoint( r.left() + 1, r.bottom() - 1 ) );
            painter->setPen( QPen( option->palette.window().color().darker( 120 ) ) );
            painter->drawLine( QPoint( r.left() + 1, r.bottom() - 1 ), QPoint( r.right() - 2, r.bottom() - 1 ) );
            painter->drawLine( QPoint( r.right() - 1, r.top() + 1 ),   QPoint( r.right() - 1, r.bottom() - 1 ) );
            painter->restore();
            break;
        }

        case PE_FrameGroupBox: {
            QRect     frame = option->rect;
            Ph::PSave save( painter );
            bool      isFlat = false;

            if ( auto groupBox = qstyleoption_cast<const QStyleOptionGroupBox *>( option ) ) {
                isFlat = groupBox->features & QStyleOptionFrame::Flat;
            }
            else if ( auto frameOpt = qstyleoption_cast<const QStyleOptionFrame *>( option ) ) {
                isFlat = frameOpt->features & QStyleOptionFrame::Flat;
            }

            if ( isFlat ) {
                Ph::fillRectEdges( painter, frame, Qt::TopEdge, 1, swatch.color( S_window_divider ) );
            }
            else {
                Ph::paintBorderedRoundRect( painter, frame, Ph::GroupBox_Rounding, swatch, S_frame_outline, S_none );
            }

            break;
        }

        case PE_IndicatorBranch: {
            if ( !(option->state & State_Children) ) {
                break;
            }

            Qt::ArrowType arrow;

            if ( option->state & State_Open ) {
                arrow = Qt::DownArrow;
            }
            else if ( option->direction != Qt::RightToLeft ) {
                arrow = Qt::RightArrow;
            }
            else {
                arrow = Qt::LeftArrow;
            }

            bool useSelectionColor = false;

            if ( option->state & State_Selected ) {
                if ( auto ivopt = qstyleoption_cast<const QStyleOptionViewItem *>( option ) ) {
                    useSelectionColor = ivopt->showDecorationSelected;
                }
            }

            Swatchy color = useSelectionColor ? S_highlightedText : S_indicator_current;
            QRect   r     = option->rect;

            if ( Ph::BranchesOnEdge ) {
                // TODO RTL
                r.moveLeft( 0 );

                if ( r.width() < r.height() ) {
                    r.setWidth( r.height() );
                }
            }

            int adj = std::min( r.width(), r.height() ) / 4;
            r.adjust( adj, adj, -adj, -adj );
            Ph::drawArrow( painter, r, arrow, swatch.brush( color ) );
            break;
        }

        case PE_IndicatorMenuCheckMark: {
            // For this PE, QCommonStyle treats State_On as drawing the check with the
            // highlighted text color, and otherwise with the regular text color. I
            // guess we should match that behavior, even though it's not consistent
            // with other check box/mark drawing in QStyle (buttons and item view
            // items.) QCommonStyle also doesn't care about tri-state or unchecked
            // states -- it seems that if you call this, you want a check, and nothing
            // else.
            //
            // We'll also catch State_Selected and treat it equivalently (the way you'd
            // expect.) We'll use windowText instead of text, though -- probably
            // doesn't matter.
            Swatchy fgColor    = S_windowText;
            bool    isSelected = option->state & (State_Selected | State_On);
            bool    isEnabled  = option->state & State_Enabled;

            if ( isSelected ) {
                fgColor = S_highlightedText;
            }
            else if ( !isEnabled ) {
                fgColor = S_windowText_disabled;
            }

            qreal rx, ry, rw, rh;
            QRectF( option->rect ).getRect( &rx, &ry, &rw, &rh );
            qreal       dim        = std::min( rw, rh );
            const qreal insetScale = 0.8;
            qreal       dimx       = dim * insetScale * Ph::CheckMark_WidthOfHeightScale;
            qreal       dimy       = dim * insetScale;
            QRectF      r_( rx + (rw - dimx) / 2, ry + (rh - dimy) / 2, dimx, dimy );
            Ph::drawCheck( painter, d->checkBox_pen_scratch, r_, swatch, fgColor );
            break;
        }

        case PE_PanelTipLabel: {
            const auto& palette    = option->palette;
            const auto& background = palette.color( QPalette::ToolTipBase );
            const qreal radius     = Phantom::DefaultFrame_Radius;
            painter->save();
            painter->setRenderHint( QPainter::Antialiasing );
            painter->setPen( Qt::NoPen );
            painter->setBrush( background );
            painter->drawRoundedRect( option->rect, radius, radius );
            painter->restore();
            break;
        }

        // Called for the content area on tree view rows that are selected
        case PE_PanelItemViewItem: {
            QCommonStyle::drawPrimitive( elem, option, painter, widget );
            break;
        }

        // Called for left-of-item-content-area on tree view rows that are selected
        case PE_PanelItemViewRow: {
            QCommonStyle::drawPrimitive( elem, option, painter, widget );
            break;
        }

        case PE_FrameTabBarBase: {
            /** Draw Nothing here */
            break;
        }

        case PE_PanelScrollAreaCorner: {
            bool      isLeftToRight = option->direction != Qt::RightToLeft;
            Qt::Edges edges         = Qt::TopEdge;
            QRect     bgRect        = option->rect;

            if ( isLeftToRight ) {
                edges |= Qt::LeftEdge;
                bgRect.setX( bgRect.x() + 1 );
            }
            else {
                edges |= Qt::RightEdge;
                bgRect.setWidth( bgRect.width() - 1 );
            }

            painter->fillRect( bgRect, swatch.color( S_window ) );
            Ph::fillRectEdges( painter, option->rect, edges, 1, swatch.color( S_window_outline ) );
            break;
        }

        case PE_IndicatorArrowUp:
        case PE_IndicatorArrowDown:
        case PE_IndicatorArrowRight:
        case PE_IndicatorArrowLeft: {
            int rx, ry, rw, rh;
            option->rect.getRect( &rx, &ry, &rw, &rh );

            if ( (rw <= 1) || (rh <= 1) ) {
                break;
            }

            Qt::ArrowType arrow = Qt::UpArrow;
            switch ( elem ) {
                case PE_IndicatorArrowUp: {
                    arrow = Qt::UpArrow;
                    break;
                }

                case PE_IndicatorArrowDown: {
                    arrow = Qt::DownArrow;
                    break;
                }

                case PE_IndicatorArrowRight: {
                    arrow = Qt::RightArrow;
                    break;
                }

                case PE_IndicatorArrowLeft: {
                    arrow = Qt::LeftArrow;
                    break;
                }

                default: {
                    break;
                }
            }

            // The caller may give us a huge rect and expect a normal-sized icon inside
            // of it, so we don't want to fill the entire thing with an arrow,
            // otherwise certain buttons will look weird, like the tab bar scroll
            // buttons. Might want to break these out into editable parameters?
            const int MaxArrowExt = Ph::dpiScaled( 12 );
            const int MinMargin   = std::min( rw, rh ) / 4;
            int       aw, ah;
            aw = std::min( MaxArrowExt, rw ) - MinMargin;
            ah = std::min( MaxArrowExt, rh ) - MinMargin;

            if ( (aw <= 2) || (ah <= 2) ) {
                break;
            }

            aw += (rw - aw) % 2;
            ah += (rh - ah) % 2;
            int ax = (rw - aw) / 2 + rx;
            int ay = (rh - ah) / 2 + ry;
            Ph::drawArrow( painter, QRect( ax, ay, aw, ah ), arrow, swatch );
            break;
        }

        case PE_IndicatorItemViewItemCheck: {
            QStyleOptionButton button;
            button.QStyleOption::operator=( *option );
            button.state &= ~State_MouseOver;
            proxy()->drawPrimitive( PE_IndicatorCheckBox, &button, painter, widget );
            return;
        }

        case PE_IndicatorHeaderArrow: {
            auto header = qstyleoption_cast<const QStyleOptionHeader *>( option );

            if ( !header ) {
                return;
            }

            QRect  r         = header->rect;
            QPoint offset    = QPoint( Phantom::HeaderSortIndicator_HOffset, Phantom::HeaderSortIndicator_VOffset );
            qreal  lightness = Phantom::DeriveColors::hack_isLightPalette( widget->palette() ) ? 0.03 : 0.0;

            if ( header->sortIndicator & QStyleOptionHeader::SortUp ) {
                Ph::drawArrow( painter, r.translated( offset ), Qt::DownArrow, swatch, true, lightness );
            }
            else if ( header->sortIndicator & QStyleOptionHeader::SortDown ) {
                Ph::drawArrow( painter, r.translated( offset ), Qt::UpArrow, swatch, true, lightness );
            }

            break;
        }

        case PE_IndicatorToolBarSeparator: {
            QRect r = option->rect;

            if ( option->state & State_Horizontal ) {
                if ( r.height() >= 10 ) {
                    r.adjust( 0, 3, 0, -3 );
                }

                r.setWidth( r.width() / 2 + 1 );
                Ph::fillRectEdges( painter, r, Qt::RightEdge, 1, swatch.color( S_window_divider ) );
            }
            else {
                // TODO replace with new code
                const int margin = 6;
                const int offset = r.height() / 2;
                painter->setPen( QPen( option->palette.window().color().darker( 110 ) ) );
                painter->drawLine(
                    r.topLeft().x() + margin,
                    r.topLeft().y() + offset,
                    r.topRight().x() - margin,
                    r.topRight().y() + offset
                );
                painter->setPen( QPen( option->palette.window().color().lighter( 110 ) ) );
                painter->drawLine(
                    r.topLeft().x() + margin,
                    r.topLeft().y() + offset + 1,
                    r.topRight().x() - margin,
                    r.topRight().y() + offset + 1
                );
            }

            break;
        }

        case PE_PanelButtonTool: {
            break;
        }

        case PE_IndicatorButtonDropDown: {
            break;
        }

        case PE_IndicatorDockWidgetResizeHandle: {
            QStyleOption dockWidgetHandle = *option;
            bool         horizontal       = option->state & State_Horizontal;
            dockWidgetHandle.state =
                !horizontal ? (dockWidgetHandle.state | State_Horizontal) : (dockWidgetHandle.state & ~State_Horizontal);
            proxy()->drawControl( CE_Splitter, &dockWidgetHandle, painter, widget );
            break;
        }

        case PE_FrameLineEdit: {
            QRect r         = option->rect;
            bool  hasFocus  = option->state & State_HasFocus;
            bool  isEnabled = option->state & State_Enabled;

            Ph::drawRoundedRect(
                painter,
                r,
                Ph::Rounding(),
                Qt::NoPen,
                QColor( 0, 0, 0, isEnabled ? 60 : 30 ),
                false
            );

            if ( hasFocus ) {
                QColor brush = swatch.color( S_highlight );
                brush.setAlphaF( 0.1 );
                Ph::drawRoundedRect(
                    painter,
                    r,
                    Ph::Rounding(),
                    Qt::NoPen,
                    brush,
                    false
                );
            }

            break;
        }

        case PE_PanelLineEdit: {
            auto panel = qstyleoption_cast<const QStyleOptionFrame *>( option );

            if ( !panel ) {
                break;
            }

            Ph::PSave save( painter );
            // We intentionally don't inset the fill rect, even if the frame will paint
            // over the perimeter, because an inset with rounding enabled may cause
            // some miscolored separated pixels between the fill and the border, since
            // we're forced to paint them in two separate draw calls.

            painter->setRenderHint( QPainter::Antialiasing );
            painter->setPen( swatch.pen( Ph::SwatchColors::S_none ) );
            QColor bgColor = swatch.color( S_base );
            bgColor.setAlpha( 100 );
            painter->setBrush( bgColor );
            painter->drawRoundedRect( option->rect, Ph::LineEdit_Rounding, Ph::LineEdit_Rounding );

            // Ph::paintSolidRoundRect(painter, option->rect, Ph::LineEdit_Rounding, swatch, S_base);
            save.restore();

            if ( panel->lineWidth > 0 ) {
                proxy()->drawPrimitive( PE_FrameLineEdit, option, painter, widget );
            }

            break;
        }

        case PE_IndicatorCheckBox: {
            auto checkbox = qstyleoption_cast<const QStyleOptionButton *>( option );

            if ( !checkbox ) {
                break;
            }

            painter->save();
            painter->setRenderHints( QPainter::Antialiasing );

            painter->setPen( Qt::NoPen );

            if ( checkbox->state & QStyle::State_Enabled ) {
                qreal adjust = 0.5;

                if ( checkbox->state & State_On ) {
                    painter->setBrush( swatch.color( S_highlight ) );
                }

                else if ( checkbox->state & State_NoChange ) {
                    painter->setPen( QPen( swatch.color( S_highlight ), 2.0 ) );
                    adjust = 1.0;
                    QColor shade = swatch.color( S_highlight ).darker( 130 );
                    shade.setAlphaF( 0.4 );
                    painter->setBrush( shade );
                }

                else {
                    painter->setBrush( QColor( 0, 0, 0, 30 ) );
                }

                painter->drawRoundedRect( QRectF( option->rect ).adjusted( adjust, adjust, -adjust, -adjust ), 3, 3 );

                if ( option->state & QStyle::State_Sunken ) {
                    painter->setPen( Qt::NoPen );
                    painter->setBrush( QColor( 0, 0, 0, 30 ) );
                    painter->drawRoundedRect( QRectF( option->rect ).adjusted( 0.5, 0.5, -0.5, -0.5 ), 3, 3 );
                }

                else if ( option->state & QStyle::State_MouseOver ) {
                    painter->setPen( QPen( swatch.color( S_highlight ).darker( 120 ), 1.0 ) );
                    painter->drawRoundedRect( QRectF( option->rect ).adjusted( 0.5, 0.5, -0.5, -0.5 ), 3, 3 );
                }
            }

            else {
                painter->setBrush( QColor( 0, 0, 0, 30 ) );
                painter->drawRoundedRect( QRectF( option->rect ).adjusted( 0.5, 0.5, -0.5, -0.5 ), 3, 3 );
            }

            painter->restore();

            break;
        }

        case PE_IndicatorRadioButton: {
            auto radiobtn = qstyleoption_cast<const QStyleOptionButton *>( option );

            if ( !radiobtn ) {
                break;
            }

            painter->save();
            painter->setRenderHints( QPainter::Antialiasing );

            painter->setPen( Qt::NoPen );

            if ( radiobtn->state & QStyle::State_Enabled ) {
                qreal adjust = 0.5;

                if ( radiobtn->state & State_On ) {
                    painter->setBrush( swatch.color( S_highlight ) );
                }

                else if ( radiobtn->state & State_NoChange ) {
                    painter->setPen( QPen( swatch.color( S_highlight ), 2.0 ) );
                    adjust = 1.0;
                    QColor shade = swatch.color( S_highlight ).darker( 130 );
                    shade.setAlphaF( 0.4 );
                    painter->setBrush( shade );
                }

                else {
                    painter->setBrush( QColor( 0, 0, 0, 30 ) );
                }

                painter->drawEllipse( QRectF( radiobtn->rect ).adjusted( adjust, adjust, -adjust, -adjust ) );

                if ( radiobtn->state & QStyle::State_Sunken ) {
                    painter->setPen( Qt::NoPen );
                    painter->setBrush( QColor( 0, 0, 0, 30 ) );
                    painter->drawEllipse( QRectF( option->rect ).adjusted( 0.5, 0.5, -0.5, -0.5 ) );
                }

                else if ( radiobtn->state & QStyle::State_MouseOver ) {
                    painter->setPen( QPen( swatch.color( S_highlight ).darker( 120 ), 1.0 ) );
                    painter->drawEllipse( QRectF( option->rect ).adjusted( 0.5, 0.5, -0.5, -0.5 ) );
                }
            }

            else {
                painter->setBrush( QColor( 0, 0, 0, 30 ) );
                painter->drawEllipse( QRectF( option->rect ).adjusted( 0.5, 0.5, -0.5, -0.5 ) );
            }

            painter->restore();

            break;
        }

        case PE_IndicatorToolBarHandle: {
            if ( !option ) {
                break;
            }

            QRect r = option->rect;

            if ( (r.width() < 3) || (r.height() < 3) ) {
                break;
            }

            int rows    = 3;
            int columns = 2;

            if ( option->state & State_Horizontal ) {
            }
            else {
                qSwap( columns, rows );
            }

            int   dotLen = Ph::dpiScaled( 2 );
            QSize occupied( dotLen * (columns * 2 - 1), dotLen * (rows * 2 - 1) );
            QRect rr = QStyle::alignedRect( option->direction, Qt::AlignCenter, QSize( occupied ), r );
            int   x  = rr.x();
            int   y  = rr.y();
            for (int row = 0; row < rows; ++row) {
                for (int col = 0; col < columns; ++col) {
                    int x_ = x + col * 2 * dotLen;
                    int y_ = y + row * 2 * dotLen;
                    painter->fillRect( x_, y_, dotLen, dotLen, swatch.color( S_window_divider ) );
                }
            }
            break;
        }

        case PE_FrameDefaultButton: {
            break;
        }

        case PE_FrameFocusRect: {
            auto fropt = qstyleoption_cast<const QStyleOptionFocusRect *>( option );

            if ( !fropt ) {
                break;
            }

            //### check for d->alt_down
            if ( !(fropt->state & State_KeyboardFocusChange) ) {
                return;
            }

            if ( fropt->state & State_Item ) {
                if ( auto itemView = qobject_cast<const QAbstractItemView *>( widget ) ) {
                    // TODO either our grid line hack is interfering, or Qt has a bug, but
                    // in RTL layout the grid borders can leave junk behind in the grid
                    // areas and the right edge of the focus rect may not get painted.
                    // (Sometimes it will, though.) To replicate, set to RTL mode, and move
                    // the current around in a table view without the selection being on
                    // the current.
                    if ( option->state & QStyle::State_Selected ) {
                        bool       showCurrent   = true;
                        bool       hasTableGrid  = false;
                        const auto selectionMode = itemView->selectionMode();

                        if ( selectionMode == QAbstractItemView::SingleSelection ) {
                            showCurrent = false;
                        }
                        else {
                            // Table views will can have a "current" frame drawn even if the
                            // "current" is within the selected range. Other item views won't,
                            // which means the "current" frame will be invisible if it's on a
                            // selected item. This is a compromise between the broken drawing
                            // behavior of Qt item views of drawing "current" frames when they
                            // don't make sense (like a tree view where you can only select
                            // entire rows, but Qt will the frame rect around whatever column
                            // was last clicked on by the mouse, but using keyboard navigation
                            // has no effect) and not drawing them at all.
                            bool isTableView = false;

                            if ( auto tableView = qobject_cast<const QTableView *>( itemView ) ) {
                                hasTableGrid = tableView->showGrid();
                                isTableView  = true;
                            }

                            const auto selectionModel = itemView->selectionModel();

                            if ( selectionModel ) {
                                const auto selection = selectionModel->selection();

                                if ( selection.count() == 1 ) {
                                    const auto& range = selection.at( 0 );

                                    if ( isTableView ) {
                                        // For table views, we don't draw the "current" frame if
                                        // there is exactly one cell selected and the "current" is
                                        // that cell, or if there is exactly one row or one column
                                        // selected with the behavior set to the corresponding
                                        // selection, and the "current" is that one row or column.
                                        const auto selectionBehavior = itemView->selectionBehavior();

                                        if ( ( (range.width() == 1) && (range.height() == 1) ) ||
                                             ( (selectionBehavior == QAbstractItemView::SelectRows) && (range.height() == 1) ) ||
                                             ( (selectionBehavior == QAbstractItemView::SelectColumns) &&
                                               (range.width() == 1) ) ) {
                                            showCurrent = false;
                                        }
                                    }
                                    else {
                                        // For any other type of item view, don't draw the "current"
                                        // frame if there is a single contiguous selection, and the
                                        // "current" is within that selection. If there's a
                                        // discontiguous selection, that means the user is probably
                                        // doing something more advanced, and we should just draw the
                                        // focus frame, even if Qt might be doing it badly in some
                                        // cases.
                                        showCurrent = false;
                                    }
                                }
                            }
                        }

                        if ( showCurrent ) {
                            // TODO handle dark-highlight-light-text
                            const QColor& borderColor = swatch.color( S_itemView_multiSelection_currentBorder );
                            const int     thickness   = hasTableGrid ? 2 : 1;
                            Ph::fillRectOutline( painter, option->rect, thickness, borderColor );
                        }
                    }
                    else {
                        Ph::fillRectOutline( painter, option->rect, 1, swatch.color( S_highlight_outline ) );
                    }

                    break;
                }
            }

            /** Tool Button Focus Rect */
            if ( auto toolBtn = qobject_cast<const QToolButton *>( widget ) ) {
                Ph::drawRoundedRect( painter, toolBtn->rect(), Ph::Rounding(), Qt::NoPen, swatch.color( S_highlight ), false );
                break;
            }

            /** CheckBox Focus Rect */
            if ( qobject_cast<const QCheckBox *>( widget ) != nullptr ) {
                painter->save();
                painter->setPen( swatch.color( S_highlight ) );
                painter->drawLine( option->rect.bottomLeft(), option->rect.bottomRight() );
                painter->restore();
                break;
            }

            /** RadioButton Focus Rect */
            if ( qobject_cast<const QRadioButton *>( widget ) != nullptr ) {
                painter->save();
                painter->setPen( swatch.color( S_highlight ) );
                painter->drawLine( option->rect.bottomLeft(), option->rect.bottomRight() );
                painter->restore();
                break;
            }

            /** GroupBox Focus Rect */
            if ( qobject_cast<const QGroupBox *>( widget ) != nullptr ) {
                painter->save();
                painter->setPen( swatch.color( S_highlight ) );
                painter->drawLine( option->rect.bottomLeft(), option->rect.bottomRight() );
                painter->restore();
                break;
            }

            // It would be nice to also handle QTreeView's allColumnsShowFocus thing in
            // the above code, in addition to the normal cases for focus rects in item
            // views. Unfortunately, with allColumnsShowFocus set to true,
            // QTreeView::drawRow() calls the style to paint with PE_FrameFocusRect for
            // the row frame with the widget set to nullptr. This makes it basically
            // impossible to figure out that we need to draw a special frame for it.
            // So, if any application code is using that mode in a QTreeView, it won't
            // get special item view frames. Too bad.
            Ph::PSave save( painter );
            Ph::paintBorderedRoundRect( painter, option->rect, Ph::FrameFocusRect_Rounding, swatch, S_highlight_outline, S_none );
            break;
        }

        case PE_PanelButtonCommand:
        case PE_PanelButtonBevel: {
            if ( auto button = qstyleoption_cast<const QStyleOptionButton *>( option ) ) {
                bool isDefault = (button->features & QStyleOptionButton::DefaultButton) && (button->state & State_Enabled);
                bool isFlat    = (button->features & QStyleOptionButton::Flat);
                bool isDown    = button->state & State_Sunken;
                bool isOn      = button->state & State_On;
                bool isHover   = button->state & State_MouseOver;

                bool isEnabled = button->state & State_Enabled;
                bool hasFocus  = (button->state & State_HasFocus && button->state & State_KeyboardFocusChange);

                QPen   pen   = Qt::NoPen;
                QColor brush = (isEnabled ? QColor( 0, 0, 0, 60 ) : QColor( 255, 255, 255, 30 ) );

                /** Nothing wehn flat */
                if ( isFlat ) {
                    brush.setAlpha( 0 );
                }

                /** Draw the base panel */
                Ph::drawRoundedRect(
                    painter,
                    option->rect,
                    Ph::Rounding(),
                    pen,
                    brush,
                    false
                );

                /** Checked button */
                if ( isOn ) {
                    brush = swatch.color( S_highlight );
                    brush.setAlpha( 90 );

                    Ph::drawRoundedRect(
                        painter,
                        option->rect,
                        Ph::Rounding(),
                        pen,
                        brush,
                        false
                    );
                }

                brush = Qt::transparent;

                /** Pressed down: extra dark */
                if ( isDown ) {
                    brush = QColor( 0, 0, 0, 30 );
                }

                /** Mouse hovering on the button */
                else if ( isHover ) {
                    brush = swatch.color( S_highlight ).lighter( 120 );
                    brush.setAlpha( 90 );
                }

                /** Draw the "highlights" */
                Ph::drawRoundedRect(
                    painter,
                    option->rect,
                    Ph::Rounding(),
                    pen,
                    brush,
                    false
                );

                pen   = Qt::NoPen;
                brush = Qt::transparent;

                if ( isDefault ) {
                    pen   = QPen( swatch.color( S_highlight ).darker( 130 ), 1.0 );
                    brush = swatch.color( S_highlight ).darker( 150 );
                    brush.setAlpha( 20 );

                    Ph::drawRoundedRect(
                        painter,
                        option->rect,
                        Ph::Rounding(),
                        pen,
                        brush,
                        false
                    );
                }

                if ( hasFocus ) {
                    pen   = QPen( swatch.color( S_highlight ), 1.0 );
                    brush = swatch.color( S_highlight );
                    brush.setAlpha( 20 );

                    Ph::drawRoundedRect(
                        painter,
                        option->rect,
                        Ph::Rounding(),
                        pen,
                        brush,
                        false
                    );
                }
            }

            break;
        }

        case PE_FrameTabWidget: {
            QRect bgRect = option->rect.adjusted( 1, 1, -1, -1 );
            painter->fillRect( bgRect, swatch.color( S_tabFrame ) );
            auto twf = qstyleoption_cast<const QStyleOptionTabWidgetFrame *>( option );

            if ( !twf ) {
                break;
            }

            bool tl = true;
            bool tr = true;
            bool br = true;
            bool bl = true;

            if ( twf->tabBarRect.width() && twf->tabBarRect.height() ) {
                switch ( twf->shape ) {
                    case QTabBar::RoundedNorth:
                    case QTabBar::TriangularNorth: {
                        tl = false;
                        break;
                    }

                    case QTabBar::RoundedSouth:
                    case QTabBar::TriangularSouth: {
                        bl = false;
                        break;
                    }

                    case QTabBar::RoundedWest:
                    case QTabBar::TriangularWest: {
                        tl = false;
                        break;
                    }

                    case QTabBar::RoundedEast:
                    case QTabBar::TriangularEast: {
                        tr = false;
                        break;
                    }
                }
            }

            Ph::drawRoundedRect( painter, twf->rect, Ph::Rounding( tl, tr, br, bl, 10.0 ), Qt::NoPen, QColor( 0, 0, 0, 60 ) );
            break;
        }

        case PE_FrameStatusBarItem: {
            break;
        }

        case PE_FrameWindow: {
            break;
        }

        case PE_IndicatorTabClose:
        case Phantom_PE_IndicatorTabNew: {
            Swatchy fg = S_windowText;
            Swatchy bg = S_none;

            if ( (option->state & State_Enabled) && (option->state & State_MouseOver) ) {
                fg = S_highlightedText;
                bg = option->state & State_Sunken ? S_highlight_outline : S_highlight;
            }

            // temp code
            Ph::PSave save( painter );

            if ( bg ) {
                Ph::paintSolidRoundRect( painter, option->rect, Ph::PushButton_Rounding, swatch, bg );
            }

            QPen pen = swatch.pen( fg );
            pen.setCapStyle( Qt::RoundCap );
            pen.setWidthF( 1.5 );
            painter->setBrush( Qt::NoBrush );
            painter->setPen( pen );
            painter->setRenderHint( QPainter::Antialiasing );
            QRect r = option->rect;
            // int adj = (int)((qreal)std::min(r.width(), r.height()) * (1.0 / 2.5));
            int adj = Ph::dpiScaled( 5.0 );
            r.adjust( adj, adj, -adj, -adj );
            qreal x, y, w, h;
            QRectF( r ).getRect( &x, &y, &w, &h );
            // painter->translate(-0.5, -0.5);
            switch ( static_cast<int>(elem) ) {
                case PE_IndicatorTabClose: {
                    painter->drawLine( QPointF( x - 0.5, y - 0.5 ),     QPointF( x + 0.5 + w, y + 0.5 + h ) );
                    painter->drawLine( QPointF( x - 0.5, y + h + 0.5 ), QPointF( x + 0.5 + w, y - 0.5 ) );
                    break;
                }

                case Phantom_PE_IndicatorTabNew: {
                    // kinda hacky here on extra len
                    painter->drawLine( QPointF( x + w / 2, y - 1.0 ), QPointF( x + w / 2, y + h + 1.0 ) );
                    painter->drawLine( QPointF( x - 1.0, y + h / 2 ), QPointF( x + w + 1.0, y + h / 2 ) );
                    break;
                }
            }
            save.restore();
            break;
        }

        case PE_PanelMenu: {
            QColor background( swatch.color( S_window ).darker( 120 ) );
            background.setAlpha( 180 );
            Ph::drawRoundedRect( painter, option->rect, Ph::Rounding(), Qt::NoPen, background );

            break;
        }

        case Phantom_PE_ScrollBarSliderVertical: {
            bool    isLeftToRight = option->direction != Qt::RightToLeft;
            bool    isSunken      = option->state & State_Sunken;
            Swatchy thumbFill, thumbSpecular;

            if ( isSunken ) {
                thumbFill     = S_button_pressed;
                thumbSpecular = S_button_pressed_specular;
            }
            else {
                thumbFill     = S_scrollbarSlider;
                thumbSpecular = S_button_specular;
            }

            Qt::Edges edges;
            QRect     edgeRect = option->rect;
            QRect     mainRect = option->rect;
            edgeRect.adjust( 0, -1, 0, 1 );

            if ( isLeftToRight ) {
                edges = Qt::LeftEdge | Qt::TopEdge | Qt::BottomEdge;
                mainRect.setX( mainRect.x() + 1 );
            }
            else {
                edges = Qt::TopEdge | Qt::BottomEdge | Qt::RightEdge;
                mainRect.setWidth( mainRect.width() - 1 );
            }

            Ph::fillRectEdges( painter, edgeRect, edges, 1, swatch.color( S_window_outline ) );
            painter->fillRect( mainRect, swatch.color( thumbFill ) );
            Ph::fillRectOutline( painter, mainRect, 1, swatch.color( thumbSpecular ) );
            break;
        }

        case Phantom_PE_WindowFrameColor: {
            painter->fillRect( option->rect, swatch.color( S_window_outline ) );
            break;
        }

        default: {
            QCommonStyle::drawPrimitive( elem, option, painter, widget );
            break;
        }
    }
}


void OnyxStyle::drawControl( ControlElement element, const QStyleOption *option, QPainter *painter, const QWidget *widget ) const {
    using Swatchy = Phantom::Swatchy;
    using namespace Phantom::SwatchColors;
    namespace Ph = Phantom;
    auto                ph_swatchPtr = Ph::getCachedSwatchOfQPalette( &d->swatchCache, &d->headSwatchFastKey, option->palette );
    const Ph::PhSwatch& swatch       = *ph_swatchPtr.data();

    switch ( element ) {
        case CE_CheckBox: {
            QCommonStyle::drawControl( element, option, painter, widget );
            // painter->fillRect(option->rect, QColor(255, 0, 0, 90));
            break;
        }

        case CE_ComboBoxLabel: {
            auto cb = qstyleoption_cast<const QStyleOptionComboBox *>( option );

            if ( !cb ) {
                break;
            }

            QRect editRect = proxy()->subControlRect( CC_ComboBox, cb, SC_ComboBoxEditField, widget );
            painter->save();
            painter->setClipRect( editRect );

            if ( !cb->currentIcon.isNull() ) {
                QIcon::Mode mode   = cb->state & State_Enabled ? QIcon::Normal : QIcon::Disabled;
                QPixmap     pixmap = cb->currentIcon.pixmap( cb->iconSize, mode );
                QRect       iconRect( editRect );
                iconRect.setWidth( cb->iconSize.width() + 4 );
                iconRect = alignedRect( cb->direction, Qt::AlignLeft | Qt::AlignVCenter, iconRect.size(), editRect );

                if ( cb->editable ) {
                    painter->fillRect( iconRect, cb->palette.brush( QPalette::Base ) );
                }

                proxy()->drawItemPixmap( painter, iconRect, Qt::AlignCenter, pixmap );

                if ( cb->direction == Qt::RightToLeft ) {
                    editRect.translate( -4 - cb->iconSize.width(), 0 );
                }
                else {
                    editRect.translate( cb->iconSize.width() + 4, 0 );
                }
            }

            if ( !cb->currentText.isEmpty() && !cb->editable ) {
                proxy()->drawItemText(
                    painter,
                    editRect.adjusted( 1, 0, -1, 0 ),
                    visualAlignment( cb->direction, Qt::AlignLeft | Qt::AlignVCenter ),
                    cb->palette,
                    cb->state & State_Enabled,
                    cb->currentText,
                    cb->editable ? QPalette::Text : QPalette::ButtonText
                );
            }

            painter->restore();
            break;
        }

        case CE_ToolButtonLabel: {
            const QStyleOptionToolButton *tbtnOpt = qstyleoption_cast<const QStyleOptionToolButton *>( option );

            bool hasArrow = tbtnOpt->features & QStyleOptionToolButton::HasMenu || tbtnOpt->features & QStyleOptionToolButton::Menu;

            Qt::ToolButtonStyle style = tbtnOpt->toolButtonStyle;

            if ( style == Qt::ToolButtonFollowStyle ) {
                style = (Qt::ToolButtonStyle)proxy()->styleHint( QStyle::SH_ToolButtonStyle );
            }

            painter->save();
            painter->setRenderHints( QPainter::Antialiasing );

            int ypad = (option->rect.height() - tbtnOpt->iconSize.height() ) / 2;
            int xpad = (tbtnOpt->icon.isNull() || tbtnOpt->iconSize.width() == 0) ? 4 : ypad;

            QIcon::Mode mode = QIcon::Normal;

            if ( (option->state & State_Enabled) == false ) {
                mode = QIcon::Disabled;
            }

            else if ( option->state & State_Selected ) {
                mode = QIcon::Selected;
            }

            switch ( style ) {
                case Qt::ToolButtonIconOnly: {
                    tbtnOpt->icon.paint( painter, QRect( xpad, ypad, tbtnOpt->iconSize.width(), tbtnOpt->iconSize.height() ), Qt::AlignCenter, mode, QIcon::Off );

                    break;
                }

                case Qt::ToolButtonTextOnly: {
                    painter->drawText( QRect( xpad, 0, option->rect.width(), option->rect.height() ), Qt::AlignVCenter | Qt::TextShowMnemonic, tbtnOpt->text );
                    break;
                }

                case Qt::ToolButtonTextBesideIcon: {
                    /** Draw the text from the left, then the icon */
                    if ( tbtnOpt->direction == Qt::RightToLeft ) {
                        painter->drawText( QRect( hasArrow ? 32 : xpad, 0, option->rect.width(), option->rect.height() ), Qt::AlignVCenter | Qt::TextShowMnemonic, tbtnOpt->text );

                        tbtnOpt->icon.paint(
                            painter,
                            QRect( option->rect.width() - xpad - tbtnOpt->iconSize.width(), ypad, tbtnOpt->iconSize.width(), tbtnOpt->iconSize.height() ),
                            Qt::AlignCenter, mode, QIcon::Off
                        );
                    }

                    else {
                        tbtnOpt->icon.paint( painter, QRect( xpad, ypad, tbtnOpt->iconSize.width(), tbtnOpt->iconSize.height() ), Qt::AlignCenter, mode, QIcon::Off );

                        painter->drawText(
                            QRect( 2 * xpad + tbtnOpt->iconSize.width(), 0, option->rect.width() - 2 * xpad + tbtnOpt->iconSize.width(), option->rect.height() ),
                            Qt::AlignVCenter | Qt::TextShowMnemonic,
                            tbtnOpt->text
                        );
                    }

                    break;
                }

                case Qt::ToolButtonTextUnderIcon: {
                    // width  = padding + std::max( toolBtn->iconSize.width(), textSize.width() ) + padding;
                    // height = padding + toolBtn->iconSize.height() + padding + textSize.height() +
                    // padding;
                    QCommonStyle::drawControl( element, option, painter, widget );
                    break;
                }

                default: {
                    qWarning() << "Should not have come here!!";
                    break;
                }
            }

            painter->restore();
            break;
        }

        case CE_Splitter: {
            QRect r = option->rect;

            // We don't have anything useful to draw if it's too thin
            if ( (r.width() < 5) || (r.height() < 5) ) {
                break;
            }

            int length = Ph::dpiScaled( Ph::SplitterMaxLength );
            // int thickness = Ph::dpiScaled(1);
            qreal radius = 0;
            QSize size;

            if ( option->state & State_Horizontal ) {
                if ( r.height() < length ) {
                    length = r.height();
                }

                // size = QSize(thickness, length);
                size   = QSize( r.width() / 2, length );
                radius = r.width() * 0.3;
            }
            else {
                if ( r.width() < length ) {
                    length = r.width();
                }

                // size = QSize(length, thickness);
                size   = QSize( length, r.height() / 2 );
                radius = r.height() * 0.3;
            }

            QRect filledRect = QStyle::alignedRect( option->direction, Qt::AlignCenter, size, r );
            Ph::paintSolidRoundRect( painter, filledRect, radius, swatch, S_button_specular );
            // Ph::fillRectOutline(painter, filledRect.adjusted(-1, 0, 1, 0), 1,
            // swatch.color(S_window_divider));
            break;
        }

        // TODO update this for phantom
        case CE_RubberBand: {
            if ( !qstyleoption_cast<const QStyleOptionRubberBand *>( option ) ) {
                break;
            }

            QColor highlight = option->palette.color( QPalette::Active, QPalette::Highlight );
            painter->save();
            QColor penColor = highlight.darker( 120 );
            penColor.setAlpha( 180 );
            painter->setPen( penColor );
            QColor dimHighlight( std::min( highlight.red() / 2 + 110, 255 ),
                                 std::min( highlight.green() / 2 + 110, 255 ),
                                 std::min( highlight.blue() / 2 + 110, 255 ) );
            dimHighlight.setAlpha( widget && widget->isWindow() ? 255 : 80 );
            painter->setRenderHint( QPainter::Antialiasing, true );
            painter->translate( 0.5, 0.5 );
            painter->setBrush( dimHighlight );
            painter->drawRoundedRect( option->rect.adjusted( 0, 0, -1, -1 ), 1, 1 );
            QColor innerLine = Qt::white;
            innerLine.setAlpha( 40 );
            painter->setPen( innerLine );
            painter->drawRoundedRect( option->rect.adjusted( 1, 1, -2, -2 ), 1, 1 );
            painter->restore();
            break;
        }

        case CE_SizeGrip: {
            Qt::LayoutDirection dir  = option->direction;
            QRect               rect = option->rect;
            int                 rcx  = rect.center().x();
            int                 rcy  = rect.center().y();
            // draw grips
            for (int i = -6; i < 12; i += 3) {
                for (int j = -6; j < 12; j += 3) {
                    if ( ( (dir == Qt::LeftToRight) && (i > -j) ) || ( (dir == Qt::RightToLeft) && (j > i) ) ) {
                        painter->fillRect( rcx + i, rcy + j, 2, 2, swatch.color( S_window_lighter ) );
                        painter->fillRect( rcx + i, rcy + j, 1, 1, swatch.color( S_window_darker ) );
                    }
                }
            }
            break;
        }

        case CE_ToolBar: {
            auto toolBar = qstyleoption_cast<const QStyleOptionToolBar *>( option );

            if ( !toolBar ) {
                break;
            }

            painter->fillRect( option->rect, option->palette.window().color() );
            bool isFloating = false;

            if ( auto tb = qobject_cast<const QToolBar *>( widget ) ) {
                isFloating = tb->isFloating();
            }

            if ( isFloating ) {
                Ph::fillRectOutline( painter, option->rect, 1, swatch.color( S_window_outline ) );
            }

            break;
        }

        case CE_DockWidgetTitle: {
            auto dwOpt = qstyleoption_cast<const QStyleOptionDockWidget *>( option );

            if ( !dwOpt ) {
                break;
            }

            painter->save();
            bool verticalTitleBar = dwOpt->verticalTitleBar;

            QRect titleRect = subElementRect( SE_DockWidgetTitleBarText, option, widget );

            if ( verticalTitleBar ) {
                QRect r      = dwOpt->rect;
                QRect rtrans = { r.x(), r.y(), r.height(), r.width() };
                titleRect = QRect( rtrans.left() + r.bottom() - titleRect.bottom(),
                                   rtrans.top() + titleRect.left() - r.left(),
                                   titleRect.height(),
                                   titleRect.width() );
                painter->translate( rtrans.left(), rtrans.top() + rtrans.width() );
                painter->rotate( -90 );
                painter->translate( -rtrans.left(), -rtrans.top() );
            }

            if ( !dwOpt->title.isEmpty() ) {
                QString titleText = painter->fontMetrics().elidedText( dwOpt->title, Qt::ElideRight, titleRect.width() );
                proxy()->drawItemText( painter,
                                       titleRect,
                                       Qt::AlignLeft | Qt::AlignVCenter | Qt::TextShowMnemonic,
                                       dwOpt->palette,
                                       dwOpt->state & State_Enabled,
                                       titleText,
                                       QPalette::WindowText );
            }

            painter->restore();
            break;
        }

        case CE_HeaderSection: {
            auto header = qstyleoption_cast<const QStyleOptionHeader *>( option );

            if ( !header ) {
                break;
            }

            QRect           rect        = header->rect;
            Qt::Orientation orientation = header->orientation;
            QStyleOptionHeader::SectionPosition position = header->position;
            // See the "Table header layout reference" comment block at the bottom of
            // this file for more information to help understand what's going on.
            bool      isLeftToRight = header->direction != Qt::RightToLeft;
            bool      isHorizontal  = orientation == Qt::Horizontal;
            bool      isVertical    = orientation == Qt::Vertical;
            bool      isEnd         = position == QStyleOptionHeader::End;
            bool      isBegin       = position == QStyleOptionHeader::Beginning;
            bool      isOnlyOne     = position == QStyleOptionHeader::OnlyOneSection;
            Qt::Edges edges;
            bool      spansToEnd      = false;
            bool      isSpecialCorner = false;

            if ( (isHorizontal && isLeftToRight && isEnd) || (isHorizontal && !isLeftToRight && isBegin) ||
                 (isVertical && isEnd) || isOnlyOne ) {
                auto hv = qobject_cast<const QHeaderView *>( widget );

                if ( hv ) {
                    spansToEnd = hv->stretchLastSection();

                    // In the case where the header item is not stretched to the end, but
                    // could plausibly be in a position where it could happen to be exactly
                    // the right width or height to be appear to be stretched to the end,
                    // we'll check to see if it actually does exactly meet the right (or
                    // bottom in vertical, or left in RTL) edge, and omit drawing the edge
                    // if that's the case. This can commonly happen if you have a tree or
                    // list view and don't set it to stretch, but the widget is still sized
                    // exactly to hold the one column. (It could also happen if there's
                    // user code running to manually stretch the last section as
                    // necessary.)
                    if ( !spansToEnd ) {
                        QRect viewBound = hv->contentsRect();

                        if ( isHorizontal ) {
                            if ( isLeftToRight ) {
                                spansToEnd = rect.right() == viewBound.right();
                            }
                            else {
                                spansToEnd = rect.left() == viewBound.left();
                            }
                        }
                        else if ( isVertical ) {
                            spansToEnd = rect.bottom() == viewBound.bottom();
                        }
                    }
                }
                else {
                    // We only need to do this check in RTL, because the corner button in
                    // RTL *doesn't* need hacks applied. In LTR, we can just treat the
                    // corner button like anything else on the horizontal header bar, and
                    // can skip doing this inherits check.
                    if ( isOnlyOne && !isLeftToRight && widget && widget->inherits( "QTableCornerButton" ) ) {
                        isSpecialCorner = true;
                    }
                }
            }

            if ( isSpecialCorner ) {
                // In RTL layout, the corner button in a table view doesn't have any
                // offset problems. This branch we're on is only taken if we're in RTL
                // layout and this is the corner button being drawn.
                edges |= Qt::BottomEdge;

                if ( isLeftToRight ) {
                    edges |= Qt::RightEdge;
                }
                else {
                    edges |= Qt::LeftEdge;
                }
            }
            else if ( isHorizontal ) {
                // This branch is taken for horizontal headers in either layout direction
                // or for the corner button in LTR.
                edges |= Qt::BottomEdge;

                if ( isLeftToRight ) {
                    // In LTR, this code path may be for both the corner button *and* the
                    // actual header item. It doesn't matter in this case, and we were able
                    // to avoid doing an extra inherits call earlier.
                    if ( !spansToEnd ) {
                        edges |= Qt::RightEdge;
                    }
                }
                else {
                    // Note: in right-to-left layouts for horizontal headers, the header
                    // view will unfortunately be shifted to the right by 1 pixel, due to
                    // what appears to be a Qt bug. This causes the vertical lines we draw
                    // in the header view to misalign with the grid, and causes the
                    // rightmost section to have its right edge clipped off. Therefore,
                    // we'll draw the separator on the on the right edge instead of the
                    // left edge. (We would have expected to draw it on the left edge in
                    // RTL layout.) This makes it line up with the grid again, except for
                    // the last section. right by 1 pixel.
                    //
                    // In RTL, the "Begin" position is on the left side for some reason
                    // (the same as LTR.) So "End" is always on the right. Ok, whatever.
                    // See the table at the bottom of this file if you're confused.
                    if ( !isOnlyOne && !isEnd ) {
                        edges |= Qt::RightEdge;
                    }

                    // The leftmost section in RTL has to draw on both its right and left
                    // edges, instead of just 1 edge like every other configuration. The
                    // left edge will be offset by 1 pixel from the grid, but it's the best
                    // we can do.
                    if ( isBegin && !spansToEnd ) {
                        edges |= Qt::LeftEdge;
                    }
                }
            }
            else if ( isVertical ) {
                if ( isLeftToRight ) {
                    edges |= Qt::RightEdge;
                }
                else {
                    edges |= Qt::LeftEdge;
                }

                if ( !spansToEnd ) {
                    edges |= Qt::BottomEdge;
                }
            }

            QRect bgRect = Ph::expandRect( rect, edges, -1 );
            painter->fillRect( bgRect, swatch.color( S_window ) );
            Ph::fillRectEdges( painter, rect, edges, 1, swatch.color( S_frame_outline ) );
            break;
        }

        case CE_HeaderLabel: {
            auto header = qstyleoption_cast<const QStyleOptionHeader *>( option );

            if ( !header ) {
                break;
            }

            QRect rect = header->rect;

            if ( !header->icon.isNull() ) {
                int     iconExtent = std::min( std::min( rect.height(), rect.width() ), option->fontMetrics.height() );
                auto    window     = widget ? widget->window()->windowHandle() : nullptr;
                QPixmap pixmap     = Ph::getIconPixmap( header->icon, window, QSize( iconExtent, iconExtent ), (header->state & State_Enabled) ? QIcon::Normal : QIcon::Disabled );
                int     pixw       = static_cast<int>(pixmap.width() / pixmap.devicePixelRatio() );
                QRect   aligned    = alignedRect( header->direction, QFlag( header->iconAlignment ), pixmap.size() / pixmap.devicePixelRatio(), rect );
                QRect   inter      = aligned.intersected( rect );
                painter->drawPixmap(
                    inter.x(),
                    inter.y(),
                    pixmap,
                    inter.x() - aligned.x(),
                    inter.y() - aligned.y(),
                    static_cast<int>(aligned.width() * pixmap.devicePixelRatio() ),
                    static_cast<int>(pixmap.height() * pixmap.devicePixelRatio() ) );
                int margin = proxy()->pixelMetric( QStyle::PM_HeaderMargin, option, widget );

                if ( header->direction == Qt::LeftToRight ) {
                    rect.setLeft( rect.left() + pixw + margin );
                }
                else {
                    rect.setRight( rect.right() - pixw - margin );
                }
            }

            proxy()->drawItemText( painter,
                                   rect,
                                   header->textAlignment,
                                   header->palette,
                                   (header->state & State_Enabled),
                                   header->text,
                                   QPalette::ButtonText );

            // But we still need some kind of indicator, so draw a line
            bool drawHighlightLine = option->state & State_On;

            // Special logic: if the selection mode of the item view is to select every
            // row or every column, there's no real need to draw special "this
            // row/column is selected" highlight indicators in the header view. The
            // application programmer can also disable this explicitly on the header
            // view, but it's nice to have it done automatically, I think.
            if ( drawHighlightLine ) {
                const QAbstractItemView *itemview = nullptr;
                // Header view itself is an item view, and we don't care about its
                // selection behavior -- we care about the actual item view. So try to
                // get the widget as the header first, then find the item view from
                // there.
                auto headerview = qobject_cast<const QHeaderView *>( widget );

                if ( headerview ) {
                    // Also don't care about highlights if there's only one row or column.
                    drawHighlightLine = headerview->count() > 1;
                    itemview          = qobject_cast<const QAbstractItemView *>( headerview->parentWidget() );
                }

                if ( drawHighlightLine && itemview ) {
                    auto selBehavior = itemview->selectionBehavior();

                    if ( (selBehavior == QAbstractItemView::SelectRows) && (header->orientation == Qt::Horizontal) ) {
                        drawHighlightLine = false;
                    }
                    else if ( (selBehavior == QAbstractItemView::SelectColumns) && (header->orientation == Qt::Vertical) ) {
                        drawHighlightLine = false;
                    }
                }
            }

            if ( drawHighlightLine ) {
                QRect    r = option->rect;
                Qt::Edge edge;

                if ( header->orientation == Qt::Horizontal ) {
                    edge = Qt::BottomEdge;
                    r.adjust( -2, 1, 1, 1 );
                }
                else {
                    bool isLeftToRight = option->direction != Qt::RightToLeft;

                    if ( isLeftToRight ) {
                        edge = Qt::RightEdge;
                        r.adjust( 1, -2, 1, 1 );
                    }
                    else {
                        edge = Qt::LeftEdge;
                        r.adjust( -1, -2, -1, 1 );
                    }
                }

                Ph::fillRectEdges( painter, r, edge, 1, swatch.color( S_itemView_headerOnLine ) );
            }

            break;
        }

        case CE_ProgressBarGroove: {
            QRect rect = option->rect;
            Ph::drawRoundedRect( painter, rect, Ph::Rounding(), Qt::NoPen, swatch.color( S_base_shadow ) );

            break;
        }

        case CE_ProgressBarContents: {
            auto bar = qstyleoption_cast<const QStyleOptionProgressBar *>( option );

            if ( !bar ) {
                break;
            }

            QRect filled, nonFilled;
            bool  isIndeterminate = false;
            auto  pBar            = qobject_cast<const QProgressBar *>( widget );
            bool  isVertical      = (pBar && pBar->orientation() == Qt::Vertical);

            /** Get the rects */
            Ph::progressBarFillRects( bar, filled, nonFilled, isIndeterminate, isVertical );

            /** Draw the rects */
            if ( isIndeterminate ) {
                // QVariantAnimation *anim = new QVariantAnimation();
                // anim->setStartValue( 0 );
                // anim->setEndValue( option->rect.width() - 100 );
                // anim->setEasingCurve( QEasingCurve::InOutCubic );
                // anim->setDuration( 250 );
                // anim->setLoopCount( -1 );

                QColor brush = swatch.color( S_highlight );
                brush.setAlphaF( 0.3 );

                /** First frame */
                QRectF rect = QRectF( option->rect ).adjusted( 2, 2, -2, -2 );
                Ph::drawRoundedRect( painter, rect, Ph::Rounding(), Qt::NoPen, brush, false );

                // connect(
                //     anim, &QVariantAnimation::valueChanged, [=] ( const QVariant &val ) {
                //         qDebug() << "Here";
                //         QRectF rect = QRectF( QPointF( val.toInt(), 0 ), QSizeF( 100,
                // option->rect.height() ) );
                //         Ph::drawRoundedRect( painter, rect, Ph::Rounding(), Qt::NoPen, brush, false );
                //     }
                // );

                // anim->start();
            }

            else {
                Ph::drawRoundedRect( painter, filled, Ph::Rounding(), Qt::NoPen, swatch.color( S_highlight ), false );
            }

            // if ( isIndeterminate || (bar->progress > bar->minimum) ) {
            //     Ph::PSave save( painter );
            //     Ph::paintBorderedRoundRect( painter, filled, rounding, swatch, S_progressBar_outline,
            // S_progressBar );
            //     Ph::paintBorderedRoundRect(
            //         painter, filled.adjusted( 1, 1, -1, -1 ), rounding, swatch, S_progressBar_specular,
            // S_none );

            //     if ( isIndeterminate ) {
            //         // TODO paint indeterminate indicator
            //     }
            // }

            break;
        }

        case CE_ProgressBarLabel: {
            auto bar = qstyleoption_cast<const QStyleOptionProgressBar *>( option );

            if ( !bar ) {
                break;
            }

            if ( bar->text.isEmpty() ) {
                break;
            }

            QRect r = bar->rect.adjusted( 2, 2, -2, -2 );

            if ( r.isEmpty() || !r.isValid() ) {
                break;
            }

            QSize textSize = option->fontMetrics.size( 0, bar->text );
            QRect textRect = QStyle::alignedRect( option->direction, Qt::AlignCenter, textSize, option->rect );
            textRect &= r;

            if ( textRect.isEmpty() ) {
                break;
            }

            QRect filled, nonFilled;
            bool  isIndeterminate = false;
            auto  pBar            = qobject_cast<const QProgressBar *>( widget );
            bool  isVertical      = (pBar && pBar->orientation() == Qt::Vertical);
            Ph::progressBarFillRects( bar, filled, nonFilled, isIndeterminate, isVertical );
            QRect     textNonFilledR = textRect & nonFilled;
            QRect     textFilledR    = textRect & filled;
            bool      needsNonFilled = !textNonFilledR.isEmpty();
            bool      needsFilled    = !textFilledR.isEmpty();
            bool      needsMasking   = needsNonFilled && needsFilled;
            Ph::PSave save( painter );

            if ( needsNonFilled ) {
                if ( needsMasking ) {
                    painter->save();
                    painter->setClipRect( textNonFilledR );
                }

                painter->setPen( swatch.pen( S_text ) );
                painter->setBrush( Qt::NoBrush );
                painter->drawText( textRect, bar->text, Qt::AlignHCenter | Qt::AlignVCenter );

                if ( needsMasking ) {
                    painter->restore();
                }
            }

            if ( needsFilled ) {
                if ( needsMasking ) {
                    painter->save();
                    painter->setClipRect( textFilledR );
                }

                painter->setPen( swatch.pen( S_highlightedText ) );
                painter->setBrush( Qt::NoBrush );
                painter->drawText( textRect, bar->text, Qt::AlignHCenter | Qt::AlignVCenter );

                if ( needsMasking ) {
                    painter->restore();
                }
            }

            break;
        }

        case CE_MenuBarItem: {
            auto mbi = qstyleoption_cast<const QStyleOptionMenuItem *>( option );

            if ( !mbi ) {
                break;
            }

            const QRect r        = option->rect;
            QRect       textRect = r;
            textRect.setY( textRect.y() + (r.height() - option->fontMetrics.height() ) / 2 );
            int alignment = Qt::AlignHCenter | Qt::AlignTop | Qt::TextShowMnemonic | Qt::TextDontClip | Qt::TextSingleLine;

            if ( !proxy()->styleHint( SH_UnderlineShortcut, mbi, widget ) ) {
                alignment |= Qt::TextHideMnemonic;
            }

            const auto itemState = mbi->state;
            bool       maybeHasAltKeyNavFocus = itemState & State_Selected && itemState & State_HasFocus;
            bool       isSelected             = itemState & State_Selected || itemState & State_Sunken;

            if ( !isSelected && maybeHasAltKeyNavFocus && widget ) {
                isSelected = widget->hasFocus();
            }

            Swatchy fill = isSelected ? S_highlight : S_window;
            Ph::drawRoundedRect( painter, r, Ph::Rounding(), Qt::NoPen, swatch.color( fill ) );
            QPalette::ColorRole textRole = isSelected ? QPalette::HighlightedText : QPalette::Text;
            proxy()->drawItemText( painter, textRect, alignment, mbi->palette, mbi->state & State_Enabled, mbi->text, textRole );

            if ( Phantom::MenuBarDrawBorder && !isSelected ) {
                Ph::fillRectEdges( painter, r, Qt::BottomEdge, 1, swatch.color( S_window_divider ) );
            }

            break;
        }

        case CE_MenuItem: {
            auto menuItem = qstyleoption_cast<const QStyleOptionMenuItem *>( option );

            if ( !menuItem ) {
                break;
            }

            const auto metrics = Ph::MenuItemMetrics::ofFontHeight( option->fontMetrics.height() );

            // Draws one item in a popup menu.
            if ( menuItem->menuItemType == QStyleOptionMenuItem::Separator ) {
                // Phantom ignores text and icons in menu separators, because
                // 1) The text and icons for separators don't render on Mac native menus
                // 2) There doesn't seem to be a way to account for the width of the text
                // properly (Fusion will often draw separator text clipped off)
                // 3) Setting text on separators also seems to mess up the metrics for
                // menu items on Mac native menus
                QRect r = option->rect;
                r.setHeight( r.height() / 2 + 1 );
                Ph::fillRectEdges( painter, r, Qt::BottomEdge, 1, swatch.color( S_window_divider ) );
                break;
            }

            const QRect itemRect = option->rect;
            painter->save();
            bool isSelected  = menuItem->state & State_Selected && menuItem->state & State_Enabled;
            bool isCheckable = menuItem->checkType != QStyleOptionMenuItem::NotCheckable;
            bool isChecked   = menuItem->checked;
            bool isSunken    = menuItem->state & State_Sunken;
            bool isEnabled   = menuItem->state & State_Enabled;
            bool hasSubMenu  = menuItem->menuItemType == QStyleOptionMenuItem::SubMenu;

            if ( isSelected ) {
                Swatchy fillColor = isSunken ? S_highlight_outline : S_highlight;
                Ph::drawRoundedRect( painter, option->rect, Ph::Rounding(), Qt::NoPen, swatch.color( fillColor ) );
            }

            if ( isCheckable ) {
                // Note: check rect might be misaligned vertically if it's a menu from a
                // combo box. Probably a bug in Qt code?
                QRect   checkRect = Ph::menuItemCheckRect( metrics, option->direction, itemRect, hasSubMenu );
                Swatchy signColor = !isEnabled ? S_windowText : isSelected ? S_windowText /*S_highlightedText*/ : S_windowText;

                if ( menuItem->checkType & QStyleOptionMenuItem::Exclusive ) {
                    // Radio button
                    if ( isChecked ) {
                        painter->setRenderHint( QPainter::Antialiasing );
                        painter->setPen( Qt::NoPen );
                        QPalette::ColorRole textRole = !isEnabled ? QPalette::Text : isSelected ? /*QPalette::HighlightedText*/ QPalette::ButtonText : QPalette::ButtonText;
                        painter->setBrush( option->palette.brush( option->palette.currentColorGroup(), textRole ) );
                        qreal rx, ry, rw, rh;
                        QRectF( checkRect ).getRect( &rx, &ry, &rw, &rh );
                        qreal  dim = std::min( checkRect.width(), checkRect.height() ) * 0.75;
                        QRectF rf( rx + rw / dim, ry + rh / dim, dim, dim );
                        painter->drawEllipse( rf );
                    }
                }
                else {
                    if ( isChecked ) {
                        Ph::drawCheck( painter, d->checkBox_pen_scratch, checkRect, swatch, signColor );
                    }
                }
            }

            const bool hasIcon = !menuItem->icon.isNull();

            if ( hasIcon ) {
                QRect       iconRect = Ph::menuItemIconRect( metrics, option->direction, itemRect, hasSubMenu );
                QIcon::Mode mode     = isEnabled ? QIcon::Normal : QIcon::Disabled;

                if ( isSelected && isEnabled ) {
                    mode = QIcon::Selected;
                }

                QIcon::State state = isChecked ? QIcon::On : QIcon::Off;

                // TODO hmm, we might be ending up with blurry icons at size 15 instead
                // of 16 for example on Windows.
                //
                // int smallIconSize =
                //     proxy()->pixelMetric(PM_SmallIconSize, option, widget);
                // QSize iconSize(smallIconSize, smallIconSize);
                int   iconExtent = std::min( iconRect.width(), iconRect.height() );
                QSize iconSize( iconExtent, iconExtent );

                if ( auto combo = qobject_cast<const QComboBox *>( widget ) ) {
                    iconSize = combo->iconSize();
                }

                QWindow *window = widget ? widget->windowHandle() : nullptr;
                QPixmap pixmap  = Ph::getIconPixmap( menuItem->icon, window, iconSize, mode, state );

                const int pixw       = static_cast<int>(pixmap.width() / pixmap.devicePixelRatio() );
                const int pixh       = static_cast<int>(pixmap.height() / pixmap.devicePixelRatio() );
                QRect     pixmapRect = QStyle::alignedRect( option->direction, Qt::AlignCenter, QSize( pixw, pixh ), iconRect );

                painter->drawPixmap( pixmapRect.topLeft(), pixmap );
            }

            // Draw main text and mnemonic text
            QString s( menuItem->text );

            if ( !s.isEmpty() ) {
                QRect textRect   = Ph::menuItemTextRect( metrics, option->direction, itemRect, hasSubMenu, hasIcon, 0 );
                int   t          = s.indexOf( QLatin1Char( '\t' ) );
                int   text_flags =
                    Qt::AlignLeft | Qt::AlignTop | Qt::TextShowMnemonic | Qt::TextDontClip | Qt::TextSingleLine;

                if ( !styleHint( SH_UnderlineShortcut, menuItem, widget ) ) {
                    text_flags |= Qt::TextHideMnemonic;
                }

                painter->setPen( swatch.pen( isSelected ? S_text /*S_highlightedText*/ : S_text ) );

                // Draw mnemonic text
                if ( t >= 0 ) {
                    QRect         mnemonicR        = Ph::menuItemMnemonicRect( metrics, option->direction, itemRect, hasSubMenu, 0 );
                    const QString textToDrawRef    = s.mid( t + 1 );
                    const QString unsafeTextToDraw = QString::fromRawData( textToDrawRef.constData(), textToDrawRef.size() );
                    painter->drawText( mnemonicR, text_flags, unsafeTextToDraw );
                    s = s.left( t );
                }

                const QString textToDrawRef    = s.left( t );
                const QString unsafeTextToDraw = QString::fromRawData( textToDrawRef.constData(), textToDrawRef.size() );
                painter->drawText( textRect, text_flags, unsafeTextToDraw );
            }

            // SubMenu Arrow
            if ( hasSubMenu ) {
                Qt::ArrowType arrow      = option->direction == Qt::RightToLeft ? Qt::LeftArrow : Qt::RightArrow;
                QRect         arrowRect  = Ph::menuItemArrowRect( metrics, option->direction, itemRect );
                Swatchy       arrowColor = isSelected ? S_highlightedText : S_indicator_current;
                Ph::drawArrow( painter, arrowRect, arrow, swatch.brush( arrowColor ) );
            }

            painter->restore();
            break;
        }

        case CE_MenuHMargin:
        case CE_MenuVMargin:
        case CE_MenuEmptyArea: {
            break;
        }

        case CE_PushButton: {
            auto btn = qstyleoption_cast<const QStyleOptionButton *>( option );

            if ( !btn ) {
                break;
            }

            proxy()->drawControl( CE_PushButtonBevel, btn, painter, widget );
            QStyleOptionButton subopt = *btn;
            subopt.rect = subElementRect( SE_PushButtonContents, btn, widget );
            proxy()->drawControl( CE_PushButtonLabel, &subopt, painter, widget );
            break;
        }

        case CE_PushButtonLabel: {
            auto button = qstyleoption_cast<const QStyleOptionButton *>( option );

            if ( !button ) {
                break;
            }

            // This code is very similar to QCommonStyle's implementation, but doesn't
            // set the icon mode to active when focused.
            QRect textRect = button->rect;
            int   tf       = Qt::AlignVCenter | Qt::TextShowMnemonic;

            if ( !proxy()->styleHint( SH_UnderlineShortcut, button, widget ) ) {
                tf |= Qt::TextHideMnemonic;
            }

            if ( !button->icon.isNull() ) {
                // Center both icon and text
                QRect        iconRect;
                QIcon::Mode  mode   = button->state & State_Enabled ? QIcon::Normal : QIcon::Disabled;
                QIcon::State state  = button->state & State_On ? QIcon::On : QIcon::Off;
                auto         window = widget ? widget->window()->windowHandle() : nullptr;
                QPixmap      pixmap = Ph::getIconPixmap( button->icon, window, button->iconSize, mode, state );

                int pixmapWidth  = static_cast<int>(pixmap.width() / pixmap.devicePixelRatio() );
                int pixmapHeight = static_cast<int>(pixmap.height() / pixmap.devicePixelRatio() );
                int labelWidth   = pixmapWidth;
                int labelHeight  = pixmapHeight;
                // 4 is hardcoded in QPushButton::sizeHint()
                int iconSpacing = 4;
                int textWidth   = button->fontMetrics.boundingRect( option->rect, tf, button->text ).width();

                if ( !button->text.isEmpty() ) {
                    labelWidth += (textWidth + iconSpacing);
                }

                iconRect = QRect( textRect.x() + (textRect.width() - labelWidth) / 2,
                                  textRect.y() + (textRect.height() - labelHeight) / 2,
                                  pixmapWidth,
                                  pixmapHeight );
                iconRect = visualRect( button->direction, textRect, iconRect );
                tf      |= Qt::AlignLeft; // left align, we adjust the text-rect instead

                if ( button->direction == Qt::RightToLeft ) {
                    textRect.setRight( iconRect.left() - iconSpacing );
                }
                else {
                    textRect.setLeft( iconRect.left() + iconRect.width() + iconSpacing );
                }

                if ( button->state & (State_On | State_Sunken) ) {
                    iconRect.translate(
                        proxy()->pixelMetric( PM_ButtonShiftHorizontal, option, widget ),
                        proxy()->pixelMetric( PM_ButtonShiftVertical, option, widget )
                    );
                }

                painter->drawPixmap( iconRect, pixmap );
            }
            else {
                tf |= Qt::AlignHCenter;
            }

            if ( button->state & (State_On | State_Sunken) ) {
                textRect.translate(
                    proxy()->pixelMetric( PM_ButtonShiftHorizontal, option, widget ),
                    proxy()->pixelMetric( PM_ButtonShiftVertical, option, widget )
                );
            }

            if ( button->features & QStyleOptionButton::HasMenu ) {
                int indicatorSize = proxy()->pixelMetric( PM_MenuButtonIndicator, button, widget );

                if ( button->direction == Qt::LeftToRight ) {
                    textRect = textRect.adjusted( 0, 0, -indicatorSize, 0 );
                }
                else {
                    textRect = textRect.adjusted( indicatorSize, 0, 0, 0 );
                }
            }

            proxy()->drawItemText(
                painter,
                textRect,
                tf,
                button->palette,
                (button->state & State_Enabled),
                button->text,
                QPalette::ButtonText
            );
            break;
        }

        case CE_MenuBarEmptyArea: {
            QRect rect = option->rect;

            if ( Phantom::MenuBarDrawBorder ) {
                Ph::fillRectEdges( painter, rect, Qt::BottomEdge, 1, swatch.color( S_window_divider ) );
            }

            painter->fillRect( rect.adjusted( 0, 0, 0, -1 ), swatch.color( S_window ) );
            break;
        }

        case CE_TabBarTab: {
            if ( const QStyleOptionTab *tab = qstyleoption_cast<const QStyleOptionTab *>( option ) ) {
                proxy()->drawControl( CE_TabBarTabShape, tab, painter, widget );

                /** Make the unselected tabl labels partly transparent, so that they're less intense */
                painter->save();
                painter->setOpacity( tab->state & State_Selected ? 1.0 : 0.5 );
                proxy()->drawControl( CE_TabBarTabLabel, tab, painter, widget );
                painter->restore();
            }

            break;
        }

        case CE_TabBarTabShape: {
            auto tab = qstyleoption_cast<const QStyleOptionTab *>( option );

            if ( !tab ) {
                break;
            }

            bool     rtlHorTabs = (tab->direction == Qt::RightToLeft && (tab->shape == QTabBar::RoundedNorth || tab->shape == QTabBar::RoundedSouth) );
            bool     lastTab    = ( (!rtlHorTabs && tab->position == QStyleOptionTab::End) || (rtlHorTabs && tab->position == QStyleOptionTab::Beginning) );
            bool     firstTab   = ( (!rtlHorTabs && tab->position == QStyleOptionTab::Beginning) || (rtlHorTabs && tab->position == QStyleOptionTab::End) );
            bool     onlyOne    = tab->position == QStyleOptionTab::OnlyOneTab;
            bool     hasFrame   = tab->features & QStyleOptionTab::HasFrame && !tab->documentMode;
            bool     isSelected = tab->state & State_Selected;
            Qt::Edge edge       = Qt::TopEdge;

            switch ( tab->shape ) {
                case QTabBar::RoundedNorth:
                case QTabBar::TriangularNorth: {
                    edge = Qt::TopEdge;
                    break;
                }

                case QTabBar::RoundedSouth:
                case QTabBar::TriangularSouth: {
                    edge = Qt::BottomEdge;
                    break;
                }

                case QTabBar::RoundedWest:
                case QTabBar::TriangularWest: {
                    edge = Qt::LeftEdge;
                    break;
                }

                case QTabBar::RoundedEast:
                case QTabBar::TriangularEast: {
                    edge = Qt::RightEdge;
                    break;
                }
            }

            Ph::drawTabShape(
                painter,
                tab->rect,
                edge,
                firstTab,
                lastTab,
                onlyOne,
                hasFrame,
                (isSelected ? QColor( 0, 0, 0, 60 ) : QColor( 90, 90, 90, 30 ) )
            );

            break;
        }

        case CE_ItemViewItem: {
            auto ivopt = qstyleoption_cast<const QStyleOptionViewItem *>( option );

            if ( !ivopt ) {
                break;
            }

            // Hack to work around broken grid line drawing in Qt's table view code:
            //
            // We tell it that the grid line color is a color via
            // SH_Table_GridLineColor. It draws the grid lines, but it in high DPI it's
            // broken because it uses a pen/path to draw the line, which makes it too
            // narrow, subpixel-incorrectly-antialiased, and/or offset from its correct
            // position. So when we draw the item view items in a table view, we'll
            // also try to paint 1 pixel outside of our current rect to try to fill in
            // the incorrectly painted areas where the grid lines are.
            //
            // Also note that the table views with the bad drawing code, when
            // scrolling, will leave garbage behind in the incorrectly-drawn grid line
            // areas. This will also paint over that.
            bool overdrawGridHack = false;

            if ( auto tableWidget = qobject_cast<const QTableView *>( widget ) ) {
                overdrawGridHack = tableWidget->showGrid() && tableWidget->gridStyle() == Qt::SolidLine;
            }

            if ( overdrawGridHack ) {
                QRect r = option->rect.adjusted( -1, -1, 1, 1 );
                Ph::fillRectOutline( painter, r, 1, swatch.color( S_base_divider ) );
            }

            QCommonStyle::drawControl( element, option, painter, widget );
            break;
        }

        case CE_ShapedFrame: {
            auto frameopt = qstyleoption_cast<const QStyleOptionFrame *>( option );

            if ( frameopt ) {
                if ( frameopt->frameShape == QFrame::HLine ) {
                    QRect r = option->rect;
                    r.setY( r.y() + r.height() / 2 );
                    r.setHeight( 2 );
                    painter->fillRect( r, swatch.color( S_tabFrame_specular ) );
                    r.setHeight( 1 );
                    painter->fillRect( r, swatch.color( S_frame_outline ) );
                    break;
                }
                else if ( frameopt->frameShape == QFrame::VLine ) {
                    QRect r = option->rect;
                    r.setX( r.x() + r.width() / 2 );
                    r.setWidth( 2 );
                    painter->fillRect( r, swatch.color( S_tabFrame_specular ) );
                    r.setWidth( 1 );
                    painter->fillRect( r, swatch.color( S_frame_outline ) );
                    break;
                }
            }

            QCommonStyle::drawControl( element, option, painter, widget );
            break;
        }

        default: {
            QCommonStyle::drawControl( element, option, painter, widget );
            break;
        }
    }
}


void OnyxStyle::drawComplexControl( ComplexControl control, const QStyleOptionComplex *option, QPainter *painter, const QWidget *widget ) const {
    using Swatchy = Phantom::Swatchy;
    using namespace Phantom::SwatchColors;
    namespace Ph = Phantom;
    auto                ph_swatchPtr = Ph::getCachedSwatchOfQPalette( &d->swatchCache, &d->headSwatchFastKey, option->palette );
    const Ph::PhSwatch& swatch       = *ph_swatchPtr.data();

    switch ( control ) {
        case CC_GroupBox: {
            auto groupBox = qstyleoption_cast<const QStyleOptionGroupBox *>( option );

            if ( !groupBox ) {
                break;
            }

            painter->save();
            // Draw frame
            QRect textRect     = proxy()->subControlRect( CC_GroupBox, option, SC_GroupBoxLabel, widget );
            QRect checkBoxRect = proxy()->subControlRect( CC_GroupBox, option, SC_GroupBoxCheckBox, widget );

            if ( groupBox->subControls & QStyle::SC_GroupBoxFrame ) {
                QStyleOptionFrame frame;
                frame.QStyleOption::operator=( *groupBox );
                frame.features     = groupBox->features;
                frame.lineWidth    = groupBox->lineWidth;
                frame.midLineWidth = groupBox->midLineWidth;
                frame.rect         = proxy()->subControlRect( CC_GroupBox, option, SC_GroupBoxFrame, widget );
                proxy()->drawPrimitive( PE_FrameGroupBox, &frame, painter, widget );
            }

            // Draw title
            if ( (groupBox->subControls & QStyle::SC_GroupBoxLabel) && !groupBox->text.isEmpty() ) {
                // groupBox->textColor gets the incorrect palette here
                painter->setPen( QPen( option->palette.windowText(), 1 ) );
                unsigned alignment = groupBox->textAlignment;

                if ( !proxy()->styleHint( QStyle::SH_UnderlineShortcut, option, widget ) ) {
                    alignment |= Qt::TextHideMnemonic;
                }

                proxy()->drawItemText(
                    painter,
                    textRect,
                    alignment | Qt::TextShowMnemonic | Qt::AlignLeft,
                    groupBox->palette,
                    groupBox->state & State_Enabled,
                    groupBox->text,
                    QPalette::NoRole
                );

                if ( groupBox->state & State_HasFocus ) {
                    QStyleOptionFocusRect fropt;
                    fropt.QStyleOption::operator=( *groupBox );
                    fropt.rect = textRect.adjusted( -1, 0, 3, 4 );
                    proxy()->drawPrimitive( PE_FrameFocusRect, &fropt, painter, widget );
                }
            }

            // Draw checkbox
            if ( groupBox->subControls & SC_GroupBoxCheckBox ) {
                QStyleOptionButton box;
                box.QStyleOption::operator=( *groupBox );
                box.rect = checkBoxRect;
                proxy()->drawPrimitive( PE_IndicatorCheckBox, &box, painter, widget );
            }

            painter->restore();
            break;
        }

        case CC_SpinBox: {
            auto spinBox = qstyleoption_cast<const QStyleOptionSpinBox *>( option );

            if ( !spinBox ) {
                break;
            }

            bool        isLeftToRight = option->direction != Qt::RightToLeft;
            const QRect rect          = spinBox->rect;
            bool        sunken        = spinBox->state & State_Sunken;
            bool        upIsActive    = spinBox->activeSubControls == SC_SpinBoxUp;
            bool        downIsActive  = spinBox->activeSubControls == SC_SpinBoxDown;
            bool        hasFocus      = option->state & State_HasFocus;
            bool        isEnabled     = option->state & State_Enabled;
            QRect       upRect        = proxy()->subControlRect( CC_SpinBox, spinBox, SC_SpinBoxUp, widget );
            QRect       downRect      = proxy()->subControlRect( CC_SpinBox, spinBox, SC_SpinBoxDown, widget );

            if ( spinBox->frame ) {
                QRect upDownRect = upRect | downRect;

                /** SpinBox rect */
                Ph::drawSelectiveRoundedRect(
                    painter,
                    rect,
                    true,
                    true,
                    true,
                    true,
                    5.0,
                    Qt::NoPen,
                    QColor( 0, 0, 0, isEnabled ? 60 : 30 )
                );

                /** Highlight rect */
                if ( hasFocus ) {
                    QColor brush = swatch.color( S_highlight );
                    brush.setAlphaF( 0.1 );
                    Ph::drawSelectiveRoundedRect(
                        painter,
                        rect.adjusted( 0, 0, upRect.width(), 0 ),
                        true,
                        false,
                        false,
                        true,
                        5.0,
                        Qt::NoPen,
                        brush
                    );
                }

                /** UpDown common rect */
                Ph::drawSelectiveRoundedRect(
                    painter,
                    upDownRect,
                    (isLeftToRight ? false : true),
                    (isLeftToRight ? true  : false),
                    (isLeftToRight ? true  : false),
                    (isLeftToRight ? false : true),
                    5.0,
                    Qt::NoPen,
                    QColor( 255, 255, 255, 30 )
                );

                /** Active Up rect, may be pressed */
                int bw    = (sunken ? 180 : 255);
                int alpha = upIsActive ? (sunken ? 60 : 30) : 0;
                Ph::drawSelectiveRoundedRect(
                    painter,
                    upRect,
                    (isLeftToRight ? false : true),
                    (isLeftToRight ? true  : false),
                    false,
                    false,
                    5.0,
                    Qt::NoPen,
                    QColor( bw, bw, bw, alpha )
                );

                /** Active Down rect, may be pressed */
                bw    = (sunken ? 180 : 255);
                alpha = downIsActive ? (sunken ? 60 : 30) : 0;
                Ph::drawSelectiveRoundedRect(
                    painter,
                    downRect,
                    false,
                    false,
                    (isLeftToRight ? true  : false),
                    (isLeftToRight ? false : true),
                    5.0,
                    Qt::NoPen,
                    QColor( bw, bw, bw, alpha )
                );
            }

            if ( spinBox->buttonSymbols == QAbstractSpinBox::PlusMinus ) {
                Ph::PSave save( painter );
                // TODO fix up old fusion code here
                int     centerX        = upRect.center().x();
                int     centerY        = upRect.center().y();
                Swatchy arrowColorUp   = spinBox->stepEnabled & QAbstractSpinBox::StepUpEnabled ? S_indicator_current : S_indicator_disabled;
                Swatchy arrowColorDown = spinBox->stepEnabled & QAbstractSpinBox::StepDownEnabled ? S_indicator_current : S_indicator_disabled;
                painter->setPen( swatch.pen( arrowColorUp ) );
                painter->drawLine( centerX - 1, centerY,     centerX + 3, centerY );
                painter->drawLine( centerX + 1, centerY - 2, centerX + 1, centerY + 2 );
                centerX = downRect.center().x();
                centerY = downRect.center().y();
                painter->setPen( arrowColorDown );
                painter->drawLine( centerX - 1, centerY, centerX + 3, centerY );
            }

            else if ( spinBox->buttonSymbols == QAbstractSpinBox::UpDownArrows ) {
                int xoffs = isLeftToRight ? 0 : 1;
                Ph::drawArrow(
                    painter,
                    upRect.adjusted( 4 + xoffs, 1, -5 + xoffs, 1 ),
                    Qt::UpArrow,
                    swatch,
                    spinBox->stepEnabled & QAbstractSpinBox::StepUpEnabled
                );

                Ph::drawArrow(
                    painter,
                    downRect.adjusted( 4 + xoffs, 0, -5 + xoffs, -1 ),
                    Qt::DownArrow,
                    swatch,
                    spinBox->stepEnabled & QAbstractSpinBox::StepDownEnabled
                );
            }

            break;
        }

        case CC_TitleBar: {
            auto titleBar = qstyleoption_cast<const QStyleOptionTitleBar *>( option );

            if ( !titleBar ) {
                break;
            }

            painter->save();
            bool     active    = (titleBar->titleBarState & State_Active);
            QRect    fullRect  = titleBar->rect;
            QPalette palette   = option->palette;
            QColor   highlight = option->palette.highlight().color();
            QColor   outline   = option->palette.dark().color();

            QColor titleBarFrameBorder( active ? highlight.darker( 180 ) : outline.darker( 110 ) );
            QColor titleBarHighlight( active ? highlight.lighter( 120 ) : palette.window().color().lighter( 120 ) );
            QColor textColor( active ? 0xffffff : 0xff000000 );
            QColor textAlphaColor( active ? 0xffffff : 0xff000000 );

            // Fill titlebar
            painter->save();
            QColor titlebarColor = QColor( active ? highlight : palette.window().color() );
            painter->setBrush( titlebarColor );
            painter->setPen( Qt::NoPen );
            painter->drawRect( QRectF( fullRect ).adjusted( 1.0, 1.0, -1.0, -1.0 ) );
            painter->restore();

            // draw title
            painter->save();
            QRect textRect = proxy()->subControlRect( CC_TitleBar, titleBar, SC_TitleBarLabel, widget );
            painter->setPen( Qt::gray );
            // Note workspace also does elliding but it does not use the correct font
            QString title = painter->fontMetrics().elidedText( titleBar->text, Qt::ElideRight, textRect.width() - 14 );
            painter->drawText( QRectF( textRect ).adjusted( 0.5, 0.5, 0.5, 0.5 ), title, QTextOption( Qt::AlignHCenter | Qt::AlignVCenter ) );

            if ( active ) {
                painter->setPen( active ? (titleBar->palette.text().color().darker( 120 ) ) : titleBar->palette.text().color() );
                painter->drawText( textRect, title, QTextOption( Qt::AlignHCenter | Qt::AlignVCenter ) );
            }

            painter->restore();

            // min button
            if ( (titleBar->subControls & SC_TitleBarMinButton) && (titleBar->titleBarFlags & Qt::WindowMinimizeButtonHint) &&
                 !(titleBar->titleBarState & Qt::WindowMinimized) ) {
                QRect minButtonRect = proxy()->subControlRect( CC_TitleBar, titleBar, SC_TitleBarMinButton, widget );

                if ( minButtonRect.isValid() ) {
                    bool hover  = (titleBar->activeSubControls & SC_TitleBarMinButton) && (titleBar->state & State_MouseOver);
                    bool sunken = (titleBar->activeSubControls & SC_TitleBarMinButton) && (titleBar->state & State_Sunken);
                    Ph::drawMdiButton( painter, minButtonRect, hover, sunken, QColor( "#FFEA00" ) );
                }
            }

            // max button
            if ( (titleBar->subControls & SC_TitleBarMaxButton) && (titleBar->titleBarFlags & Qt::WindowMaximizeButtonHint) &&
                 !(titleBar->titleBarState & Qt::WindowMaximized) ) {
                QRect maxButtonRect = proxy()->subControlRect( CC_TitleBar, titleBar, SC_TitleBarMaxButton, widget );

                if ( maxButtonRect.isValid() ) {
                    bool hover  = (titleBar->activeSubControls & SC_TitleBarMaxButton) && (titleBar->state & State_MouseOver);
                    bool sunken = (titleBar->activeSubControls & SC_TitleBarMaxButton) && (titleBar->state & State_Sunken);
                    Ph::drawMdiButton( painter, maxButtonRect, hover, sunken, QColor( "#004953" ) );
                }
            }

            // close button
            if ( (titleBar->subControls & SC_TitleBarCloseButton) && (titleBar->titleBarFlags & Qt::WindowSystemMenuHint) ) {
                QRect closeButtonRect = proxy()->subControlRect( CC_TitleBar, titleBar, SC_TitleBarCloseButton, widget );

                if ( closeButtonRect.isValid() ) {
                    bool hover  = (titleBar->activeSubControls & SC_TitleBarCloseButton) && (titleBar->state & State_MouseOver);
                    bool sunken = (titleBar->activeSubControls & SC_TitleBarCloseButton) && (titleBar->state & State_Sunken);
                    Ph::drawMdiButton( painter, closeButtonRect, hover, sunken, QColor( "#EF0107" ) );
                }
            }

            // normalize button
            if ( (titleBar->subControls & SC_TitleBarNormalButton) &&
                 ( ( (titleBar->titleBarFlags & Qt::WindowMinimizeButtonHint) &&
                     (titleBar->titleBarState & Qt::WindowMinimized) ) ||
                   ( (titleBar->titleBarFlags & Qt::WindowMaximizeButtonHint) &&
                     (titleBar->titleBarState & Qt::WindowMaximized) ) ) ) {
                QRect normalButtonRect = proxy()->subControlRect( CC_TitleBar, titleBar, SC_TitleBarNormalButton, widget );

                if ( normalButtonRect.isValid() ) {
                    bool hover  = (titleBar->activeSubControls & SC_TitleBarNormalButton) && (titleBar->state & State_MouseOver);
                    bool sunken = (titleBar->activeSubControls & SC_TitleBarNormalButton) && (titleBar->state & State_Sunken);
                    Ph::drawMdiButton( painter, normalButtonRect, hover, sunken, QColor( "#FFA52C" ) );
                }
            }

            // context help button
            if ( titleBar->subControls & SC_TitleBarContextHelpButton &&
                 (titleBar->titleBarFlags & Qt::WindowContextHelpButtonHint) ) {
                QRect contextHelpButtonRect =
                    proxy()->subControlRect( CC_TitleBar, titleBar, SC_TitleBarContextHelpButton, widget );

                if ( contextHelpButtonRect.isValid() ) {
                    bool hover  = (titleBar->activeSubControls & SC_TitleBarContextHelpButton) && (titleBar->state & State_MouseOver);
                    bool sunken = (titleBar->activeSubControls & SC_TitleBarContextHelpButton) && (titleBar->state & State_Sunken);
                    Ph::drawMdiButton( painter, contextHelpButtonRect, hover, sunken, QColor( "#D2AFFF" ) );

                    QIcon helpIcon = QCommonStyle::standardIcon( QStyle::SP_DialogHelpButton );
                    helpIcon.paint( painter, contextHelpButtonRect.adjusted( 4, 4, -4, -4 ) );
                }
            }

            // shade button
            if ( titleBar->subControls & SC_TitleBarShadeButton && (titleBar->titleBarFlags & Qt::WindowShadeButtonHint) ) {
                QRect shadeButtonRect = proxy()->subControlRect( CC_TitleBar, titleBar, SC_TitleBarShadeButton, widget );

                if ( shadeButtonRect.isValid() ) {
                    bool hover  = (titleBar->activeSubControls & SC_TitleBarShadeButton) && (titleBar->state & State_MouseOver);
                    bool sunken = (titleBar->activeSubControls & SC_TitleBarShadeButton) && (titleBar->state & State_Sunken);
                    Ph::drawMdiButton( painter, shadeButtonRect, hover, sunken, QColor( "#8E4E8E" ) );
                    Ph::drawArrow( painter, shadeButtonRect.adjusted( 5, 7, -5, -7 ), Qt::UpArrow, swatch );
                }
            }

            // unshade button
            if ( titleBar->subControls & SC_TitleBarUnshadeButton && (titleBar->titleBarFlags & Qt::WindowShadeButtonHint) ) {
                QRect unshadeButtonRect = proxy()->subControlRect( CC_TitleBar, titleBar, SC_TitleBarUnshadeButton, widget );

                if ( unshadeButtonRect.isValid() ) {
                    bool hover  = (titleBar->activeSubControls & SC_TitleBarUnshadeButton) && (titleBar->state & State_MouseOver);
                    bool sunken = (titleBar->activeSubControls & SC_TitleBarUnshadeButton) && (titleBar->state & State_Sunken);
                    Ph::drawMdiButton( painter, unshadeButtonRect, hover, sunken, QColor( "#884696" ) );
                    Ph::drawArrow( painter, unshadeButtonRect.adjusted( 5, 7, -5, -7 ), Qt::DownArrow, swatch );
                }
            }

            if ( (titleBar->subControls & SC_TitleBarSysMenu) && (titleBar->titleBarFlags & Qt::WindowSystemMenuHint) ) {
                QRect iconRect = proxy()->subControlRect( CC_TitleBar, titleBar, SC_TitleBarSysMenu, widget );

                if ( iconRect.isValid() ) {
                    if ( !titleBar->icon.isNull() ) {
                        titleBar->icon.paint( painter, iconRect );
                    }
                    else {
                        QStyleOption tool = *titleBar;
                        QPixmap      pm   = proxy()->standardIcon( SP_TitleBarMenuButton, &tool, widget ).pixmap( 16, 16 );
                        tool.rect = iconRect;
                        painter->save();
                        proxy()->drawItemPixmap( painter, iconRect, Qt::AlignCenter, pm );
                        painter->restore();
                    }
                }
            }

            painter->restore();
            break;
        }

        case CC_ScrollBar: {
            auto scrollBar = qstyleoption_cast<const QStyleOptionSlider *>( option );

            if ( !scrollBar ) {
                break;
            }

            auto  pr = proxy();
            QRect scrollBarSubLine = pr->subControlRect( control, scrollBar, SC_ScrollBarSubLine, widget );
            QRect scrollBarAddLine = pr->subControlRect( control, scrollBar, SC_ScrollBarAddLine, widget );
            QRect scrollBarSlider  = pr->subControlRect( control, scrollBar, SC_ScrollBarSlider, widget );
            QRect scrollBarGroove  = pr->subControlRect( control, scrollBar, SC_ScrollBarGroove, widget );

            int padding = Ph::dpiScaled( 4 );
            scrollBarSlider.setX( scrollBarSlider.x() + padding );
            scrollBarSlider.setY( scrollBarSlider.y() + padding );
            // Width and height should be reduced by 2 * padding, but somehow padding is enough.
            scrollBarSlider.setWidth( scrollBarSlider.width() - padding );
            scrollBarSlider.setHeight( scrollBarSlider.height() - padding );

            // Groove/gutter/trench area
            if ( scrollBar->subControls & SC_ScrollBarGroove ) {
                painter->fillRect( scrollBarGroove, swatch.color( S_window ) );
            }

            // Slider thumb
            if ( scrollBar->subControls & SC_ScrollBarSlider ) {
                qreal radius =
                    (scrollBar->orientation == Qt::Horizontal ? scrollBarSlider.height() : scrollBarSlider.width() ) / 2.0;
                bool mouseOver( (option->state & State_Active) && option->state & State_MouseOver );
                bool mousePress( option->state & State_Sunken );
                painter->fillRect( scrollBarSlider, swatch.color( S_window ) );
                // Ph::paintSolidRoundRect(painter, scrollBarSlider, radius, swatch, S_scrollbarSlider);
                painter->save();
                painter->setRenderHint( QPainter::Antialiasing );
                painter->setPen( Qt::NoPen );
                painter->setBrush( mouseOver ? swatch.brush( S_scrollbarSlider_hover ) : swatch.brush( S_scrollbarSlider ) );

                if ( mousePress ) {
                    painter->setBrush( swatch.brush( S_scrollbarSlider_pressed ) );
                }

                painter->drawRoundedRect( scrollBarSlider, radius, radius );
                painter->restore();
            }

            // The SubLine (up/left) buttons
            if ( scrollBar->subControls & SC_ScrollBarSubLine ) {
                painter->fillRect( scrollBarSubLine, swatch.color( S_window ) );
            }

            // The AddLine (down/right) button
            if ( scrollBar->subControls & SC_ScrollBarAddLine ) {
                painter->fillRect( scrollBarAddLine, swatch.color( S_window ) );
            }

            break;
        }

        case CC_ComboBox: {
            auto comboBox = qstyleoption_cast<const QStyleOptionComboBox *>( option );

            if ( !comboBox ) {
                break;
            }

            painter->save();
            bool  isLeftToRight = option->direction != Qt::RightToLeft;
            bool  hasFocus      = option->state & State_HasFocus && option->state & State_KeyboardFocusChange;
            bool  isSunken      = comboBox->state & State_Sunken;
            QRect rect          = comboBox->rect;
            QRect downArrowRect = proxy()->subControlRect( CC_ComboBox, comboBox, SC_ComboBoxArrow, widget );

            // Draw a line edit
            if ( comboBox->editable ) {
                Swatchy buttonFill = isSunken ? S_button_pressed : S_button;
                // if (!hasOptions)
                //   buttonFill = S_window;
                painter->fillRect( rect, swatch.color( buttonFill ) );

                if ( comboBox->frame ) {
                    QStyleOptionFrame buttonOption;
                    buttonOption.QStyleOption::operator=( *comboBox );
                    buttonOption.rect  = rect;
                    buttonOption.state =
                        (comboBox->state & (State_Enabled | State_MouseOver | State_HasFocus) ) | State_KeyboardFocusChange;

                    if ( isSunken ) {
                        buttonOption.state |= State_Sunken;
                        buttonOption.state &= ~State_MouseOver;
                    }

                    proxy()->drawPrimitive( PE_FrameLineEdit, &buttonOption, painter, widget );
                    QRect fr = proxy()->subControlRect( CC_ComboBox, option, SC_ComboBoxEditField, widget );
                    QRect br = rect;

                    if ( isLeftToRight ) {
                        br.setLeft( fr.x() + fr.width() );
                    }
                    else {
                        br.setRight( fr.left() - 1 );
                    }

                    Qt::Edge edge  = isLeftToRight ? Qt::LeftEdge : Qt::RightEdge;
                    Swatchy  color = hasFocus ? S_highlight_outline : S_window_outline;
                    br.adjust( 0, 1, 0, -1 );
                    Ph::fillRectEdges( painter, br, edge, 1, swatch.color( color ) );
                    br.adjust( 1, 0, -1, 0 );
                    Swatchy specular = isSunken ? S_button_pressed_specular : S_button_specular;
                    Ph::fillRectOutline( painter, br, 1, swatch.color( specular ) );
                }
            }

            else {
                QStyleOptionButton buttonOption;
                buttonOption.QStyleOption::operator=( *comboBox );
                buttonOption.rect  = rect;
                buttonOption.state = comboBox->state & (State_Enabled | State_MouseOver | State_HasFocus | State_Active | State_KeyboardFocusChange);

                // Combo boxes should be shown to be keyboard interactive if they're
                // focused at all, not just if the user has pressed tab to enter keyboard
                // focus change mode. This is because the up/down arrows can, regardless
                // of having pressed tab, control the combo box selection.
                if ( comboBox->state & State_HasFocus ) {
                    buttonOption.state |= State_KeyboardFocusChange;
                }

                if ( isSunken ) {
                    buttonOption.state |= State_Sunken;
                    buttonOption.state &= ~State_MouseOver;
                }

                proxy()->drawPrimitive( PE_PanelButtonCommand, &buttonOption, painter, widget );
            }

            if ( comboBox->subControls & SC_ComboBoxArrow ) {
                int   margin = static_cast<int>(std::min( downArrowRect.width(), downArrowRect.height() ) * Ph::ComboBox_ArrowMarginRatio);
                QRect r      = downArrowRect;
                r.adjust( margin, margin, -margin, -margin );
                // Draw the up/down arrow
                Ph::drawArrow( painter, r, Qt::DownArrow, swatch );
            }

            painter->restore();
            break;
        }

        case CC_Slider: {
            auto slider = qstyleoption_cast<const QStyleOptionSlider *>( option );

            if ( !slider ) {
                break;
            }

            const QRect groove       = proxy()->subControlRect( CC_Slider, option, SC_SliderGroove, widget );
            const QRect handle       = proxy()->subControlRect( CC_Slider, option, SC_SliderHandle, widget );
            bool        horizontal   = slider->orientation == Qt::Horizontal;
            bool        ticksAbove   = slider->tickPosition & QSlider::TicksAbove;
            bool        ticksBelow   = slider->tickPosition & QSlider::TicksBelow;
            Swatchy     outlineColor = S_window_outline;

            if ( option->state & State_HasFocus && option->state & State_KeyboardFocusChange ) {
                outlineColor = S_highlight_outline;
            }

            if ( (option->subControls & SC_SliderGroove) && groove.isValid() ) {
                QRect g0 = groove;

                if ( g0.height() > 5 ) {
                    g0.adjust( 0, 1, 0, -1 );
                }

                Ph::PSave saver( painter );
                Swatchy   gutterColor = option->state & State_Enabled ? S_scrollbarGutter : S_window;
                // Ph::paintBorderedRoundRect( painter, groove, Ph::SliderGroove_Rounding, swatch, outlineColor, gutterColor );
                Ph::drawRoundedRect( painter, groove, Ph::Rounding(), swatch.color( outlineColor ), swatch.color( gutterColor ), false );

                QRectF highlightRect = groove.adjusted( 1, 1, 0, -1 );
                highlightRect.setWidth( groove.width() * 1.0 * (slider->sliderPosition - slider->minimum) / (slider->maximum - slider->minimum) - 1 );
                Ph::drawRoundedRect( painter, highlightRect, Ph::Rounding( true, true, true, true, 4.0 ), Qt::NoPen, swatch.color( S_highlight ), false );
            }

            if ( option->subControls & SC_SliderTickmarks ) {
                Ph::PSave save( painter );
                painter->setPen( swatch.pen( S_window_outline ) );
                int tickSize  = proxy()->pixelMetric( PM_SliderTickmarkOffset, option, widget );
                int available = proxy()->pixelMetric( PM_SliderSpaceAvailable, slider, widget );
                int interval  = slider->tickInterval;

                if ( interval <= 0 ) {
                    interval = slider->singleStep;

                    if ( QStyle::sliderPositionFromValue( slider->minimum, slider->maximum, interval, available )
                         - QStyle::sliderPositionFromValue( slider->minimum, slider->maximum, 0, available )
                         < 3 ) {
                        interval = slider->pageStep;
                    }
                }

                if ( interval <= 0 ) {
                    interval = 1;
                }

                int v   = slider->minimum;
                int len = proxy()->pixelMetric( PM_SliderLength, slider, widget );
                while ( v <= slider->maximum + 1 ) {
                    if ( (v == slider->maximum + 1) && (interval == 1) ) {
                        break;
                    }

                    const int v_  = std::min( v, slider->maximum );
                    int       pos = sliderPositionFromValue( slider->minimum,
                                                             slider->maximum,
                                                             v_,
                                                             (horizontal ? slider->rect.width() : slider->rect.height() ) - len,
                                                             slider->upsideDown )
                                    + len / 2;
                    int extra = 2 - ( (v_ == slider->minimum || v_ == slider->maximum) ? 1 : 0);

                    if ( horizontal ) {
                        if ( ticksAbove ) {
                            painter->drawLine( pos, slider->rect.top() + extra, pos, slider->rect.top() + tickSize );
                        }

                        if ( ticksBelow ) {
                            painter->drawLine( pos, slider->rect.bottom() - extra, pos, slider->rect.bottom() - tickSize );
                        }
                    }
                    else {
                        if ( ticksAbove ) {
                            painter->drawLine( slider->rect.left() + extra, pos, slider->rect.left() + tickSize, pos );
                        }

                        if ( ticksBelow ) {
                            painter->drawLine( slider->rect.right() - extra, pos, slider->rect.right() - tickSize, pos );
                        }
                    }

                    // in the case where maximum is max int
                    int nextInterval = v + interval;

                    if ( nextInterval < v ) {
                        break;
                    }

                    v = nextInterval;
                }
            }

            // draw handle
            if ( (option->subControls & SC_SliderHandle) ) {
                if ( option->state & State_MouseOver ) {
                    bool    isPressed = option->state & QStyle::State_Sunken && option->activeSubControls & SC_SliderHandle;
                    QRect   r         = handle;
                    Swatchy handleFill, handleSpecular;

                    if ( isPressed ) {
                        handleFill     = S_sliderHandle_pressed;
                        handleSpecular = S_sliderHandle_pressed_specular;
                    }

                    else {
                        handleFill     = S_sliderHandle;
                        handleSpecular = S_sliderHandle_specular;
                    }

                    painter->save();

                    painter->setRenderHints( QPainter::Antialiasing );

                    painter->setPen( QPen( swatch.color( handleSpecular ), 1.5 ) );
                    painter->setBrush( swatch.color( handleFill ) );

                    painter->drawEllipse( r );

                    painter->restore();
                }
            }

            break;
        }

        case CC_ToolButton: {
            /**
             * This is a complex control. The tool button can be (a) just a button (b) a menu (c) containing
             * a menu.
             * The three cases need to be handled separately.
             * Case (a): Clean simple button.
             *   We can simply draw the button, it's just like a normal push button.
             *   Draw the frame, draw the focus, and then the contents.
             * Case (b): Button is a menu.
             *   This is very similar to the previous case, with a few additional cares.
             *   Draw the frame, but we aware: The frame rect will still be btnArea, so we need to obtain
             *   the menuBtn area by subtracting 32px from the width.
             *   Draw the focus, draw the button arrow, and then the contents.
             * Case (c): Button **has** a menu.
             *   This is the most challenging one. The button area is separate from menu area.
             *   Draw the button area frame - pay attention to LTR/RTL. Draw it's focus and contents.
             *   Draw the menu button frame, it's focus and then the arrow, independent of the button area.
             */
            if ( const QStyleOptionToolButton *tbtnOpt = qstyleoption_cast<const QStyleOptionToolButton *>( option ) ) {
                /** We need to access QStyleOptionToolButton */
                auto tbOpt = qstyleoption_cast<const QStyleOptionToolButton *>( option );

                /** Button with or without menu are the same when there is no arrow */
                bool hasArrow    = tbtnOpt->features & QStyleOptionToolButton::HasMenu || tbtnOpt->features & QStyleOptionToolButton::Menu;
                bool hasArrowBtn = tbtnOpt->features & QStyleOptionToolButton::MenuButtonPopup;

                /** Button area */
                QRect btnArea = proxy()->subControlRect( control, tbtnOpt, SC_ToolButton, widget );

                /** Menu button area */
                QRect menubtnArea = proxy()->subControlRect( control, tbtnOpt, SC_ToolButtonMenu, widget );

                /** For a neutral toolButton, draw no frame */
                bool isFlat    = tbtnOpt->state & State_AutoRaise;
                bool isDown    = tbtnOpt->state & State_Sunken;
                bool isOn      = tbtnOpt->state & State_On;
                bool isEnabled = tbtnOpt->state & State_Enabled;
                bool isHover   = tbtnOpt->state & State_MouseOver;
                bool hasFocus  = (option->state & State_HasFocus && option->state & State_KeyboardFocusChange);
                bool isRTL     = option->direction == Qt::RightToLeft;

                Ph::Rounding rounding;
                rounding.tl = (hasArrowBtn ? (isRTL ? false : true) : true);
                rounding.tr = (hasArrowBtn ? (isRTL ? true : false) : true);
                rounding.br = (hasArrowBtn ? (isRTL ? true : false) : true);
                rounding.bl = (hasArrowBtn ? (isRTL ? false : true) : true);

                /** Draw the base */
                Ph::drawRoundedRect(
                    painter,
                    btnArea,
                    rounding,
                    Qt::NoPen,
                    (isFlat ? Qt::transparent : isEnabled ? QColor( 0, 0, 0, 30 ) : QColor( 255, 255, 255, 30 ) ),
                    false
                );

                /** Draw the arrow button if we have one */
                if ( hasArrowBtn ) {
                    rounding.tl = (isRTL ? true : false);
                    rounding.tr = (isRTL ? false : true);
                    rounding.br = (isRTL ? false : true);
                    rounding.bl = (isRTL ? true : false);

                    Ph::drawRoundedRect(
                        painter,
                        menubtnArea,
                        rounding,
                        Qt::NoPen,
                        (isFlat ? Qt::transparent : QColor( 0, 0, 0, 30 ) ),
                        false
                    );
                }

                /** The button is checked */
                if ( isOn ) {
                    QColor check = swatch.color( S_highlight );
                    check.setAlphaF( 0.5 );

                    Ph::drawRoundedRect(
                        painter,
                        btnArea | menubtnArea,
                        Ph::Rounding(),
                        Qt::NoPen,
                        check,
                        false
                    );
                }

                /** The button is pressed */
                if ( isDown ) {
                    if ( tbtnOpt->activeSubControls & SC_ToolButton ) {
                        rounding.tl = (hasArrowBtn ? (isRTL ? false : true) : true);
                        rounding.tr = (hasArrowBtn ? (isRTL ? true : false) : true);
                        rounding.br = (hasArrowBtn ? (isRTL ? true : false) : true);
                        rounding.bl = (hasArrowBtn ? (isRTL ? false : true) : true);

                        Ph::drawRoundedRect(
                            painter,
                            btnArea,
                            rounding,
                            Qt::NoPen,
                            QColor( 0, 0, 0, 30 ),
                            false
                        );
                    }

                    else {
                        Ph::drawRoundedRect(
                            painter,
                            btnArea | menubtnArea,
                            Ph::Rounding(),
                            Qt::NoPen,
                            QColor( 0, 0, 0, 30 ),
                            false
                        );
                    }

                    Ph::drawRoundedRect(
                        painter,
                        btnArea,
                        rounding,
                        Qt::NoPen,
                        QColor( 0, 0, 0, 30 ),
                        false
                    );
                }

                /** The button is being hovered upon */
                else if ( isHover ) {
                    QColor hover = swatch.color( S_highlight );
                    hover.setAlphaF( 0.5 );

                    if ( tbtnOpt->activeSubControls & SC_ToolButton ) {
                        rounding.tl = (hasArrowBtn ? (isRTL ? false : true) : true);
                        rounding.tr = (hasArrowBtn ? (isRTL ? true : false) : true);
                        rounding.br = (hasArrowBtn ? (isRTL ? true : false) : true);
                        rounding.bl = (hasArrowBtn ? (isRTL ? false : true) : true);

                        Ph::drawRoundedRect(
                            painter,
                            btnArea,
                            rounding,
                            Qt::NoPen,
                            hover,
                            false
                        );
                    }

                    else {
                        rounding.tl = (isRTL ? true : false);
                        rounding.tr = (isRTL ? false : true);
                        rounding.br = (isRTL ? false : true);
                        rounding.bl = (isRTL ? true : false);

                        Ph::drawRoundedRect(
                            painter,
                            menubtnArea,
                            rounding,
                            Qt::NoPen,
                            hover,
                            false
                        );
                    }
                }

                /** Draw focus rectangle */
                if ( hasFocus ) {
                    QColor brush = swatch.color( S_highlight );
                    brush.setAlpha( 10 );

                    Ph::drawRoundedRect(
                        painter,
                        btnArea | menubtnArea,
                        Ph::Rounding(),
                        QPen( swatch.color( S_highlight ) ),
                        brush,
                        false
                    );
                }

                /** Draw the arrow, if we have one */
                if ( hasArrow ) {
                    QRect arrowArea = QRect(
                        isRTL ? 0 : tbOpt->rect.width() - 32,
                        0,
                        32,
                        32
                    );

                    QStyleOptionToolButton arrow = *tbtnOpt;
                    arrow.rect = arrowArea;
                    proxy()->drawPrimitive( PE_IndicatorArrowDown, &arrow, painter, widget );
                }

                QStyleOptionToolButton label = *tbtnOpt;
                label.rect = btnArea;

                /** We have an arrow to show, and arrow is not a dedicated button */
                if ( hasArrow && !hasArrowBtn ) {
                    label.rect.adjust(
                        (isRTL ? 28 : 0),           // Right-to-Left => Arrow on the left
                        0,
                        0,                          // Shrink width by 28px
                        0
                    );
                }

                proxy()->drawControl( CE_ToolButtonLabel, &label, painter, widget );
            }

            break;
        }

        case CC_Dial: {
            if ( auto dial = qstyleoption_cast<const QStyleOptionSlider *>( option ) ) {
                Ph::drawDial( dial, painter );
            }

            break;
        }

        default: {
            QCommonStyle::drawComplexControl( control, option, painter, widget );
            break;
        }
    }
}


void OnyxStyle::polish( QApplication *app ) {
    QCommonStyle::polish( app );
}


void OnyxStyle::unpolish( QApplication *app ) {
    QCommonStyle::unpolish( app );
}


void OnyxStyle::polish( QWidget *widget ) {
    QCommonStyle::polish( widget );

    if ( false ||
         qobject_cast<QAbstractButton *>(  widget ) ||
         qobject_cast<QComboBox *>(        widget ) ||
         qobject_cast<QProgressBar *>(     widget ) ||
         qobject_cast<QScrollBar *>(       widget ) ||
         qobject_cast<QSplitterHandle *>(  widget ) ||
         qobject_cast<QAbstractSlider *>(  widget ) ||
         qobject_cast<QAbstractSpinBox *>( widget ) ||
         (widget->inherits( "QDockSeparator" ) ) ||
         (widget->inherits( "QDockWidgetSeparator" ) )
    ) {
        widget->setAttribute( Qt::WA_Hover,            true );
        widget->setAttribute( Qt::WA_OpaquePaintEvent, false );
    }

    if ( qobject_cast<QMenu *>( widget ) ) {
        widget->setAttribute( Qt::WA_TranslucentBackground, true );
    }

    if ( widget->inherits( "QTipLabel" ) || widget->inherits( "QComboBoxPrivateContainer" ) ) {
        widget->setAttribute( Qt::WA_TranslucentBackground, true );
    }
}


void OnyxStyle::unpolish( QWidget *widget ) {
    QCommonStyle::unpolish( widget );

    if ( false ||
         qobject_cast<QAbstractButton *>(  widget ) ||
         qobject_cast<QComboBox *>(        widget ) ||
         qobject_cast<QProgressBar *>(     widget ) ||
         qobject_cast<QScrollBar *>(       widget ) ||
         qobject_cast<QSplitterHandle *>(  widget ) ||
         qobject_cast<QAbstractSlider *>(  widget ) ||
         qobject_cast<QAbstractSpinBox *>( widget ) ||
         (widget->inherits( "QDockSeparator" ) ) ||
         (widget->inherits( "QDockWidgetSeparator" ) )
    ) {
        widget->setAttribute( Qt::WA_Hover, false );
    }

    if ( qobject_cast<QMenu *>( widget ) ) {
        widget->setAttribute( Qt::WA_TranslucentBackground, false );
    }

    if ( widget->inherits( "QTipLabel" ) ) {
        widget->setAttribute( Qt::WA_TranslucentBackground, false );
    }
}


void OnyxStyle::drawItemPixmap( QPainter *painter, const QRect& rect, int alignment, const QPixmap& pixmap ) const {
    QCommonStyle::drawItemPixmap( painter, rect, alignment, pixmap );
}


QStyle::SubControl OnyxStyle::hitTestComplexControl( ComplexControl cc, const QStyleOptionComplex *opt, const QPoint& pt, const QWidget *w ) const {
    return QCommonStyle::hitTestComplexControl( cc, opt, pt, w );
}


QPixmap OnyxStyle::generatedIconPixmap( QIcon::Mode iconMode, const QPixmap& pixmap, const QStyleOption *opt ) const {
    // Default icon highlight is way too subtle
    if ( iconMode == QIcon::Selected ) {
        QImage   img = pixmap.toImage().convertToFormat( QImage::Format_ARGB32_Premultiplied );
        QPainter painter( &img );

        painter.setCompositionMode( QPainter::CompositionMode_SourceAtop );

        QColor color =
            Phantom::DeriveColors::adjustLightness( opt->palette.color( QPalette::Normal, QPalette::Highlight ), .25 );
        color.setAlphaF( 0.25 );
        painter.fillRect( 0, 0, img.width(), img.height(), color );

        painter.end();

        return QPixmap::fromImage( img );
    }

    return QCommonStyle::generatedIconPixmap( iconMode, pixmap, opt );
}


int OnyxStyle::styleHint( StyleHint hint, const QStyleOption *option, const QWidget *widget, QStyleHintReturn *returnData ) const {
    if ( auto menu = qobject_cast<const QMenu *>( widget ) ) {
        Q_UNUSED( menu )
        const_cast<QWidget *>(widget)->setAttribute( Qt::WA_TranslucentBackground );
    }

    switch ( hint ) {
        case SH_Slider_SnapToValue:
        case SH_PrintDialog_RightAlignButtons:
        case SH_FontDialog_SelectAssociatedText:
        case SH_ComboBox_ListMouseTracking:
        case SH_Slider_StopMouseOverSlider:
        case SH_ScrollBar_MiddleClickAbsolutePosition:
        case SH_TitleBar_AutoRaise:
        case SH_TitleBar_NoBorder:
        case SH_ItemView_ArrowKeysNavigateIntoChildren:
        case SH_ItemView_ChangeHighlightOnFocus:
        case SH_MenuBar_MouseTracking:
        case SH_Menu_MouseTracking: {
            return 1;
        }

        case SH_Menu_SupportsSections: {
            return 0;
        }

            #ifndef Q_OS_MAC
                case SH_MenuBar_AltKeyNavigation: {
                    return 1;
                }

            #endif
            #if defined(QT_PLATFORM_UIKIT)
                case SH_ComboBox_UseNativePopup: {
                    return 1;
                }

            #endif
        case SH_ItemView_ShowDecorationSelected: {
            // QWindowsStyle does this as well -- QCommonStyle seems to have some
            // internal confusion buried within its private implementation of laying
            // out and drawing item views where it can't keep track of what's
            // considered a decoration and what's not. For tree views, if you give 0
            // for ShowDecorationSelected, it applies only to the disclosure indicator
            // and not to the QIcon/pixmap that might be present for the item. So
            // selecting an item in a tree view will have the selection color drawn
            // underneath the icon/pixmap, but not the disclosure indicator. However,
            // in list views, if you give 0 for ShowDecorationSelected, it will *not*
            // draw the selection color underneath the icon/pixmap. There's no way to
            // access this internal logic in QCommonStyle without fully reimplementing
            // the huge mass of stuff for item view layout and drawing. Therefore, the
            // best we can do is at least try to get consistent behavior: if it's a
            // list view, just always return 1 for ShowDecorationSelected.
            if ( !Phantom::ShowItemViewDecorationSelected && qobject_cast<const QListView *>( widget ) ) {
                return 1;
            }

            return Phantom::ShowItemViewDecorationSelected;
        }

        case SH_ItemView_MovementWithoutUpdatingSelection: {
            return 1;
        }

            #if (QT_VERSION >= QT_VERSION_CHECK( 5, 7, 0 ) )
                case SH_ItemView_ScrollMode: {
                    return QAbstractItemView::ScrollPerPixel;
                }

            #endif
        case SH_ScrollBar_ContextMenu: {
            #ifdef Q_OS_MAC
                return 0;

            #else
                return 1;

            #endif
        }

        case SH_DialogButtonBox_ButtonsHaveIcons: {
            if ( mDialogButtonsHaveIcons == Qt::Unchecked ) {
                return 0;
            }

            return 1;
        }

        case SH_ItemView_ActivateItemOnSingleClick: {
            if ( mActivateItemOnSingleClick == Qt::Unchecked ) {
                return 0;
            }

            return 1;
        }

        case SH_UnderlineShortcut: {
            if ( mMnemnonics == Qt::Unchecked ) {
                return 0;
            }

            return 1;
        }

        case SH_ScrollBar_Transient: {
            return 1;
        }

        case SH_EtchDisabledText:
        case SH_DitherDisabledText:
        case SH_ToolBox_SelectedPageTitleBold:
        case SH_Menu_AllowActiveAndDisabled:
        case SH_MainWindow_SpaceBelowMenuBar:
        case SH_MessageBox_CenterButtons:
        case SH_RubberBand_Mask:
        case SH_ScrollView_FrameOnlyAroundContents: {
            return 0;
        }

        case SH_ComboBox_Popup: {
            return Phantom::UseQMenuForComboBoxPopup;
        }

        case SH_Table_GridLineColor: {
            using namespace Phantom::SwatchColors;
            namespace Ph = Phantom;

            if ( !option ) {
                return 0;
            }

            auto                ph_swatchPtr = Ph::getCachedSwatchOfQPalette( &d->swatchCache, &d->headSwatchFastKey, option->palette );
            const Ph::PhSwatch& swatch       = *ph_swatchPtr.data();
            // Qt code in table views for drawing grid lines is broken. See case for
            // CE_ItemViewItem painting for more information.
            return static_cast<int>(swatch.color( S_base_divider ).rgb() );
        }

        case SH_MessageBox_TextInteractionFlags: {
            return Qt::TextSelectableByMouse | Qt::LinksAccessibleByMouse;
        }

        case SH_WizardStyle: {
            return QWizard::ClassicStyle;
        }

        case SH_Menu_SubMenuPopupDelay: {
            // Returning 0 will break sloppy submenus even if they're enabled
            return 10;
        }

        case SH_Menu_SloppySubMenus: {
            return true;
        }

        case SH_Menu_SubMenuSloppyCloseTimeout: {
            return 500;
        }

        case SH_Menu_SubMenuDontStartSloppyOnLeave: {
            return 1;
        }

        case SH_Menu_SubMenuSloppySelectOtherActions: {
            return 1;
        }

        case SH_Menu_SubMenuUniDirection: {
            return 1;
        }

        case SH_Menu_SubMenuUniDirectionFailCount: {
            return 1;
        }

        case SH_Menu_SubMenuResetWhenReenteringParent: {
            return 0;
        }

            #ifdef Q_OS_MAC
                case SH_Menu_FlashTriggeredItem: {
                    return 1;
                }

                case SH_Menu_FadeOutOnHide: {
                    return 0;
                }

            #endif
        case SH_WindowFrame_Mask: {
            return 0;
        }

        case SH_Widget_Animate: {
            return 1;
        }

        default: {
            break;
        }
    }
    return QCommonStyle::styleHint( hint, option, widget, returnData );
}


#include "OnyxStyleMetrics.cpp"
