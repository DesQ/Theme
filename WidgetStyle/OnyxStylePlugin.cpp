/**
 * This file is a part of DesQ Appearance.
 * DesQ Appearance is the Settings plugin to manage DesQ's widget theme,
 * fonts, colors and other related configurations.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "OnyxStylePlugin.hpp"
#include "OnyxStyle.hpp"

#include <QApplication>
#include <QStyleFactory>
#include <QDebug>

#include <desq/Utils.hpp>

DFL::Settings *styleSett = nullptr;

QStringList ProxyStylePlugin::keys() const {
    return { "Onyx" };
}


QStyle *ProxyStylePlugin::create( const QString& key ) {
    if ( key.toLower() == "onyx" ) {
        styleSett = DesQ::Utils::initializeDesQSettings( "Looks", "Looks", true );

        return new OnyxStyle;
    }

    return nullptr;
}
