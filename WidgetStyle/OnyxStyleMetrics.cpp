/**
 * This file is a part of DesQ Appearance.
 * DesQ Appearance is the Settings plugin to manage DesQ's widget theme,
 * fonts, colors and other related configurations.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This plugin is derived from cutefish
 * Copyright (C) 2020 Reven Martin
 * Copyright (C) 2020 KeePassXC Team <team@keepassxc.org>
 * Copyright (C) 2019 Andrew Richards
 *
 * Derived from Phantomstyle and relicensed under the GPLv2 or v3.
 * https://github.com/randrew/phantomstyle
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "OnyxStyle.hpp"
#include "OnyxColor.hpp"

#include <QDebug>
#include <QAbstractItemView>
#include <QApplication>
#include <QComboBox>
#include <QDialogButtonBox>
#include <QFile>
#include <QHeaderView>
#include <QListView>
#include <QMainWindow>
#include <QMdiSubWindow>
#include <QMenu>
#include <QPainter>
#include <QPainterPath>
#include <QPoint>
#include <QPolygon>
#include <QPushButton>
#include <QProgressBar>
#include <QScrollBar>
#include <QSharedData>
#include <QSlider>
#include <QSpinBox>
#include <QSplitter>
#include <QString>
#include <QStyleOption>
#include <QTableView>
#include <QToolBar>
#include <QToolButton>
#include <QTreeView>
#include <QWindow>
#include <QWizard>
#include <QtMath>
#include <qdrawutil.h>

#include <QSettings>

#include <cmath>

QT_BEGIN_NAMESPACE
Q_GUI_EXPORT int qt_defaultDpiX();

QT_END_NAMESPACE



int OnyxStyle::pixelMetric( PixelMetric metric, const QStyleOption *option, const QWidget *widget ) const {
    // Calculate pixel metrics.
    // Use immediate return if value is not supposed to be dpi-scaled.
    int val = -1;

    switch ( metric ) {
        case PM_SliderTickmarkOffset: {
            val = 6;
            break;
        }

        case PM_ToolTipLabelFrameWidth:
        case PM_HeaderMargin:
        case PM_ButtonMargin:
        case PM_SpinBoxFrameWidth: {
            val = 0;
            break;
        }

        case PM_ButtonDefaultIndicator:
        case PM_ButtonShiftHorizontal: {
            val = 0;
            break;
        }

        case PM_ButtonShiftVertical: {
            if ( qobject_cast<const QToolButton *>( widget ) ) {
                return 0;
            }

            val = 1;
            break;
        }

        case PM_ComboBoxFrameWidth: {
            return 0;
        }

        case PM_DefaultFrameWidth: {
            // Original comment from fusion:
            // Do not dpi-scale because the drawn frame is always exactly 1 pixel thick
            // My note:
            // I seriously doubt, with all of the hacky add-or-remove-1 things
            // everywhere in fusion (and still in phantom), and the fact that fusion is
            // totally broken in high dpi, that this actually holds true.
            if ( qobject_cast<const QAbstractItemView *>( widget ) ) {
                return 1;
            }

            val = std::max( 1, Phantom::DefaultFrameWidth - 2 );
            break;
        }

        case PM_MessageBoxIconSize: {
            val = 48;
            break;
        }

        case PM_ScrollBarSliderMin: {
            val = 26;
            break;
        }

        case PM_TitleBarHeight: {
            val = 27;
            break;
        }

        case PM_ScrollBarExtent: {
            val = 14;
            break;
        }

        case PM_SliderThickness:
        case PM_SliderLength: {
            val = 15;
            break;
        }

        case PM_DockWidgetTitleMargin: {
            val = 1;
            break;
        }

        case PM_MenuVMargin: {
            val = Phantom::DefaultFrame_Radius / 2;
            break;
        }

        case PM_MenuHMargin: {
            val = Phantom::DefaultFrame_Radius / 2;
            break;
        }

        case PM_MenuPanelWidth: {
            val = 0;
            break;
        }

        case PM_MenuBarItemSpacing: {
            val = Phantom::MenuBar_ItemSpacing / 2;
            break;
        }

        case PM_MenuBarHMargin: {
            // option is usually nullptr, use widget instead to get font metrics
            if ( !Phantom::MenuBarLeftMargin || !widget ) {
                val = 0;
                break;
            }

            return widget->fontMetrics().height() * Phantom::MenuBar_HorizontalPaddingFontRatio;
        }

        case PM_MenuBarVMargin:
        case PM_MenuBarPanelWidth: {
            val = 0;
            break;
        }

        case PM_ToolBarSeparatorExtent: {
            val = 9;
            break;
        }

        case PM_ToolBarHandleExtent: {
            int dotLen = Phantom::dpiScaled( 2 );
            return dotLen * (3 * 2 - 1);
        }

        case PM_ToolBarItemSpacing: {
            val = 1;
            break;
        }

        case PM_ToolBarFrameWidth: {
            val = Phantom::MenuBar_FrameWidth;
            break;
        }

        case PM_ToolBarItemMargin: {
            val = 1;
            break;
        }

        case PM_ToolBarExtensionExtent: {
            val = 32;
            break;
        }

        case PM_ListViewIconSize:
        case PM_SmallIconSize: {
            if ( Phantom::ItemView_UseFontHeightForDecorationSize && widget &&
                 qobject_cast<const QAbstractItemView *>( widget ) ) {
                // QAbstractItemView::viewOptions() always uses nullptr for the
                // styleoption when querying for PM_SmallIconSize. The best we can do is
                // use the font set on the widget itself, which is obviously going to be
                // wrong if the row has a custom font set on it. Hmm.
                return widget->fontMetrics().height();
            }

            val = 16;
            break;
        }

        case PM_ButtonIconSize: {
            if ( option ) {
                return option->fontMetrics.height();
            }

            if ( widget ) {
                return widget->fontMetrics().height();
            }

            val = 16;
            break;
        }

        case PM_MenuButtonIndicator: {
            val = 32;
            break;
        }

        case PM_DockWidgetTitleBarButtonMargin: {
            val = 2;
            break;
        }

        case PM_TitleBarButtonSize: {
            val = 19;
            break;
        }

        case PM_MaximumDragDistance: {
            return -1;     // Do not dpi-scale because the value is magic
        }

        case PM_TabCloseIndicatorWidth:
        case PM_TabCloseIndicatorHeight: {
            val = 16;
            break;
        }

        case PM_TabBarTabHSpace: {
            // Contents may clip out horizontally if we don't some extra pixels here or
            // in sizeFromContents for CT_TabBarTab.
            if ( !option ) {
                break;
            }

            return static_cast<int>(option->fontMetrics.height() * Phantom::TabBar_HPaddingFontRatio)
                   + static_cast<int>(Phantom::dpiScaled( 4 ) );
        }

        case PM_TabBarTabVSpace: {
            if ( !option ) {
                break;
            }

            return static_cast<int>(option->fontMetrics.height() * Phantom::TabBar_VPaddingFontRatio)
                   + static_cast<int>(Phantom::dpiScaled( 4 ) );
        }

        case PM_TabBarTabOverlap: {
            val = 0;
            break;
        }

        case PM_TabBarBaseOverlap: {
            val = 2;
            break;
        }

        case PM_TabBarIconSize: {
            if ( !widget ) {
                break;
            }

            return widget->fontMetrics().height();
        }

        case PM_TabBarTabShiftHorizontal: {
            val = 0;
            break;
        }

        case PM_TabBarTabShiftVertical: {
            val = 0;
            break;
        }

        case PM_SubMenuOverlap: {
            val = -1;
            break;
        }

        case PM_DockWidgetHandleExtent:
        case PM_SplitterWidth: {
            val = 5;
            break;
        }

        case PM_IndicatorHeight:
        case PM_IndicatorWidth:
        case PM_ExclusiveIndicatorHeight:
        case PM_ExclusiveIndicatorWidth: {
            if ( option ) {
                return std::max( 20, option->fontMetrics.height() );
            }

            if ( widget ) {
                return std::max( 20, widget->fontMetrics().height() );
            }

            val = 20;
            break;
        }

        case PM_RadioButtonLabelSpacing:
        case PM_CheckBoxLabelSpacing: {
            val = std::max( 10, QCommonStyle::pixelMetric( metric, option, widget ) );
            break;
        }

        case PM_ScrollView_ScrollBarOverlap:
        case PM_ScrollView_ScrollBarSpacing: {
            val = 0;
            break;
        }

        case PM_TreeViewIndentation: {
            if ( widget ) {
                return widget->fontMetrics().height();
            }

            val = 12;
            break;
        }

        default: {
            val = QCommonStyle::pixelMetric( metric, option, widget );
        }
    }

    return Phantom::dpiScaled( val );
}


QSize OnyxStyle::sizeFromContents( ContentsType type, const QStyleOption *option, const QSize& size, const QWidget *widget ) const {
    namespace Ph = Phantom;
    // Cases which do not rely on the parent class to do any work
    switch ( type ) {
        case CT_RadioButton:
        case CT_CheckBox: {
            auto btn = qstyleoption_cast<const QStyleOptionButton *>( option );

            if ( !btn ) {
                break;
            }

            bool isRadio = type == CT_RadioButton;
            int  w       = proxy()->pixelMetric( isRadio ? PM_ExclusiveIndicatorWidth : PM_IndicatorWidth, btn, widget );
            int  h       = proxy()->pixelMetric( isRadio ? PM_ExclusiveIndicatorHeight : PM_IndicatorHeight, btn, widget );
            int  margins = 0;

            if ( !btn->icon.isNull() || !btn->text.isEmpty() ) {
                margins = proxy()->pixelMetric( isRadio ? PM_RadioButtonLabelSpacing : PM_CheckBoxLabelSpacing, option, widget );
            }

            return QSize( size.width() + w + margins, std::max( size.height(), h ) );
        }

        case CT_MenuBarItem: {
            return QSize( QCommonStyle::sizeFromContents( type, option, size, widget ) + QSize( 8, 5 ) );

            // int fontHeight = option ? option->fontMetrics.height() : size.height();
            // int w = static_cast<int>(fontHeight * Ph::MenuBar_HorizontalPaddingFontRatio);
            // int h = static_cast<int>(fontHeight * Ph::MenuBar_VerticalPaddingFontRatio);
            // int line = Ph::dpiScaled(1);
            // return QSize(size.width() + w * 2, size.height() + h * 2 + line);
        }

        case CT_MenuItem: {
            auto menuItem = qstyleoption_cast<const QStyleOptionMenuItem *>( option );

            if ( !menuItem ) {
                return size;
            }

            bool hasTabChar        = menuItem->text.contains( QLatin1Char( '\t' ) );
            bool hasSubMenu        = menuItem->menuItemType == QStyleOptionMenuItem::SubMenu;
            bool isSeparator       = menuItem->menuItemType == QStyleOptionMenuItem::Separator;
            int  fontMetricsHeight = -1;

            // See notes at CE_MenuItem and SH_ComboBox_Popup for more information
            if ( Ph::UseQMenuForComboBoxPopup && qobject_cast<const QComboBox *>( widget ) ) {
                if ( !widget->testAttribute( Qt::WA_SetFont ) ) {
                    fontMetricsHeight = QFontMetrics( qApp->font( "QMenu" ) ).height();
                }
            }

            if ( fontMetricsHeight == -1 ) {
                fontMetricsHeight = option->fontMetrics.height();
            }

            auto metrics = Ph::MenuItemMetrics::ofFontHeight( fontMetricsHeight );
            // Incoming width is the sum of the visual widths of the main item text and
            // the mnemonic text (if any). To this width we will add the widths of the
            // other features for this menu item -- the icon/checkbox, spacing between
            // icon/text/mnemonic, etc. For cases like separators without any text, we
            // may disregard the width.
            //
            // Height is the text height, probably.
            int w = size.width();
            // Frame
            w += metrics.frameThickness * 2;
            // Left margins don't depend on whether or not we have a submenu arrow.
            // Calculating the right margins requires knowing whether or not the menu
            // item has a submenu arrow.
            w += metrics.leftMargin;
            // Phantom treats every menu item with the same space on the left for a
            // check mark, even if it doesn't have the checkable property.
            w += metrics.checkWidth + metrics.checkRightSpace;

            if ( !menuItem->icon.isNull() ) {
                // Phantom disregards any user-specified icon sizing at the moment.
                w += metrics.fontHeight;
                w += metrics.iconRightSpace;
            }

            // Tab character is used for separating the shortcut text
            if ( hasTabChar ) {
                w += metrics.mnemonicSpace;
            }

            if ( hasSubMenu ) {
                w += metrics.arrowSpace + metrics.arrowWidth + metrics.rightMarginForArrow;
            }
            else {
                w += metrics.rightMarginForText;
            }

            int h;

            if ( isSeparator ) {
                h = metrics.separatorHeight;
            }
            else {
                h = metrics.totalHeight;
            }

            if ( !menuItem->icon.isNull() ) {
                if ( auto combo = qobject_cast<const QComboBox *>( widget ) ) {
                    h = std::max( combo->iconSize().height() + 2, h );
                }
            }

            QSize sz;
            sz.setWidth( std::max<int>( w, Ph::dpiScaled( Ph::MenuMinimumWidth ) ) );
            sz.setHeight( h );
            return sz;
        }

        case CT_Menu: {
            if ( !Ph::MenuExtraBottomMargin || !option || !widget ) {
                break;
            }

            // Trick the QMenu into putting a margin only at the bottom by adding extra
            // height to the contents size. We only want to add this tricky space if
            // there is at least more than 1 item in the menu.
            const auto acts = widget->actions();

            if ( acts.count() < 2 ) {
                break;
            }

            // We only want to add the tricky space if there's at least 1 separator,
            // otherwise it looks weird.
            bool anySeps = false;
            for (auto act : acts) {
                if ( act->isSeparator() ) {
                    anySeps = true;
                    break;
                }
            }

            if ( !anySeps ) {
                break;
            }

            // int fheight = option->fontMetrics.height();
            // int vmargin = static_cast<int>(fheight * Ph::MenuItem_SeparatorHeightFontRatio) / 2;
            // QSize sz = size;
            // sz.setHeight(sz.height() + vmargin);
            return size;
        }

        case CT_TabBarTab: {
            // Placeholder in case we change this in the future
            return size;
        }

        case CT_Slider: {
            QSize sz = size;

            // mitigate zero-pointer dereference
            if ( !widget || (qobject_cast<const QSlider *>( widget )->orientation() == Qt::Horizontal) ) {
                sz.setHeight( sz.height() + PM_SliderTickmarkOffset );
            }
            else {
                sz.setWidth( sz.width() + PM_SliderTickmarkOffset );
            }

            return sz;
        }

        case CT_GroupBox: {
            // This doesn't seem to get used except once by QGroupBox for
            // minimumSizeHint(). After that, the sizing/layout calculations seem to
            // use the rects given by subControlRect().
            auto opt = qstyleoption_cast<const QStyleOptionGroupBox *>( option );

            if ( !opt ) {
                break;
            }

            // Checkbox and text height already accounted for, but margin between text
            // and frame isn't.
            int xadd = 0;
            int yadd = 0;

            if ( opt->subControls & (SC_GroupBoxCheckBox | SC_GroupBoxLabel) ) {
                int fontHeight = option->fontMetrics.height();
                yadd += static_cast<int>(fontHeight * Phantom::GroupBox_LabelBottomMarginFontRatio);
            }

            // We can test for the frame in general, but unfortunately testing to see
            // if it's the 1-line "flat" style or 4-line box/rect "anything else" style
            // doesn't seem to be possible here, only when painting.
            if ( opt->subControls & SC_GroupBoxFrame ) {
                xadd += 2;
                yadd += 2;
            }

            return QSize( size.width() + xadd, size.height() + yadd );
        }

        case CT_ItemViewItem: {
            auto vopt = qstyleoption_cast<const QStyleOptionViewItem *>( option );

            if ( !vopt ) {
                break;
            }

            QSize sz = QCommonStyle::sizeFromContents( type, option, size, widget );
            sz += QSize( 0, Phantom::DefaultFrameWidth );

            // QCommonStyle has a bunch of complicated logic for laying out/calculating
            // rects of view items, which is locked behind a private data guy. In
            // sizeFromContents for CT_ItemViewItem, it unions all of the item row's
            // rects together and then, if the decoration height is exactly the same as
            // the row height, it adds 2 pixels (not dpi scaled) to the height. The
            // comment says it's to prevent "icons from overlapping" but I have no idea
            // how that's supposed to help. And we don't necessarily want those extra 2
            // pixels. Anyway, I don't want to copy and paste all of that code into
            // Phantom and then maintain it. So when Phantom is in the mode where we're
            // basing the item view decoration sizes off of the font size, we'll just
            // take a guess when QCommonStyle has added 2 to the height (because the
            // row height and decoration height are both the font height), and
            // re-remove those two pixels.
            if ( Phantom::ItemView_UseFontHeightForDecorationSize ) {
                int fh = vopt->fontMetrics.height();

                if ( (sz.height() == fh + 2) && (vopt->decorationSize.height() == fh) ) {
                    sz.setHeight( fh );
                }
            }

            return sz;
        }

        case CT_HeaderSection: {
            auto hdr = qstyleoption_cast<const QStyleOptionHeader *>( option );

            if ( !hdr ) {
                break;
            }

            // This is pretty crummy. Should also check if we need multi-line support
            // or not.
            bool  nullIcon = hdr->icon.isNull();
            int   margin   = proxy()->pixelMetric( QStyle::PM_HeaderMargin, hdr, widget );
            int   iconSize = nullIcon ? 0 : option->fontMetrics.height();
            QSize txt      = hdr->fontMetrics.size( Qt::TextSingleLine, hdr->text );
            QSize sz;
            sz.setHeight( margin + std::max( iconSize, txt.height() ) + margin );
            sz.setWidth( (nullIcon ? 0 : margin) + iconSize + (hdr->text.isNull() ? 0 : margin) + txt.width() + margin );

            if ( hdr->sortIndicator != QStyleOptionHeader::None ) {
                if ( hdr->orientation == Qt::Horizontal ) {
                    sz.rwidth() += sz.height() + margin;
                }
                else {
                    sz.rheight() += sz.width() + margin;
                }
            }

            return sz;
        }

        default: {
            break;
        }
    }

    // Cases which modify the size given by the parent class
    QSize newSize = QCommonStyle::sizeFromContents( type, option, size, widget );
    switch ( type ) {
        case CT_PushButton: {
            auto pbopt = qstyleoption_cast<const QStyleOptionButton *>( option );

            if ( !pbopt || pbopt->text.isEmpty() ) {
                break;
            }

            int hpad = static_cast<int>(pbopt->fontMetrics.height() * Phantom::PushButton_HorizontalPaddingFontHeightRatio);
            newSize.rwidth() += hpad * 2;

            if ( widget && qobject_cast<const QDialogButtonBox *>( widget->parent() ) ) {
                int dialogButtonMinWidth = Phantom::dpiScaled( 115 );
                newSize.rwidth() = std::max( newSize.width(), dialogButtonMinWidth );
            }

            newSize.setHeight( std::max( 32, newSize.height() ) );

            break;
        }

        case CT_ToolButton: {
            /** We need to access QStyleOptionToolButton */
            auto toolBtn = qstyleoption_cast<const QStyleOptionToolButton *>( option );

            /**
             * The @size we have is text or icon size. QCommonStyle just adds 6px to the width.
             * We will add a minimum padding of 4px on the left and right. QCommonStyle, likewise,
             * adds 5px to the height. We will add a minimum of 4px padding on top and bottom,
             * and then ensure the button is minimum of 32px in height.
             * Buttons can have menus, and these menus can be triggered in two ways:
             * (a) Button itself is pressed (instant or delayed)
             * (b) We have a menu button, which is pressed.
             * We should decide if the size needs to be changed.
             * 1. Get the button style (icon/text/icon+text/text-below-icon/style)
             * 2. If
             */

            Qt::ToolButtonStyle style = toolBtn->toolButtonStyle;

            if ( style == Qt::ToolButtonFollowStyle ) {
                style = (Qt::ToolButtonStyle)proxy()->styleHint( QStyle::SH_ToolButtonStyle );
            }

            /**
             * Let's calculate size from first-principles:
             * Width = padding + ifIcon( iconWidth + padding ) + ifText( textWidth + padding ) + ifMenu( 32
             *- padding ) + ifMenuButton( padding )
             * Height = padding + ifIcon( iconHeight + padding ) + ifText( textHeight + padding ) + padding
             * Note: ifMenu( 32 - padding ) + ifMenuButton( padding ) will be handled separately
             */

            int width   = 0;
            int height  = newSize.height();
            int padding = 4;

            QSize textSize = toolBtn->fontMetrics.size( Qt::TextShowMnemonic, toolBtn->text );

            /** Button with or without menu are the same when there is no arrow */
            bool needsArrow  = toolBtn->features & QStyleOptionToolButton::HasMenu || toolBtn->features & QStyleOptionToolButton::Menu;
            bool hasArrowBtn = toolBtn->features & QStyleOptionToolButton::MenuButtonPopup;

            switch ( style ) {
                case Qt::ToolButtonIconOnly: {
                    height  = std::max( 32, padding + toolBtn->iconSize.height() + padding );
                    padding = (height - toolBtn->iconSize.height() ) / 2;

                    width  = padding + toolBtn->iconSize.width() + padding;
                    width += (needsArrow ? std::min( width, 32 - padding ) + (hasArrowBtn ? padding : 0) : 0);
                    break;
                }

                case Qt::ToolButtonTextOnly: {
                    width  = padding + textSize.width() + padding;
                    width += (needsArrow ? std::min( width, 32 - padding ) + (hasArrowBtn ? padding : 0) : 0);
                    height = std::max( 32, padding + toolBtn->iconSize.height() + padding );
                    break;
                }

                case Qt::ToolButtonTextBesideIcon: {
                    height  = std::max( 32, padding + toolBtn->iconSize.height() + padding );
                    padding = (height - toolBtn->iconSize.height() ) / 2;
                    width   = padding;

                    if ( toolBtn->iconSize.width() ) {
                        width += toolBtn->iconSize.width() + padding;
                        width += (needsArrow ? std::min( width, 32 - padding ) + (hasArrowBtn ? padding : 0) : 0);
                    }

                    if ( toolBtn->text.length() ) {
                        width += textSize.width() + padding;
                    }

                    break;
                }

                case Qt::ToolButtonTextUnderIcon: {
                    width  = padding + std::max( toolBtn->iconSize.width(), textSize.width() ) + padding;
                    width += (needsArrow ? std::min( width, 32 - padding ) + (hasArrowBtn ? padding : 0) : 0);
                    height = padding + toolBtn->iconSize.height() + padding + textSize.height() + padding;
                    break;
                }

                default: {
                    qWarning() << "Should not have come here!!";
                    break;
                }
            }

            newSize = QSize( width, height );

            break;
        }

        case CT_ComboBox: {
            newSize += QSize( 0, Ph::dpiScaled( 4 + Phantom::DefaultFrameWidth ) );
            auto cb = qstyleoption_cast<const QStyleOptionComboBox *>( option );

            // Non-editable combo boxes have some extra padding on the left side,
            // similar to push buttons. We should account for that here to avoid text
            // being clipped off.
            if ( cb ) {
                int pad = 0;

                if ( cb->editable ) {
                    pad = Ph::dpiScaled( Ph::LineEdit_ContentsHPad );
                }
                else {
                    pad = Ph::dpiScaled( Ph::ComboBox_NonEditable_ContentsHPad );
                }

                newSize.rwidth() += pad * 2;
                newSize.rheight() = std::max( 32, newSize.height() );
            }

            break;
        }

        case CT_LineEdit: {
            newSize += QSize( 0, 4 );
            int pad = Ph::dpiScaled( Ph::LineEdit_ContentsHPad );
            newSize.rwidth() += pad * 2;

            newSize.rheight() = std::max( 32, newSize.height() );

            break;
        }

        case CT_SpinBox: {
            newSize.rwidth() = std::max( 64, newSize.width() );
            break;
        }

        case CT_SizeGrip: {
            newSize += QSize( 4, 4 );
            break;
        }

        case CT_MdiControls: {
            newSize -= QSize( 1, 0 );
            break;
        }

        case CT_ProgressBar: {
            newSize.setHeight( std::max( 32, newSize.height() ) );
            break;
        }

        default: {
            break;
        }
    }
    return newSize;
}


QRect OnyxStyle::subControlRect( ComplexControl control, const QStyleOptionComplex *option, SubControl subControl, const QWidget *widget ) const {
    namespace Ph = Phantom;
    QRect rect = QCommonStyle::subControlRect( control, option, subControl, widget );

    switch ( control ) {
        case CC_Slider: {
            auto slider = qstyleoption_cast<const QStyleOptionSlider *>( option );

            if ( !slider ) {
                break;
            }

            int tickSize = proxy()->pixelMetric( PM_SliderTickmarkOffset, option, widget );
            switch ( subControl ) {
                case SC_SliderHandle: {
                    if ( slider->orientation == Qt::Horizontal ) {
                        rect.setHeight( proxy()->pixelMetric( PM_SliderThickness ) );
                        rect.setWidth( proxy()->pixelMetric( PM_SliderLength ) );
                        int centerY = slider->rect.center().y() - rect.height() / 2;

                        if ( slider->tickPosition & QSlider::TicksAbove ) {
                            centerY += tickSize;
                        }

                        if ( slider->tickPosition & QSlider::TicksBelow ) {
                            centerY -= tickSize;
                        }

                        rect.moveTop( centerY );
                    }
                    else {
                        rect.setWidth( proxy()->pixelMetric( PM_SliderThickness ) );
                        rect.setHeight( proxy()->pixelMetric( PM_SliderLength ) );
                        int centerX = slider->rect.center().x() - rect.width() / 2;

                        if ( slider->tickPosition & QSlider::TicksAbove ) {
                            centerX += tickSize;
                        }

                        if ( slider->tickPosition & QSlider::TicksBelow ) {
                            centerX -= tickSize;
                        }

                        rect.moveLeft( centerX );
                    }

                    break;
                }

                case SC_SliderGroove: {
                    QPoint    grooveCenter    = slider->rect.center();
                    const int grooveThickness = Ph::dpiScaled( 7 );

                    if ( slider->orientation == Qt::Horizontal ) {
                        rect.setHeight( grooveThickness );

                        if ( slider->tickPosition & QSlider::TicksAbove ) {
                            grooveCenter.ry() += tickSize;
                        }

                        if ( slider->tickPosition & QSlider::TicksBelow ) {
                            grooveCenter.ry() -= tickSize;
                        }
                    }
                    else {
                        rect.setWidth( grooveThickness );

                        if ( slider->tickPosition & QSlider::TicksAbove ) {
                            grooveCenter.rx() += tickSize;
                        }

                        if ( slider->tickPosition & QSlider::TicksBelow ) {
                            grooveCenter.rx() -= tickSize;
                        }
                    }

                    rect.moveCenter( grooveCenter );
                    break;
                }

                default: {
                    break;
                }
            }
            break;
        }

        case CC_SpinBox: {
            auto spinbox = qstyleoption_cast<const QStyleOptionSpinBox *>( option );

            if ( !spinbox ) {
                break;
            }

            // Some leftover Fusion code here. Should clean up this mess.
            int       center      = spinbox->rect.height() / 2;
            const int buttonWidth = static_cast<int>(Ph::dpiScaled( Ph::SpinBox_ButtonWidth ) ) + 2;

            switch ( subControl ) {
                case SC_SpinBoxUp: {
                    if ( spinbox->buttonSymbols == QAbstractSpinBox::NoButtons ) {
                        return {};
                    }

                    rect = QRect( spinbox->rect.width() - buttonWidth, 0, buttonWidth, center );
                    break;
                }

                case SC_SpinBoxDown: {
                    if ( spinbox->buttonSymbols == QAbstractSpinBox::NoButtons ) {
                        return QRect();
                    }

                    rect = QRect( spinbox->rect.width() - buttonWidth, center, buttonWidth, center );
                    break;
                }

                case SC_SpinBoxEditField: {
                    if ( spinbox->buttonSymbols == QAbstractSpinBox::NoButtons ) {
                        rect = QRect( QPoint( 0, 0 ), spinbox->rect.size() );
                    }

                    else {
                        rect = QRect( 0, 0, spinbox->rect.width() - buttonWidth, spinbox->rect.height() );
                    }

                    break;
                }

                case SC_SpinBoxFrame: {
                    rect = spinbox->rect;
                    break;
                }

                default: {
                    break;
                }
            }

            break;
        }

        case CC_GroupBox: {
            auto groupBox = qstyleoption_cast<const QStyleOptionGroupBox *>( option );

            if ( !groupBox ) {
                break;
            }

            switch ( subControl ) {
                case SC_GroupBoxFrame:
                case SC_GroupBoxContents: {
                    QRect r = option->rect;

                    if ( groupBox->subControls & (SC_GroupBoxLabel | SC_GroupBoxCheckBox) ) {
                        int fontHeight = option->fontMetrics.height();
                        int topMargin  = std::max( pixelMetric( PM_ExclusiveIndicatorHeight ), fontHeight );
                        topMargin += static_cast<int>(fontHeight * Ph::GroupBox_LabelBottomMarginFontRatio);
                        r.setTop( r.top() + topMargin );
                    }

                    if ( (subControl == SC_GroupBoxContents) && groupBox->subControls & SC_GroupBoxFrame ) {
                        // Testing against groupBox->features for the frame type doesn't seem
                        // to work here.
                        r.adjust( 1, 1, -1, -1 );
                    }

                    return r;
                }

                case SC_GroupBoxCheckBox:
                case SC_GroupBoxLabel: {
                    // Accurate height doesn't matter -- the other group box style
                    // implementations also fail with multi-line or too-tall text.
                    int textHeight = option->fontMetrics.height();
                    // width()/horizontalAdvance() is faster than size() and good enough for
                    // us, since we only support a single line of text here anyway.
                    int textWidth           = Phantom::fontMetricsWidth( option->fontMetrics, groupBox->text );
                    int indicatorWidth      = proxy()->pixelMetric( PM_IndicatorWidth, option, widget );
                    int indicatorHeight     = proxy()->pixelMetric( PM_IndicatorHeight, option, widget );
                    int margin              = 0;
                    int indicatorRightSpace = textHeight / 3;
                    int contentWidth        = textWidth;

                    if ( option->subControls & QStyle::SC_GroupBoxCheckBox ) {
                        contentWidth += indicatorWidth + indicatorRightSpace;
                    }

                    int x = margin;
                    int y = 0;
                    switch ( groupBox->textAlignment & Qt::AlignHorizontal_Mask ) {
                        case Qt::AlignHCenter: {
                            x += (option->rect.width() - contentWidth) / 2;
                            break;
                        }

                        case Qt::AlignRight: {
                            x += option->rect.width() - contentWidth;
                            break;
                        }

                        default: {
                            break;
                        }
                    }
                    int w, h;

                    if ( subControl == SC_GroupBoxCheckBox ) {
                        w = indicatorWidth;
                        h = indicatorHeight;

                        if ( textHeight > indicatorHeight ) {
                            y = (textHeight - indicatorHeight) / 2;
                        }
                    }
                    else {
                        w = contentWidth;
                        h = textHeight;

                        if ( option->subControls & QStyle::SC_GroupBoxCheckBox ) {
                            x += indicatorWidth + indicatorRightSpace;
                            w -= indicatorWidth + indicatorRightSpace;
                        }
                    }

                    return visualRect( option->direction, option->rect, QRect( x, y, w, h ) );
                }

                default: {
                    break;
                }
            }
            break;
        }

        case CC_ComboBox: {
            auto cb = qstyleoption_cast<const QStyleOptionComboBox *>( option );

            if ( !cb ) {
                return QRect();
            }

            int   frame = cb->frame ? proxy()->pixelMetric( PM_ComboBoxFrameWidth, cb, widget ) : 0;
            QRect r     = option->rect;
            r.adjust( frame, frame, -frame, -frame );
            int dim = std::min( r.width(), r.height() );

            if ( dim < 1 ) {
                return QRect();
            }

            switch ( subControl ) {
                case SC_ComboBoxFrame: {
                    return cb->rect;
                }

                case SC_ComboBoxArrow: {
                    QRect r0 = r;
                    r0.setX( (r0.x() + r0.width() ) - dim + 1 );
                    return visualRect( option->direction, option->rect, r0 );
                }

                case SC_ComboBoxEditField: {
                    // Add extra padding if not editable
                    int pad = 0;

                    if ( cb->editable ) {
                        // Line edit padding already added
                    }
                    else {
                        pad = Ph::dpiScaled( Ph::ComboBox_NonEditable_ContentsHPad );
                    }

                    r.adjust( pad, 0, -dim, 0 );
                    return visualRect( option->direction, option->rect, r );
                }

                case SC_ComboBoxListBoxPopup: {
                    //return cb->rect;
                    //rekols
                    QSize size = proxy()->sizeFromContents( CT_Menu, option, option->rect.size(), widget );
                    QRect rect = option->rect;

                    if ( auto styleOption = static_cast<const QStyleOption *>(option) ) {
                        if ( auto menuItem = static_cast<const QStyleOptionMenuItem *>(styleOption) ) {
                            if ( menuItem->icon.isNull() ) {
                                rect.setWidth( size.width() );
                            }
                        }
                    }

                    return rect;
                }

                default: {
                    break;
                }
            }
            break;
        }

        case CC_TitleBar: {
            auto tb = qstyleoption_cast<const QStyleOptionTitleBar *>( option );

            if ( !tb ) {
                break;
            }

            SubControl sc                  = subControl;
            QRect&     ret                 = rect;
            const int  indent              = 3;
            const int  controlTopMargin    = 3;
            const int  controlBottomMargin = 3;
            const int  controlWidthMargin  = 2;
            const int  controlHeight       = tb->rect.height() - controlTopMargin - controlBottomMargin;
            const int  delta               = controlHeight + controlWidthMargin;
            int        offset              = 0;
            bool       isMinimized         = tb->titleBarState & Qt::WindowMinimized;
            bool       isMaximized         = tb->titleBarState & Qt::WindowMaximized;
            switch ( sc ) {
                case SC_TitleBarLabel: {
                    if ( tb->titleBarFlags & (Qt::WindowTitleHint | Qt::WindowSystemMenuHint) ) {
                        ret = tb->rect;

                        if ( tb->titleBarFlags & Qt::WindowSystemMenuHint ) {
                            ret.adjust( delta, 0, -delta, 0 );
                        }

                        if ( tb->titleBarFlags & Qt::WindowMinimizeButtonHint ) {
                            ret.adjust( 0, 0, -delta, 0 );
                        }

                        if ( tb->titleBarFlags & Qt::WindowMaximizeButtonHint ) {
                            ret.adjust( 0, 0, -delta, 0 );
                        }

                        if ( tb->titleBarFlags & Qt::WindowShadeButtonHint ) {
                            ret.adjust( 0, 0, -delta, 0 );
                        }

                        if ( tb->titleBarFlags & Qt::WindowContextHelpButtonHint ) {
                            ret.adjust( 0, 0, -delta, 0 );
                        }
                    }

                    break;
                }

                case SC_TitleBarContextHelpButton: {
                    if ( tb->titleBarFlags & Qt::WindowContextHelpButtonHint ) {
                        offset += delta;
                    }

                    Q_FALLTHROUGH();
                }

                case SC_TitleBarMinButton: {
                    if ( !isMinimized && (tb->titleBarFlags & Qt::WindowMinimizeButtonHint) ) {
                        offset += delta;
                    }
                    else if ( sc == SC_TitleBarMinButton ) {
                        break;
                    }

                    Q_FALLTHROUGH();
                }

                case SC_TitleBarNormalButton: {
                    if ( isMinimized && (tb->titleBarFlags & Qt::WindowMinimizeButtonHint) ) {
                        offset += delta;
                    }
                    else if ( isMaximized && (tb->titleBarFlags & Qt::WindowMaximizeButtonHint) ) {
                        offset += delta;
                    }
                    else if ( sc == SC_TitleBarNormalButton ) {
                        break;
                    }

                    Q_FALLTHROUGH();
                }

                case SC_TitleBarMaxButton: {
                    if ( !isMaximized && (tb->titleBarFlags & Qt::WindowMaximizeButtonHint) ) {
                        offset += delta;
                    }
                    else if ( sc == SC_TitleBarMaxButton ) {
                        break;
                    }

                    Q_FALLTHROUGH();
                }

                case SC_TitleBarShadeButton: {
                    if ( !isMinimized && (tb->titleBarFlags & Qt::WindowShadeButtonHint) ) {
                        offset += delta;
                    }
                    else if ( sc == SC_TitleBarShadeButton ) {
                        break;
                    }

                    Q_FALLTHROUGH();
                }

                case SC_TitleBarUnshadeButton: {
                    if ( isMinimized && (tb->titleBarFlags & Qt::WindowShadeButtonHint) ) {
                        offset += delta;
                    }
                    else if ( sc == SC_TitleBarUnshadeButton ) {
                        break;
                    }

                    Q_FALLTHROUGH();
                }

                case SC_TitleBarCloseButton: {
                    if ( tb->titleBarFlags & Qt::WindowSystemMenuHint ) {
                        offset += delta;
                    }
                    else if ( sc == SC_TitleBarCloseButton ) {
                        break;
                    }

                    ret.setRect(
                        tb->rect.right() - indent - offset, tb->rect.top() + controlTopMargin, controlHeight, controlHeight );
                    break;
                }

                case SC_TitleBarSysMenu: {
                    if ( tb->titleBarFlags & Qt::WindowSystemMenuHint ) {
                        ret.setRect(
                            tb->rect.left() + controlWidthMargin + indent,
                            tb->rect.top() + controlTopMargin,
                            controlHeight,
                            controlHeight
                        );
                    }

                    break;
                }

                default: {
                    break;
                }
            }
            rect = visualRect( tb->direction, tb->rect, ret );
            break;
        }

        case CC_ToolButton: {
            if ( const QStyleOptionToolButton *tb = qstyleoption_cast<const QStyleOptionToolButton *>( option ) ) {
                /** We're not using this currently */
                // int mbi = proxy()->pixelMetric( PM_MenuButtonIndicator, tb, widget );

                /** Button rect */
                rect = tb->rect;

                /** Do we have a dedicated arrow button? */
                bool hasArrowBtn = tb->features & QStyleOptionToolButton::MenuButtonPopup;

                switch ( subControl ) {
                    case SC_ToolButton: {
                        if ( hasArrowBtn ) {
                            /** Reduce the width by 32px */
                            rect.adjust( 0, 0, -32, 0 );
                        }

                        break;
                    }

                    case SC_ToolButtonMenu: {
                        if ( hasArrowBtn ) {
                            rect.adjust( rect.width() - 32, 0, 0, 0 );
                        }

                        break;
                    }

                    default: {
                        break;
                    }
                }
            }

            break;
        }

        default: {
            break;
        }
    }

    return rect;
}


QRect OnyxStyle::itemPixmapRect( const QRect& r, int flags, const QPixmap& pixmap ) const {
    return QCommonStyle::itemPixmapRect( r, flags, pixmap );
}


QRect OnyxStyle::subElementRect( SubElement sr, const QStyleOption *opt, const QWidget *w ) const {
    switch ( sr ) {
        case SE_ProgressBarLabel:
        case SE_ProgressBarContents:
        case SE_ProgressBarGroove: {
            return opt->rect;
        }

        case SE_DockWidgetTitleBarText: {
            auto titlebar = qstyleoption_cast<const QStyleOptionDockWidget *>( opt );

            if ( !titlebar ) {
                break;
            }

            QRect r = QCommonStyle::subElementRect( sr, opt, w );
            bool  verticalTitleBar = titlebar->verticalTitleBar;

            if ( verticalTitleBar ) {
                r.adjust( 0, 0, 0, -4 );
            }
            else {
                if ( opt->direction == Qt::LeftToRight ) {
                    r.adjust( 4, 0, 0, 0 );
                }
                else {
                    r.adjust( 0, 0, -4, 0 );
                }
            }

            return r;
        }

        case SE_TreeViewDisclosureItem: {
            if ( Phantom::BranchesOnEdge ) {
                // Shove it all the way to the left (or right) side, probably outside of
                // the rect it gave us. Old-school.
                QRect rect = opt->rect;

                if ( opt->direction != Qt::RightToLeft ) {
                    rect.moveLeft( 0 );

                    if ( rect.width() < rect.height() ) {
                        rect.setWidth( rect.height() );
                    }
                }
                else {
                    // todo
                }

                return rect;
            }

            break;
        }

        case SE_LineEditContents: {
            QRect r   = QCommonStyle::subElementRect( sr, opt, w );
            int   pad = Phantom::dpiScaled( Phantom::LineEdit_ContentsHPad );
            return r.adjusted( pad, 0, -pad, 0 );
        }

        default: {
            break;
        }
    }
    return QCommonStyle::subElementRect( sr, opt, w );
}
