/**
 * This file is a part of DesQ Appearance.
 * DesQ Appearance is the Settings plugin to manage DesQ's widget theme,
 * fonts, colors and other related configurations.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This plugin is derived from cutefish
 * Copyright (C) 2020 Reven Martin
 * Copyright (C) 2020 KeePassXC Team <team@keepassxc.org>
 * Copyright (C) 2019 Andrew Richards
 *
 * Derived from Phantomstyle and relicensed under the GPLv2 or v3.
 * https://github.com/randrew/phantomstyle
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#ifndef OnyxStyle_H
    #define OnyxStyle_H

    #include <QCommonStyle>
    #include <DFSettings.hpp>

    extern DFL::Settings *styleSett;

    class OnyxStylePrivate;
    class OnyxStyle : public QCommonStyle {
        Q_OBJECT

        public:
            OnyxStyle();
            ~OnyxStyle() override;

            enum PhantomPrimitiveElement {
                Phantom_PE_IndicatorTabNew = PE_CustomBase + 1,
                Phantom_PE_ScrollBarSliderVertical,
                Phantom_PE_WindowFrameColor,
            };

            /** Style hints: like position of the spinbox buttons, or if shortcuts are underlined */
            int styleHint( StyleHint hint, const QStyleOption *option = nullptr, const QWidget *widget = nullptr, QStyleHintReturn *returnData = nullptr ) const override;

            /** Draw a primitive element: These are the simplest elements there can be - a frame, panel for
             * a
             * button, etc */
            void drawPrimitive( PrimitiveElement elem, const QStyleOption *option, QPainter *painter, const QWidget *widget = nullptr ) const override;

            /** Draw a control element: text of a pushbutton, or focus rect of a checkbox */
            void drawControl( ControlElement ce, const QStyleOption *option, QPainter *painter, const QWidget *widget ) const override;

            /** Draw a control element: Different behaviour based on where the user clicks */
            void drawComplexControl( ComplexControl control, const QStyleOptionComplex *option, QPainter *painter, const QWidget *widget ) const override;

            /** Helper to get the subcontrol on which the user clicks */
            SubControl hitTestComplexControl( ComplexControl cc, const QStyleOptionComplex *opt, const QPoint& pt, const QWidget *w = nullptr ) const override;

            /** Pixmap modified as per icon mode and palette */
            QPixmap generatedIconPixmap( QIcon::Mode iconMode, const QPixmap& pixmap, const QStyleOption *opt ) const override;

            /** Draw the item pixmap */
            void drawItemPixmap( QPainter *painter, const QRect& rect, int alignment, const QPixmap& pixmap ) const override;

            /** Draw the item text */
            void drawItemText( QPainter *painter, const QRect& rect, int flags, const QPalette& pal, bool enabled, const QString& text, QPalette::ColorRole textRole = QPalette::NoRole ) const override;

            /** Sizes of various elements: slider thickness or checkbox width or height, etc */
            int pixelMetric( PixelMetric metric, const QStyleOption *option = nullptr, const QWidget *widget = nullptr ) const override;

            /** Area occupied by a sub element: text area of a pushbutton, radio button focus rect */
            QRect subElementRect( SubElement r, const QStyleOption *opt, const QWidget *widget = nullptr ) const override;

            /** Get the size of the elements from their contents: Checkbox, or a line edit */
            QSize sizeFromContents( ContentsType type, const QStyleOption *option, const QSize& size, const QWidget *widget ) const override;

            /** Get the area of a subcontrol: spinbox up/down button, tool button, and it's menu, etc */
            QRect subControlRect( ComplexControl cc, const QStyleOptionComplex *opt, SubControl sc, const QWidget *widget ) const override;

            /** Area covered by the icon */
            QRect itemPixmapRect( const QRect& r, int flags, const QPixmap& pixmap ) const override;

            using QCommonStyle::polish;
            void polish( QApplication *app ) override;
            void unpolish( QApplication *app ) override;
            void polish( QWidget *widget ) override;
            void unpolish( QWidget *widget ) override;

        protected:

            /**
             * @return Paths to application stylesheets
             */
            virtual QString getAppStyleSheet() const {
                return {};
            }

            OnyxStylePrivate *d;

        private:
            int mDialogButtonsHaveIcons;
            int mActivateItemOnSingleClick;
            int mMnemnonics;
    };

#endif
