/**
 * This file is a part of DesQ Appearance.
 * DesQ Appearance is the Settings plugin to manage DesQ's widget theme,
 * fonts, colors and other related configurations.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This plugin is derived from Qt5CT project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"

class FontsPage : public QWidget {
    Q_OBJECT;

    public:
        FontsPage( QWidget *parent = nullptr );
        ~FontsPage();

    private:
        void createGUI();
        void loadDefaults();
        void storeFCSettings();

        QFont mGeneralFont = QFont( "Quicksand", 10, QFont::Normal );
        QFont mFixedFont   = QFont( "Fira Code", 10, QFont::Normal );
        QFont mSmallFont   = QFont( "Quicksand", 8, QFont::Normal );
        QFont mToolbarFont = QFont( "Quicksand", 9, QFont::Normal );
        QFont mMenuFont    = QFont( "Quicksand", 9, QFont::Normal );

        QCheckBox *aaCB;
        QCheckBox *hintCB;
        QComboBox *styleCB;
        QComboBox *subpxlCB;
        QCheckBox *autoHintCB;
        QCheckBox *noBoldHintCB;
        QComboBox *lcdFilterCB;
        QSpinBox *fresSB;

    Q_SIGNALS:
        void changeFont( QFont );
};
