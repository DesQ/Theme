/**
 * This file is a part of DesQ Appearance.
 * DesQ Appearance is the Settings plugin to manage DesQ's widget theme,
 * fonts, colors and other related configurations.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This plugin is derived from Qt5CT project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <DFXdg.hpp>
#include <DFColorScheme.hpp>
#include <desq/desq-config.h>

#include "ColorsPage.hpp"
#include "Global.hpp"
#include "PreviewWidget.hpp"
#include "Widgets.hpp"


static QString colorSS(
    "QToolButton {"
    "   border: none;"
    "   border-radius: 12px;"
    "   background-color: %1;"
    "}"
    "QToolButton:hover {"
    "   border: 2px solid palette(Highlight);"
    "}"
    "QToolButton:pressed {"
    "   border: 2px solid palette(Highlight);"
    "}"
    "QToolButton:checked {"
    "   border: 2px solid palette(Highlight);"
    "}"
);


ColorsPage::ColorsPage( QWidget *parent ) : QWidget( parent ) {
    DFL::Config::ColorScheme::addColorSchemePaths(
        {
            ThemePath "ColorSchemes",
            DFL::XDG::xdgDataHome() + "DesQ/Themes/ColorSchemes",
        }
    );

    createGUI();

    QString style = (QString)settings->value( "Appearance::Styles::WidgetStyle" );
    QString color = (QString)settings->value( "Appearance::Colors::Scheme" );

    for ( QString cs: DFL::Config::ColorScheme::colorSchemes() ) {
        QPixmap         pix   = PreviewWidget::preview( style, cs );
        QListWidgetItem *item = new QListWidgetItem( QIcon( pix ), cs, colors );
        colors->addItem( item );
    }

    QList<QListWidgetItem *> selColorItems = colors->findItems( color, Qt::MatchFixedString );

    if ( selColorItems.count() ) {
        colors->setCurrentItem( selColorItems.at( 0 ) );
    }

    connect(
        colors, &QListWidget::currentTextChanged, [ = ]( QString newColors ) {
            // Emit the signal to update the preview dialog
            emit changeColorScheme( newColors );

            /** Save the setting */
            settings->setValue( "Appearance::Colors::Scheme", newColors );
        }
    );
}


ColorsPage::~ColorsPage() {
    delete colors;
}


void ColorsPage::createGUI() {
    colors = new ThemeView();

    QVBoxLayout *baseLyt = new QVBoxLayout();

    baseLyt->addWidget( new AccentColorSwatch( this ) );
    baseLyt->addWidget( colors );
    setLayout( baseLyt );

    setMinimumSize( 800, 600 );
}


AccentColorSwatch::AccentColorSwatch( QWidget *parent ): QWidget( parent ) {
    QMap<QString, QColor> colorHash;

    colorHash[ "black" ]   = QColor( "#242124" );
    colorHash[ "blue" ]    = QColor( "#1F75FE" );
    colorHash[ "brown" ]   = QColor( "#832A0D" );
    colorHash[ "cyan" ]    = QColor( "#20B2AA" );
    colorHash[ "green" ]   = QColor( "#009F6B" );
    colorHash[ "gray" ]    = QColor( "#4C5866" );
    colorHash[ "magenta" ] = QColor( "#D0417E" );
    colorHash[ "orange" ]  = QColor( "#ED9121" );
    colorHash[ "red" ]     = QColor( "#C51E3A" );
    colorHash[ "violet" ]  = QColor( "#4C2882" );
    colorHash[ "yellow" ]  = QColor( "#795D00" );

    QHBoxLayout *lyt = new QHBoxLayout();

    lyt->addStretch();
    lyt->addWidget( new QLabel( "Accent Color:" ) );

    QButtonGroup *btnGrp = new QButtonGroup();

    for ( QString key: colorHash.keys() ) {
        QToolButton *btn = new QToolButton();

        btn->setFixedSize( 24, 24 );

        btn->setAutoRaise( true );
        btn->setCheckable( true );
        btn->setChecked( false );

        QColor color( colorHash[ key ] );
        btn->setStyleSheet( colorSS.arg( color.name() ) );
        btn->setProperty( "color", key );
        btn->setToolTip(
            QString( "%1<br>%2, %3, %4" )
               .arg( key )
               .arg( color.red() )
               .arg( color.green() )
               .arg( color.blue() )
        );

        btnGrp->addButton( btn );

        lyt->addWidget( btn );

        connect(
            btn, &QToolButton::clicked, [ = ] () {
                QStringList rgb = QString( "%1,%2,%3" ).arg( color.red() ).arg( color.green() ).arg( color.blue() ).split( "," );
                QSettings kdeglobals( QDir::home().filePath( ".config/kdeglobals.desq" ), QSettings::IniFormat );
                kdeglobals.setValue( "AccentColor",               rgb );
                kdeglobals.setValue( "LastUsedCustomAccentColor", rgb );
                kdeglobals.sync();

                settings->setValue( "Appearance::Colors::Accent", color.name() );
            }
        );
    }

    lyt->addStretch();

    setLayout( lyt );
}
