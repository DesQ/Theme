/**
 * This file is a part of DesQ Appearance.
 * DesQ Appearance is the Settings plugin to manage DesQ's widget theme,
 * fonts, colors and other related configurations.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This plugin is derived from Qt5CT project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <DFColorScheme.hpp>
#include "PreviewWidget.hpp"

PreviewWidget::PreviewWidget( QWidget *parent = nullptr ) : QWidget( parent ) {
    QWidget *tab1 = new QWidget();

    QLineEdit *lineEdit = new QLineEdit( this );

    lineEdit->setPlaceholderText( "Type something here..." );

    QSpinBox *spinBox = new QSpinBox( this );

    spinBox->setRange( 0, 100 );
    spinBox->setValue( 27 );

    QPushButton *button = new QPushButton( "Push Button", this );

    connect(
        button, &QPushButton::clicked, [ = ]() {
            spinBox->setValue( 27 );
        }
    );

    QProgressBar *progress = new QProgressBar( this );

    progress->setRange( 0, 100 );
    progress->setValue( 27 );

    connect( spinBox, qOverload<int>( &QSpinBox::valueChanged ), progress, &QProgressBar::setValue );

    QGridLayout *grid = new QGridLayout();

    grid->addWidget( lineEdit, 0, 0 );
    grid->addWidget( spinBox,  0, 1 );
    grid->addWidget( button,   1, 0 );
    grid->addWidget( progress, 1, 1 );
    tab1->setLayout( grid );

    QWidget *tab2 = new QWidget();

    QRadioButton *radioBtn = new QRadioButton( "&Radio Button" );
    QCheckBox    *checkBox = new QCheckBox( "&Check Box" );

    QVBoxLayout *vbox = new QVBoxLayout();

    vbox->addWidget( radioBtn );
    vbox->addWidget( checkBox );
    tab2->setLayout( vbox );

    tabWidget = new QTabWidget( this );
    tabWidget->setMinimumSize( QSize( 256, 128 ) );
    tabWidget->addTab( tab1, "Tab 1" );
    tabWidget->addTab( tab2, "Tab 2" );

    QFrame *frame = new QFrame();

    frame->setFrameStyle( QFrame::StyledPanel | QFrame::Plain );
    QVBoxLayout *frameLyt = new QVBoxLayout();

    frameLyt->addWidget( tabWidget );

    frame->setLayout( frameLyt );

    QVBoxLayout *baseLyt = new QVBoxLayout();

    baseLyt->addWidget( frame );
    setLayout( baseLyt );

    setFixedSize( 360, 180 );
}


QSize PreviewWidget::minimumSizeHint() {
    return QSize( 270, 162 );
}


void PreviewWidget::setStyle( QString style ) {
    if ( not style.length() ) {
        return;
    }

    QStyle *ws = QStyleFactory::create( style );

    QWidget::setStyle( ws );
    setPalette( ws->standardPalette() );
    for ( QWidget *w: findChildren<QWidget *>() ) {
        w->setStyle( ws );
        w->setPalette( ws->standardPalette() );
    }
}


void PreviewWidget::setColorScheme( QString colors ) {
    DFL::Config::ColorScheme cs( colors );
    QPalette                 pltt = cs.palette();

    for ( QWidget *w: findChildren<QWidget *>() ) {
        w->setPalette( pltt );
    }
    setPalette( pltt );
}


QPixmap PreviewWidget::preview( QString styleName, QString colorScheme ) {
    PreviewWidget *preview = new PreviewWidget( nullptr );

    preview->setStyle( styleName );
    preview->setColorScheme( colorScheme );

    QPixmap pix = preview->grab();

    delete preview;

    return pix;
}
