/**
 * This file is a part of DesQ Appearance.
 * DesQ Appearance is the Settings plugin to manage DesQ's widget theme,
 * fonts, colors and other related configurations.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This plugin is derived from Qt5CT project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QtWidgets>

class ThemeView : public QListWidget {
    Q_OBJECT;

    public:
        ThemeView();

        void setIconSize( QSize );

    private:
        int viewportWidth = 512;
        int mSpacing      = 10;

    protected:
        void resizeEvent( QResizeEvent *rEvent ) override;
};

QLineEdit * createLineEdit( QFont font, QString text, bool wide );
QWidget * separator( Qt::Orientation o = Qt::Horizontal );
