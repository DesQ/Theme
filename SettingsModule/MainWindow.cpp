/**
 * This file is a part of DesQ Appearance.
 * DesQ Appearance is the Settings plugin to manage DesQ's widget theme,
 * fonts, colors and other related configurations.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This plugin is derived from Qt5CT project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QApplication>
#include <QStyle>
#include <QProcessEnvironment>
#include <QtWidgets>

#include <desq/Utils.hpp>

#include "MainWindow.hpp"
#include "StylesPage.hpp"
#include "ColorsPage.hpp"
#include "FontsPage.hpp"
#include "PreviewWidget.hpp"

DFL::Settings *settings = nullptr;

MainWindow::MainWindow() : QWidget() {
    /** Initialize the settings */
    settings = DesQ::Utils::initializeDesQSettings( "Looks", "Looks", true );

    themePage = new StylesPage( this );
    colorPage = new ColorsPage( this );
    fontsPage = new FontsPage( this );

    tabWidget = new QTabWidget( this );
    tabWidget->addTab( themePage, QString( "Appearance" ) );
    tabWidget->addTab( colorPage, QString( "Colors" ) );
    tabWidget->addTab( fontsPage, QString( "Fonts" ) );
    // tabWidget->addTab( iconsPage, QString( "Icons" ) );
    // tabWidget->addTab( ifacePage, QString( "Interface" ) );

    setWindowIcon( QIcon::fromTheme( "preferences-desktop-theme" ) );

    QVBoxLayout *baseLyt = new QVBoxLayout();

    baseLyt->addWidget( tabWidget );
    setLayout( baseLyt );
}


MainWindow::~MainWindow() {
    delete themePage;
    delete colorPage;
    delete fontsPage;
}
