/**
 * This file is a part of DesQ Appearance.
 * DesQ Appearance is the Settings plugin to manage DesQ's widget theme,
 * fonts, colors and other related configurations.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This plugin is derived from Qt5CT project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <DFXdg.hpp>
#include <DFSettings.hpp>
#include <desq/Utils.hpp>

#include "FontsPage.hpp"
#include "Widgets.hpp"


static inline QString getFontAsString( QFont f ) {
    return QString( "%1, %2, %3, %4" ).arg( f.family() ).arg( f.pointSize() ).arg( (f.weight() > QFont::Medium ? true : false) ).arg( f.italic() );
}


static inline QFont getFontFromString( QVariant fontData ) {
    QStringList fontParts = fontData.toString().split( ", " );

    return QFont(
        fontParts.value( 0 ),
        fontParts.value( 1 ).toInt(),
        (fontParts.value( 2 ) == "true" ? QFont::Bold : QFont::Normal),
        (fontParts.value( 3 ) == "true" ? true : false)
    );
}


FontsPage::FontsPage( QWidget *parent ) : QWidget( parent ) {
    loadDefaults();
    createGUI();
}


FontsPage::~FontsPage() {
}


void FontsPage::createGUI() {
    /** Labels */
    QLabel *genLbl = new QLabel( "&General: " );
    QLabel *fxdLbl = new QLabel( "Fi&xed: " );
    QLabel *smlLbl = new QLabel( "S&mall: " );
    QLabel *tlbLbl = new QLabel( "Tool&bar: " );
    QLabel *mnuLbl = new QLabel( "Me&nu: " );

    /** Font Name LE */
    QLineEdit *genLE = createLineEdit( mGeneralFont, QString(), false );
    QLineEdit *fxdLE = createLineEdit( mFixedFont, QString(), false );
    QLineEdit *smlLE = createLineEdit( mSmallFont, QString(), false );
    QLineEdit *tlbLE = createLineEdit( mToolbarFont, QString(), false );
    QLineEdit *mnuLE = createLineEdit( mMenuFont, QString(), false );

    /** Display LE */
    QString   sampleText = "Jackdaws love my big sphinx of quartz. 01234567890 +-*/ [{(,.)}]";
    QLineEdit *genDispLE = createLineEdit( mGeneralFont, sampleText, true );
    QLineEdit *fxdDispLE = createLineEdit( mFixedFont, sampleText, true );
    QLineEdit *smlDispLE = createLineEdit( mSmallFont, sampleText, true );
    QLineEdit *tlbDispLE = createLineEdit( mToolbarFont, sampleText, true );
    QLineEdit *mnuDispLE = createLineEdit( mMenuFont, sampleText, true );

    /** Font Buttons */
    QToolButton *genBtn = new QToolButton();

    genBtn->setIcon( QIcon::fromTheme( "gtk-select-font" ) );
    genLbl->setBuddy( genBtn );
    connect(
        genBtn, &QToolButton::clicked, [ = ]() {
            bool ok       = false;
            QFont newFont = QFontDialog::getFont( &ok, mGeneralFont, this, "DesQ Looks | Select Font" );

            if ( ok ) {
                genLE->setFont( newFont );
                genLE->setText( QString( "%1 %2pt" ).arg( newFont.family() ).arg( newFont.pointSize() ) );
                genDispLE->setFont( newFont );
                mGeneralFont = newFont;
                qCritical() << "Storing General font";
                settings->setValue( "Fonts::General", getFontAsString( mGeneralFont ) );
            }
        }
    );

    QToolButton *fxdBtn = new QToolButton();

    fxdBtn->setIcon( QIcon::fromTheme( "gtk-select-font" ) );
    fxdLbl->setBuddy( fxdBtn );
    connect(
        fxdBtn, &QToolButton::clicked, [ = ]() {
            bool ok       = false;
            QFont newFont = QFontDialog::getFont( &ok, mFixedFont, this, "DesQ Looks | Select Font" );

            if ( ok ) {
                fxdLE->setFont( newFont );
                fxdLE->setText( QString( "%1 %2pt" ).arg( newFont.family() ).arg( newFont.pointSize() ) );
                fxdDispLE->setFont( newFont );
                mFixedFont = newFont;
                settings->setValue( "Fonts::Fixed", getFontAsString( mFixedFont ) );
            }
        }
    );

    QToolButton *smlBtn = new QToolButton();

    smlBtn->setIcon( QIcon::fromTheme( "gtk-select-font" ) );
    smlLbl->setBuddy( smlBtn );
    connect(
        smlBtn, &QToolButton::clicked, [ = ]() {
            bool ok       = false;
            QFont newFont = QFontDialog::getFont( &ok, mSmallFont, this, "DesQ Looks | Select Font" );

            if ( ok ) {
                smlLE->setFont( newFont );
                smlLE->setText( QString( "%1 %2pt" ).arg( newFont.family() ).arg( newFont.pointSize() ) );
                smlDispLE->setFont( newFont );
                mSmallFont = newFont;
                settings->setValue( "Fonts::Small", getFontAsString( mSmallFont ) );
            }
        }
    );

    QToolButton *tlbBtn = new QToolButton();

    tlbBtn->setIcon( QIcon::fromTheme( "gtk-select-font" ) );
    tlbLbl->setBuddy( tlbBtn );
    connect(
        tlbBtn, &QToolButton::clicked, [ = ]() {
            bool ok       = false;
            QFont newFont = QFontDialog::getFont( &ok, mToolbarFont, this, "DesQ Looks | Select Font" );

            if ( ok ) {
                tlbLE->setFont( newFont );
                tlbLE->setText( QString( "%1 %2pt" ).arg( newFont.family() ).arg( newFont.pointSize() ) );
                tlbDispLE->setFont( newFont );
                mToolbarFont = newFont;
                settings->setValue( "Fonts::Toolbar", getFontAsString( mToolbarFont ) );
            }
        }
    );

    QToolButton *mnuBtn = new QToolButton();

    mnuBtn->setIcon( QIcon::fromTheme( "gtk-select-font" ) );
    mnuLbl->setBuddy( mnuBtn );
    connect(
        mnuBtn, &QToolButton::clicked, [ = ]() {
            bool ok       = false;
            QFont newFont = QFontDialog::getFont( &ok, mMenuFont, this, "DesQ Looks | Select Font" );

            if ( ok ) {
                mnuLE->setFont( newFont );
                mnuLE->setText( QString( "%1 %2pt" ).arg( newFont.family() ).arg( newFont.pointSize() ) );
                mnuDispLE->setFont( newFont );
                mMenuFont = newFont;
                settings->setValue( "Fonts::Menu", getFontAsString( mMenuFont ) );
            }
        }
    );

    QFontComboBox *fontsCB = new QFontComboBox();

    fontsCB->setCurrentFont( mGeneralFont );
    connect(
        fontsCB, &QFontComboBox::currentFontChanged, [ = ] ( QFont newFont ) {
            mGeneralFont.setFamily( newFont.family() );
            mSmallFont.setFamily( newFont.family() );
            mToolbarFont.setFamily( newFont.family() );
            mMenuFont.setFamily( newFont.family() );

            genLE->setFont( mGeneralFont );
            smlLE->setFont( mSmallFont );
            tlbLE->setFont( mToolbarFont );
            mnuLE->setFont( mMenuFont );

            genDispLE->setFont( mGeneralFont );
            smlDispLE->setFont( mSmallFont );
            tlbDispLE->setFont( mToolbarFont );
            mnuDispLE->setFont( mMenuFont );

            genLE->setText( QString( "%1 %2 pt" ).arg( mGeneralFont.family() ).arg( mGeneralFont.pointSize() ) );
            smlLE->setText( QString( "%1 %2 pt" ).arg( mSmallFont.family() ).arg( mSmallFont.pointSize() ) );
            tlbLE->setText( QString( "%1 %2 pt" ).arg( mToolbarFont.family() ).arg( mToolbarFont.pointSize() ) );
            mnuLE->setText( QString( "%1 %2 pt" ).arg( mMenuFont.family() ).arg( mMenuFont.pointSize() ) );
        }
    );

    QPushButton *allFontsBtn = new QPushButton( QIcon::fromTheme( "gtk-select-font" ), "Adjust All &Fonts" );

    allFontsBtn->setCheckable( true );
    allFontsBtn->setChecked( false );
    connect(
        allFontsBtn, &QPushButton::toggled, [ = ]( bool checked ) {
            if ( checked ) {
                fontsCB->show();
            }

            else {
                fontsCB->hide();
            }
        }
    );

    QHBoxLayout *btnLyt = new QHBoxLayout();

    btnLyt->addStretch();
    btnLyt->addWidget( allFontsBtn );
    btnLyt->addWidget( fontsCB );

    fontsCB->hide();

    /** Layout */
    QGridLayout *fontsLyt = new QGridLayout();

    fontsLyt->addWidget( genLbl,    0, 0, Qt::AlignRight | Qt::AlignHCenter );
    fontsLyt->addWidget( genLE,     0, 1 );
    fontsLyt->addWidget( genDispLE, 0, 2 );
    fontsLyt->addWidget( genBtn,    0, 3 );

    fontsLyt->addWidget( fxdLbl,    1, 0, Qt::AlignRight | Qt::AlignHCenter );
    fontsLyt->addWidget( fxdLE,     1, 1 );
    fontsLyt->addWidget( fxdDispLE, 1, 2 );
    fontsLyt->addWidget( fxdBtn,    1, 3 );

    fontsLyt->addWidget( smlLbl,    2, 0, Qt::AlignRight | Qt::AlignHCenter );
    fontsLyt->addWidget( smlLE,     2, 1 );
    fontsLyt->addWidget( smlDispLE, 2, 2 );
    fontsLyt->addWidget( smlBtn,    2, 3 );

    fontsLyt->addWidget( tlbLbl,    3, 0, Qt::AlignRight | Qt::AlignHCenter );
    fontsLyt->addWidget( tlbLE,     3, 1 );
    fontsLyt->addWidget( tlbDispLE, 3, 2 );
    fontsLyt->addWidget( tlbBtn,    3, 3 );

    fontsLyt->addWidget( mnuLbl,    4, 0, Qt::AlignRight | Qt::AlignHCenter );
    fontsLyt->addWidget( mnuLE,     4, 1 );
    fontsLyt->addWidget( mnuDispLE, 4, 2 );
    fontsLyt->addWidget( mnuBtn,    4, 3 );

    fontsLyt->addLayout( btnLyt, 6, 0, 1, 4 );

    setLayout( fontsLyt );

    QGroupBox *fontsGB = new QGroupBox( "Fonts Settings" );

    fontsGB->setLayout( fontsLyt );

    /** Fontconfig settings */
    QLabel *aaLbl = new QLabel( "Anti-A&liasing:" );

    aaCB = new QCheckBox();

    aaCB->setChecked( (bool)settings->value( "Fonts::AppFonts::AntiAliasing" ) );
    aaLbl->setBuddy( aaCB );

    QLabel *hintLbl = new QLabel( "Hin&ting:" );

    hintCB = new QCheckBox();

    hintCB->setChecked( (bool)settings->value( "Fonts::AppFonts::Hinting" ) );
    hintLbl->setBuddy( hintCB );

    QLabel *styleLbl = new QLabel( "&Style:" );

    styleCB = new QComboBox();

    styleCB->addItems( { "None", "Slight", "Medium", "Full" } );
    styleCB->setCurrentText( (QString)settings->value( "Fonts::AppFonts::Style" ) );
    styleLbl->setBuddy( styleCB );

    QLabel *subpxlLbl = new QLabel( "Sub&pixel:" );

    subpxlCB = new QComboBox();

    subpxlCB->addItems( { "None", "RGB", "BGR", "V-RGB", "V-BGR" } );
    subpxlCB->setCurrentText( (QString)settings->value( "Fonts::AppFonts::Subpixel" ) );
    subpxlLbl->setBuddy( subpxlCB );

    connect(
        hintCB, &QCheckBox::toggled, [ = ]( bool checked ) {
            styleLbl->setEnabled( checked );
            styleCB->setEnabled( checked );
            subpxlLbl->setEnabled( checked );
            subpxlCB->setEnabled( checked );
        }
    );

    QLabel *autoHintLbl = new QLabel( "A&uto Hinting:" );

    autoHintCB = new QCheckBox();

    autoHintCB->setChecked( (bool)settings->value( "Fonts::AppFonts::AutoHinting" ) );
    autoHintLbl->setBuddy( autoHintCB );

    noBoldHintCB = new QCheckBox( "&Disable auto-hinting for bold fonts" );

    noBoldHintCB->setChecked( (bool)settings->value( "Fonts::AppFonts::DisableAutoHint" ) );
    noBoldHintCB->setDisabled( true );
    connect( autoHintCB, &QCheckBox::toggled, noBoldHintCB, &QCheckBox::setEnabled );

    QLabel *lcdFilterLbl = new QLabel( "LCD Filte&r:" );

    lcdFilterCB = new QComboBox();

    lcdFilterCB->addItems( { "None", "Default", "Light", "Legacy" } );
    lcdFilterCB->setCurrentText( (QString)settings->value( "Fonts::AppFonts::LCDFilter" ) );
    lcdFilterLbl->setBuddy( lcdFilterCB );

    QLabel *fresLbl = new QLabel( "Font r&esolution:" );

    fresSB = new QSpinBox();

    fresSB->setRange( 0, 1000 );
    fresSB->setValue( (double)settings->value( "Fonts::AppFonts::FontResolution" ) );
    fresLbl->setBuddy( fresSB );

    QPushButton *resetBtn = new QPushButton( QIcon::fromTheme( "document-revert" ), "Restore defaults" );

    connect(
        resetBtn, &QPushButton::clicked, [ = ] () {
            aaCB->setChecked( (bool)settings->value( "Fonts::AppFonts::AntiAliasing" ) );
            hintCB->setChecked( (bool)settings->value( "Fonts::AppFonts::Hinting" ) );
            styleCB->setCurrentText( (QString)settings->value( "Fonts::AppFonts::Style" ) );
            subpxlCB->setCurrentText( (QString)settings->value( "Fonts::AppFonts::Subpixel" ) );
            autoHintCB->setChecked( (bool)settings->value( "Fonts::AppFonts::AutoHinting" ) );
            noBoldHintCB->setChecked( (bool)settings->value( "Fonts::AppFonts::DisableAutoHint" ) );
            lcdFilterCB->setCurrentText( (QString)settings->value( "Fonts::AppFonts::LCDFilter" ) );
            fresSB->setValue( (double)settings->value( "Fonts::AppFonts::FontResolution" ) );
        }
    );

    QPushButton *saveBtn = new QPushButton( QIcon::fromTheme( "document-save" ), "Save config" );

    connect( saveBtn, &QPushButton::clicked, this, &FontsPage::storeFCSettings );

    /** Font Config Layout */
    QGridLayout *fcLyt = new QGridLayout();

    fcLyt->addWidget( aaLbl,        0, 0, Qt::AlignRight );
    fcLyt->addWidget( aaCB,         0, 1, Qt::AlignLeft );

    fcLyt->addWidget( hintLbl,      1, 0, Qt::AlignRight );
    fcLyt->addWidget( hintCB,       1, 1, Qt::AlignLeft );

    fcLyt->addWidget( styleLbl,     2, 1, Qt::AlignLeft );
    fcLyt->addWidget( styleCB,      2, 2, Qt::AlignLeft );

    fcLyt->addWidget( subpxlLbl,    3, 1, Qt::AlignLeft );
    fcLyt->addWidget( subpxlCB,     3, 2, Qt::AlignLeft );

    fcLyt->addWidget( autoHintLbl,  4, 0, Qt::AlignRight );
    fcLyt->addWidget( autoHintCB,   4, 1, Qt::AlignLeft );

    fcLyt->addWidget( noBoldHintCB, 5, 1, 1, 2, Qt::AlignLeft );

    fcLyt->addWidget( lcdFilterLbl, 6, 0, Qt::AlignRight );
    fcLyt->addWidget( lcdFilterCB,  6, 1, Qt::AlignLeft );

    fcLyt->addWidget( fresLbl,      7, 0, Qt::AlignRight );
    fcLyt->addWidget( fresSB,       7, 1, Qt::AlignLeft );

    fcLyt->addWidget( resetBtn,     8, 0, Qt::AlignLeft );
    fcLyt->addWidget( saveBtn,      8, 2, Qt::AlignRight );

    QSpacerItem *stretch = new QSpacerItem( 1, 1, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum );

    fcLyt->addItem( stretch, 0, 2 );

    /** Font Config groupbox */
    QGroupBox *fcGB = new QGroupBox( "Font Config Settings" );

    fcGB->setLayout( fcLyt );

    /** Base Layout to keep everything together */
    QVBoxLayout *base = new QVBoxLayout();

    base->addWidget( fontsGB );
    base->addWidget( fcGB );
    base->addStretch();

    setLayout( base );
}


void FontsPage::loadDefaults() {
    mGeneralFont = getFontFromString( settings->value( "Fonts::AppFonts::General" ) );
    mFixedFont   = getFontFromString( settings->value( "Fonts::AppFonts::Fixed" ) );
    mSmallFont   = getFontFromString( settings->value( "Fonts::AppFonts::Small" ) );
    mToolbarFont = getFontFromString( settings->value( "Fonts::AppFonts::Toolbar" ) );
    mMenuFont    = getFontFromString( settings->value( "Fonts::AppFonts::Menu" ) );
}


void FontsPage::storeFCSettings() {
    QString fcStart(
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
        "<!DOCTYPE fontconfig SYSTEM \"fonts.dtd\">\n"
        "<fontconfig>\n"
        "    <match target=\"font\">\n"
    );

    QString fcEndMatch(
        "    </match>\n"
    );

    QString fcEnd(
        "</fontconfig>\n"
    );

    /** Anti-aliasing */
    QString aaStr = QString(
        "        <edit name=\"antialias\" mode=\"assign\">\n"
        "            <bool>%1</bool>\n"
        "        </edit>\n"
    ).arg( aaCB->isChecked() ? "true" : "false" );

    /** Hinting */
    QString htStr = QString(
        "        <edit name=\"hinting\" mode=\"assign\">\n"
        "            <bool>%1</bool>\n"
        "        </edit>\n"
    ).arg( hintCB->isChecked() ? "true" : "false" );

    /** Hint Style */
    QString hsStr = QString(
        "        <edit name=\"hintstyle\" mode=\"assign\">\n"
        "            <const>hint%1</const>\n"
        "        </edit>\n"
    ).arg( styleCB->currentText().toLower() );

    /** Sub-Pixel */
    QString spStr = QString(
        "        <edit name=\"rgba\" mode=\"assign\">\n"
        "            <const>%1</const>\n"
        "        </edit>\n"
    ).arg( subpxlCB->currentText().toLower().replace( "-", "" ) );

    /** Auto-Hint */
    QString ahStr = QString(
        "        <edit name=\"autohint\" mode=\"assign\">\n"
        "            <bool>%1</bool>\n"
        "        </edit>\n"
    ).arg( autoHintCB->isChecked() ? "true" : "false" );

    /** Bold Hinting */
    QString bhStr = QString(
        "    <match target=\"font\">\n"
        "        <test name=\"weight\" compare=\"more\">\n"
        "            <const>medium</const>\n"
        "        </test>\n"
        "        <edit name=\"autohint\" mode=\"assign\">\n"
        "            <bool>%1</bool>\n"
        "        </edit>\n"
        "    </match>\n"
    ).arg( noBoldHintCB->isChecked() ? "true" : "false" );

    /** LCD Filter */
    QString lfStr = QString(
        "        <edit name=\"lcdfilter\" mode=\"assign\">\n"
        "            <const>lcd%1</const>\n"
        "        </edit>\n"
    ).arg( lcdFilterCB->currentText().toLower() );

    /** Resolution */
    QString rsStr = QString(
        "        <edit name=\"dpi\" mode=\"assign\">\n"
        "            <double>%1</double>\n"
        "        </edit>\n"
    ).arg( fresSB->value() );

    QFile fcConf( DFL::XDG::xdgConfigHome() + "fontconfig/fonts.conf" );

    if ( fcConf.exists() ) {
        bool ok = QFile::copy( fcConf.fileName(), fcConf.fileName() + ".bak" + QDateTime::currentDateTime().toString( ".yyyyMMddhhmmss" ) );

        if ( not ok ) {
            int ret = QMessageBox::question(
                this,
                "DesQ Theme Settings | Question?",
                "DesQ has failed to make a backup of the existing fonts configuration file: "
                "<p><center><tt>" + fcConf.fileName() + "</tt></center></p>"
                "Overwrite the existing configuration?"
            );

            if ( ret == QMessageBox::No ) {
                return;
            }
        }
    }

    if ( fcConf.open( QFile::WriteOnly ) ) {
        fcConf.write( fcStart.toUtf8() );
    }

    /** Write anti-aliasing */
    fcConf.write( aaStr.toUtf8() );

    /** Writing hinting */
    fcConf.write( htStr.toUtf8() );

    /** Write hint style and subpixel only if hinting is enabled */
    if ( hintCB->isChecked() ) {
        fcConf.write( hsStr.toUtf8() );
        fcConf.write( spStr.toUtf8() );
    }

    /** Writing auto-hinting */
    fcConf.write( ahStr.toUtf8() );

    /** Write lcd-filter */
    fcConf.write( lfStr.toUtf8() );

    /** Write resolution */
    fcConf.write( rsStr.toUtf8() );

    /** Close block */
    fcConf.write( fcEndMatch.toUtf8() );

    /**
     * This is a separate block. Write after other entries.
     * Disable bold hinting only if auto-hinting is enabled
     */
    if ( autoHintCB->isChecked() ) {
        fcConf.write( ahStr.toUtf8() );
    }

    /** End file */
    fcConf.write( fcEnd.toUtf8() );

    fcConf.close();

    settings->setValue( "Fonts::FontConfig::AntiAliasing",    aaCB->isChecked() );
    settings->setValue( "Fonts::FontConfig::Hinting",         hintCB->isChecked() );
    settings->setValue( "Fonts::FontConfig::Style",           styleCB->currentText() );
    settings->setValue( "Fonts::FontConfig::Subpixel",        subpxlCB->currentText() );
    settings->setValue( "Fonts::FontConfig::AutoHinting",     autoHintCB->isChecked() );
    settings->setValue( "Fonts::FontConfig::DisableAutoHint", noBoldHintCB->isChecked() );
    settings->setValue( "Fonts::FontConfig::LCDFilter",       lcdFilterCB->currentText() );
    settings->setValue( "Fonts::FontConfig::FontResolution",  fresSB->value() );
}
