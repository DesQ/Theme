/**
 * This file is a part of DesQ Appearance.
 * DesQ Appearance is the Settings plugin to manage DesQ's widget theme,
 * fonts, colors and other related configurations.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This plugin is derived from Qt5CT project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Widgets.hpp"

ThemeView::ThemeView() {
    setViewMode( QListView::IconMode );
    setFlow( QListView::LeftToRight );
    setResizeMode( QListView::Adjust );
    setIconSize( QSize( 256, 128 ) );
    setUniformItemSizes( true );
    setSelectionBehavior( QListView::SelectItems );
    setSelectionMode( QListView::SingleSelection );
}


void ThemeView::setIconSize( QSize size ) {
    int minWidth = qMin( 284, qMax( 144, size.width() * 3 ) );
    int height   = 160;

    /* Items per row */
    int items = ( int )(viewportWidth / (minWidth + mSpacing) );

    /* Minimum width of all items */
    int itemsWidth = items * minWidth;

    /* Empty space remaining */
    int empty = viewportWidth - itemsWidth;

    /* Extra space per item */
    int extra = empty / items;

    /* New grid size */
    QSize gridSize = QSize( minWidth + extra + 2, height + 2 );

    /* New grid size */
    gridSize = QSize( minWidth + mSpacing + extra, height + mSpacing );
    int extraSpace = mSpacing + extra;

    setSpacing( extraSpace );

    QListView::setGridSize( gridSize );
    QListView::setIconSize( size );
}


void ThemeView::resizeEvent( QResizeEvent *rEvent ) {
    rEvent->accept();

    /* To prevent crashes while hiding the split view */
    viewportWidth = qMax( viewport()->width(), 128 );
    setIconSize( iconSize() );
}


QLineEdit * createLineEdit( QFont font, QString text, bool wide ) {
    QLineEdit *le;

    if ( text.isEmpty() ) {
        le = new QLineEdit( QString( "%1 %2pt" ).arg( font.family() ).arg( font.pointSize() ) );
    }

    else {
        le = new QLineEdit( text );
    }

    le->setFont( font );
    le->setReadOnly( true );
    le->setFocusPolicy( Qt::NoFocus );

    if ( not wide ) {
        le->setMinimumWidth( 75 );
        le->setMaximumWidth( 150 );
    }

    return le;
}


QWidget * separator( Qt::Orientation o ) {
    QFrame *f = new QFrame();

    switch ( o ) {
        case Qt::Horizontal: {
            f->setFrameStyle( QFrame::HLine | QFrame::Plain );
            break;
        }

        case Qt::Vertical: {
            f->setFrameStyle( QFrame::VLine | QFrame::Plain );
            break;
        }
    }

    return f;
}
