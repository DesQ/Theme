/**
 * This file is a part of DesQ Appearance.
 * DesQ Appearance is the Settings plugin to manage DesQ's widget theme,
 * fonts, colors and other related configurations.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This plugin is derived from Qt5CT project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <signal.h>

#include <QApplication>
#include <QLibraryInfo>
#include <QLocale>
#include <QtGlobal>
#include <QTranslator>
#include <QtDebug>

#include <desq/Utils.hpp>
#include <desq/desq-config.h>

#include "MainWindow.hpp"
#include "PreviewWidget.hpp"

#include <DFApplication.hpp>
#include <DFUtils.hpp>
#include <DFXdg.hpp>

int main( int argc, char *argv[] ) {
    QDir cache( DFL::XDG::xdgCacheHome() );

    DFL::log = fopen( cache.filePath( "DesQ/Theme.log" ).toLocal8Bit().data(), "a" );

    qInstallMessageHandler( DFL::Logger );

    QByteArray date = QDateTime::currentDateTime().toString( "yyyyMMddThhmmss" ).toUtf8();

    qDebug() << "------------------------------------------------------------------------";
    qDebug() << "DesQ Theme started at" << date.constData();
    qDebug() << "------------------------------------------------------------------------\n";

    DFL::Application app( argc, argv );

    app.setQuitOnLastWindowClosed( true );

    // Set application info
    app.setOrganizationName( "DesQ" );
    app.setApplicationName( "Theme Settings" );
    app.setApplicationVersion( PROJECT_VERSION );
    app.setDesktopFileName( "desq-theme-settings" );

    app.interceptSignal( SIGSEGV, true );
    app.interceptSignal( SIGABRT, true );

    QCommandLineParser parser;

    parser.addHelpOption();
    parser.addVersionOption();

    if ( app.lockApplication() ) {
        MainWindow *w = new MainWindow();

        w->show();

        QObject::connect(
            &app, &DFL::Application::messageFromClient, [ = ] ( QString, int ) {
                w->raise();
                // w->activate();
            }
        );

        return app.exec();
    }

    else {
        app.messageServer( "raise" );
        return 0;
    }

    return 0;
}
