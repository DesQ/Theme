/**
 * This file is a part of DesQ Appearance.
 * DesQ Appearance is the Settings plugin to manage DesQ's widget theme,
 * fonts, colors and other related configurations.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This plugin is derived from Qt5CT project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "StylesPage.hpp"
#include "PreviewWidget.hpp"
#include "Widgets.hpp"

StylesPage::StylesPage( QWidget *parent ) : QWidget( parent ) {
    createGUI();

    QString style  = (QString)settings->value( "Appearance::Styles::WidgetStyle" );
    QString colors = (QString)settings->value( "Appearance::Colors::Scheme" );

    /** Populate the styles */
    for ( QString style: QStyleFactory::keys() ) {
        /** Ignore DesQ style */
        if ( style.compare( "DesQ", Qt::CaseInsensitive ) == 0 ) {
            continue;
        }

        /** Ignore Qt5CT style */
        if ( style.compare( "qt5ct-style", Qt::CaseInsensitive ) == 0 ) {
            continue;
        }

        /** Ignore Qt6CT style */
        if ( style.compare( "qt6ct-style", Qt::CaseInsensitive ) == 0 ) {
            continue;
        }

        QPixmap         pix   = PreviewWidget::preview( style, colors );
        QListWidgetItem *item = new QListWidgetItem( QIcon( pix ), style, styles );
        styles->addItem( item );
    }

    QList<QListWidgetItem *> selStyleItems = styles->findItems( style, Qt::MatchFixedString );
    QList<QListWidgetItem *> fusionItems   = styles->findItems( "Fusion", Qt::MatchExactly );

    if ( selStyleItems.count() ) {
        styles->setCurrentItem( selStyleItems.at( 0 ) );
    }

    else if ( fusionItems.count() ) {
        styles->setCurrentItem( fusionItems.at( 0 ) );
    }

    else {
        qWarning() << "Unable to find Fusion widget style. Please check your Qt installation.";
    }

    connect(
        styles, &QListWidget::currentTextChanged, [ = ]( QString newStyle ) {
            // Emit the signal to update the pewview dialog
            emit changeStyle( newStyle );

            /** Save the setting */
            settings->setValue( "Appearance::Styles::WidgetStyle", newStyle );
        }
    );
}


StylesPage::~StylesPage() {
    delete styles;
}


void StylesPage::createGUI() {
    styles = new ThemeView();

    gtkBtn = new QPushButton( QIcon::fromTheme( "preferences-gtk-config" ), "&Gtk Theme" );
    connect(
        gtkBtn, &QPushButton::clicked, [ = ]() {
            QMessageBox::information(
                this,
                "DesQ Appearance | Sorry",
                "Sorry. GTK Settings are not yet available. Please be patient while we work on this."
            );
        }
    );

    QHBoxLayout *btnLyt = new QHBoxLayout();

    btnLyt->addWidget( gtkBtn );
    btnLyt->addStretch();

    QVBoxLayout *baseLyt = new QVBoxLayout();

    baseLyt->addWidget( styles );
    baseLyt->addLayout( btnLyt );
    setLayout( baseLyt );

    setMinimumSize( 800, 600 );
}
